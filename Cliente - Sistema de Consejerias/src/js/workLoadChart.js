import * as d3 from 'd3'

///metodo que dibuja la barra de horas en carga de trabajo.
///el parametro horas es en formato {practicas:, teoricas:, autonomas:,}
let val = 0;
export function createChart(horas){
    let empty = false;
    if(horas.practicas === 0 && horas.teoricas === 0 && horas.autonomas === 0){
        empty = true;
        horas = {
            teoricas: 10,
            practicas: 10,
            autonomas: 10
        }
    }
        //return;
    d3.select('.headerWorkload').remove();
    d3.select('.divworkload').remove();
    //d3.select('.charLine').style('display', 'block');
    let height = 0;
    if(val === 0){
        height = d3.select('.chartLine').node().offsetHeight;
        val = height;
    }else{
        height = val;
    }
    
   // console.log(height);
    //desaparezco el linechart
    hideRectChart();
    //creo el header
    let divHeader = d3.select('.chartOne')
        .append('div')
        .attr('class', 'headerWorkload')
        .attr('width', '100%')
        //.attr('height')
        .style('display', 'flex');
    ///creo el texto del header mediante un h5
    divHeader.append('h5')
        .text('Horas de carga de trabajo semanal');
    //creo el contenedor de la grafica 
  /*  let divWorkload = d3.select(".chartOne")
        .append('div')
        .attr('class', 'divworkload')
        .attr('height', '100%')
        .attr('width', '100%');
    const margin = {top: 10, bottom: 10, left: 10, right: 10}
    //console.log(divWorkload.node().offsetHeight);
    const width = divWorkload.node().offsetWidth - margin.left - margin.right;
    //const height = divWorkload.node().offsetHeight - margin.top - margin.bottom;
    console.log('width '+width);
    let svg = divWorkload.append('svg')
        // .attr('width', width)
        // .attr('height', height)
        .attr("preserveAspectRatio", "xMinYMin meet")
          //.attr("viewBox", "0 0 "+(width + margin.left + margin.right)+" "+(height + margin.top + margin.bottom))
        .attr("viewBox", "0 0 540 225")
        .attr('class', 'svgWorkload');

    let HorasCalculadas = returnSizes(horas, width);
    
    let xScale = d3.scaleLinear().domain([0, HorasCalculadas.mayor]).range([0, HorasCalculadas.mayor]);
    let array = [horas.practicas, horas.teoricas, horas.autonomas]; 
    // let dat = [{practicas: horas.practicas, teoricas: horas.teoricas, autonomas: horas.autonomas}];
    // // Transpose the data into layers
    // var dataset = d3.stack()(["practicas", "teoricas", "autonomas"].map(function(horas) {
    //     return dat.map(function(d, i) {
    //         console.log("data");
    //     return {y: i,x: +d[horas]};
    //     });
    // }));
    // console.log(dataset);
    // // Set x, y and colors
    // var yy = d3.scaleOrdinal()
    //     .domain(dataset[0].map(function(d) { return d.y; }))
    //     .rangeRoundBands([0, 20], 0.02);
    // var x = d3.scaleLinear()
    //     .domain([0, d3.max(dataset, function(d) {  return d3.max(d, function(d) { return d.x0 + d.x; });  })])
    //     .range([0, HorasCalculadas.size]);

    //     var colors = ["b33040", "#d25c4d", "#f2b447", "#d9d574"];

    // // Create groups for each series, rects for each segment 
    // var groups = svg.selectAll("g.cost")
    // .data(dataset)
    // .enter().append("g")
    // .attr("class", "cost")
    // .style("fill", function(d, i) { return colors[i]; });

    // var rect = groups.selectAll("rect")
    //     .data(function(d) { return d; })
    //     .enter()
    //     .append("rect")
    //     .attr("y", function(d) { return y(d.y); })
    //     .attr("x", function(d) { return x(d.x0 + d.x); })
    //     .attr("height", function(d) { return y.rangeBand(); })
    //     .attr("width", function (d){return x(d.x0) - x(d.x0 + d.x);});
    //     // .on("mouseover", function() { tooltip.style("display", null); })
    //     // .on("mouseout", function() { tooltip.style("display", "none"); })
    //     // .on("mousemove", function(d) {
    //     //     var xPosition = d3.mouse(this)[0] - 15;
    //     //     var yPosition = d3.mouse(this)[1] - 25;
    //     //     tooltip.attr("transform", "translate(" + xPosition + "," + yPosition + ")");
    //     //     tooltip.select("text").text(d.y);
    //     // });


    


    let y = height - (height / 3);

    //console.log("height "+height);
    let anterior =0 ;
    let anterior2 =0 ;
    svg.selectAll('rect')
        .data(array)
        .enter()
        .append('rect')
        .attr('x', function (d, i){
            //console.log(xScale(i));
            // if( i === 0){
            //     anterior += xScale(d) / 3;
            //     return i;      
            // }else if(i===1){
            //     anterior2 = anterior + xScale(d) / 3;
            //     return anterior;
            // }else{
            //     return anterior2;
            // }   
             return xScale(d);
        })
        .attr('y', height - 20)
        .attr('width', function(d, i){
            console.log(xScale(d.x0));
            return xScale(d.x0) - xScale(d.x0 + d.x);
        })
        .attr('height', 20)
        .style('fill', function(d, i){
            if(i===0){
                return 'lightblue';
            }else if(i===1){
                return 'darkblue';
            }else {
                return 'blue';
            }
        });

    svg.append('rect')
        .attr('x', margin.left)
        .attr('y', y)
        .attr('width', HorasCalculadas.practicas)
        .attr('height', 20)
        .style('fill', 'lightblue');

    svg.append('rect')
        .attr('x', (margin.left + HorasCalculadas.practicas))
        .attr('y', y)
        .attr('width', HorasCalculadas.teoricas)
        .attr('height', 20)
        .style('fill', 'darkblue');

    svg.append('rect')
        .attr('x', (margin.left + HorasCalculadas.practicas + HorasCalculadas.teoricas))
        .attr('y', y)
        .attr('width', HorasCalculadas.autonomas)
        .attr('height', 20)
        .style('fill', 'blue');

    */
   let data = [{"interest_rate": "data", "practicas":horas.practicas, "teoricas": horas.teoricas, "autonomas": horas.autonomas}];
//    var data = [
//     {
//          "interest_rate":"< 4%",
//          "Default":60,
//          "Charge-off":20,
//          "Current":456,
//          "30 days":367.22,
//          "60 days":222,
//          "90 days":198,
//          "Default":60
         
//       },
//       {
//          "interest_rate":"4-7.99%",
//          "Charge-off":2,
//          "Default":30,
//          "Current":271,
//          "30 days":125,
//          "60 days":78,
//          "90 days":72
         
        
//       }
// ];

const margin = {top: 10, bottom: 10, left: 10, right: 10};
            // width = 450 - margin.left - margin.right,
            // height = 315 - margin.top - margin.bottom,
             
    let divWorkload = d3.select(".chartOne")
             .append('div')
             .attr('class', 'divworkload')
             .attr('height', '90%')
             .attr('width', '100%');
        //obtengo el ancho del divworkload
        const width = divWorkload.node().offsetWidth - margin.left - margin.right-30;
             //const height = divWorkload.node().offsetHeight - margin.top - margin.bottom;
        //defino a y que sera desde 0 a el alto, cuyo alto es el alto del div chartOne(donde esta la grafica de lineas comparativas)
         var y = d3.scaleBand().rangeRound([0, height]);
        //defino x la cual va desde 0 hatsa el tamano de ancho del divworkload
         var x = d3.scaleLinear().rangeRound([0, width*0.65]);
         ///sirve para definir colores   (uso solo para defrinir rangos par adibujar las barras, los colores en si, no los uso)
         var color = d3.scaleOrdinal(d3.schemeCategory10);;
        //creo mi array de colores
        let colores = ['#87E0FE', '#023C52', '#446B7A'];
        let datos = [horas.practicas, horas.teoricas, horas.autonomas];
         //declaro el axis y (no necesito)
        // var yAxis = d3.axisLeft().scale(y);
        ///declaro el axis X (no uso)
      //   var xAxis = d3.axisBottom().scale(x).tickFormat(d3.format(".0%"));
        //creo la imagen svg 
        //console.log(window.screen.width+" "+window.screen.height+" "+window.devicePixelRatio);
        
        //EN CASO DE QUE LA RESOLUCION SEA DE 4K ENTONCES TIENE QUE ADAPTARSE A LA PANTALLA DE 4K LA GRAFICA, YA QUE NO SE ADAPTA CON EL VIEWBOX PREDETERMINADO
        let resWidth = window.screen.width * window.devicePixelRatio;//encuentro la resolucion en cuanto al ancho (el devicePixelratio sirve para moviles, en descktop es 1)
        let resheight = window.screen.height * window.devicePixelRatio;//encuentro la resolucion en cuanto al alto
        let viewBox = "0 0 540 175";
        if(resWidth > 3000){
            viewBox = "0 0 1080 225";
        }
         var svg = d3.select(".divworkload")
            .append("svg")
                // .attr("width", width + margin.left + margin.right)
                // .attr("height", height + margin.top + margin.bottom)
                .attr("preserveAspectRatio","xMinYMin meet")
                // .attr("viewBox","0 0 540 225")
                .attr("viewBox",viewBox)
                .append("g")
                    .attr("transform", "translate(" + (margin.left+50) + "," + (margin.top + 10) + ")");

         color.domain(d3.keys(data[0]).filter(function (key) {
             return key !== "interest_rate";
         }));


         data.forEach(function (d) {
             var x0 = 0;

             d.rates = color.domain().map(function (name) {
                 //console.log();;
                 return {
                     name: name,
                     x0: x0,
                     x1: x0 += +d[name],
                     amount: d[name]
                 };
             });
             d.rates.forEach(function (d) {
                 d.x0 /= x0;
                 d.x1 /= x0;
             });

           //  console.log(data);
         });
         data.sort(function (a, b) {
             return b.rates[0].x1 - a.rates[0].x1;
         });

         y.domain(data.map(function (d) {
             return d.interest_rate;
         }));

        // svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + height + ")").call(xAxis);

        // svg.append("g").attr("class", "y axis").call(yAxis);
         let yPos = height / 2 - 65;
         var interest_rate = svg.selectAll(".interest-rate").data(data).enter().append("g").attr("class", "interest-rate").attr("transform", function (d) {
             //return "translate(0," + y(d.interest_rate) + ")";
             return "translate(0," + yPos + ")";
         });
         let ancho = 0;
         interest_rate.selectAll("rect").data(function (d) {
             return d.rates;
         }).enter().append("rect").attr("height", 20).attr("x", function (d) {
             return x(d.x0);
         }).attr("width", function (d) {
             //return x(d.x0) - x(d.x1);
             ancho = x(d.x1);
             return  x(d.x1) - x(d.x0);
         }).style("fill", function (d, i) {
             //console.log(color(d.name));
             //return color(d.name);
             return colores[i];
          });

          //inserto el texto en el medio de las barras (numero de horas)
          interest_rate.selectAll("text").data(function (d) {
            return d.rates;
        }).enter().append("text").attr("x", function (d) {
            return ((x(d.x1) - x(d.x0))/2) + x(d.x0) - 15;
        }).attr("y", function(d){
            //retorno este valor porque cuando declaro interest_rate (var interest_rate) alli ya hago un translate en y, 
            //por lo cual la pos cero de cualquier elemento dentro de este componente va a ser la posicion del componente.
            return 16;
        }).text(function(d, i){
            if(parseInt(datos[i]) > 0)
                if(empty === true)
                    return '0h';
                else
                    return datos[i]+"h";
            else
                return '';
        }).style("fill", function (d, i) {
            //console.log(color(d.name));
            //return color(d.name);
            if(i === 1)
                return 'white';
            else
                return 'black';
         });
         
         //inserto el texto al lado de las barras (practicas, teoricas, autonomas)

         let texto = ["Trabajo Práctico", "Teoría", "Trabajo Autónomo"];
         if(parseInt(horas.practicas) === 0){
            texto = ["", "Teoría", "Trabajo Autónomo"];
         }else if(parseInt(horas.teoricas) === 0){
            texto = ["Trabajo Práctico", "", "Trabajo Autónomo"];
         }else if(parseInt(horas.autonomas) === 0){
            texto = ["Trabajo Práctico", "Teoría", ""];
         }
         if(parseInt(horas.practicas) === 0 && parseInt(horas.teoricas) === 0 && parseInt(horas.autonomas) !== 0){
             texto = ["", "", "Trabajo Autónomo"];
         }
         if(parseInt(horas.practicas) === 0 && parseInt(horas.autonomas) === 0 && parseInt(horas.teoricas) !== 0){
             texto = ["", "Teoría", ""];
         }
         if(parseInt(horas.autonomas) === 0 && parseInt(horas.teoricas) === 0 && parseInt(horas.practicas) !== 0){
             texto = ["Trabajo Práctico", "", ""];
         }
         interest_rate.selectAll("g").data(function (d) {
            return d.rates;
        }).enter().append("text").attr("x", function (d, i) {
            //if(i === 0){
                let len = Math.round(texto[i].length / 2) * 8;//por 8 porque se supone que cada caracter tiene 8 pixeles
               // ancho = ((x(d.x1) - x(d.x0))/2) + x(d.x0);  
                return ((x(d.x1) - x(d.x0))/2) + x(d.x0) - len;
            //}
            
        }).attr("y", function(d, i){
            //retorno este valor porque cuando declaro interest_rate (var interest_rate) alli ya hago un translate en y, 
            //por lo cual la pos cero de cualquier elemento dentro de este componente va a ser la posicion del componente.
            if(i === 1)
                return 33; 
            return -5;
        }).text(function(d, i){
            return texto[i];
        }).style("fill", function (d, i) {
                return 'black';
         }).attr('class', 'legendWorkLoad');
        //  .on('mouseover', function (d) {
        //      var total_amt;
        //      total_amt = d.amount;

         ///dibujo el texto del total de horas
         svg.append('g')
            .attr('transform', function(){
                let size  = 100;
                return 'translate('+(ancho + 10)+','+(yPos + 20 )+')';
            })
            .style('font-size', '1.4vw')
            .style('font-weight', '800')
            .append('text')
            .text(function (){
                let total =  horas.practicas + horas.teoricas + horas.autonomas;
                if(empty === true)
                    return '0h';
                return total + 'h';
            });

        //      console.log('----');
        //      d3.select(".chart-tip").style('opacity', '1').html('Amount: <strong>$' + that.numberWithCommas(total_amt.toFixed(2)) + '</strong>');

        //  }).on('mouseout', function () {
        //      d3.select(".chart-tip").style('opacity', '0');
        //  });

        //  var legend = svg.selectAll(".legend").data(color.domain().slice().reverse()).enter().append("g").attr("class", "legend").attr("transform", function (d, i) {
        //      return "translate(" + i * -70 + ",283)";
        //  });


        //  legend.append("rect").attr("x", width + -53).attr("width", 10).attr("height", 10).style("fill", color);

        //  legend.append("text").attr("x", width - 40).attr("y", 5).attr("width", 40).attr("dy", ".35em").style("text-anchor", "start").text(function (d) {
        //      return d;
        // });



    
}

function returnSizes(horas, width){
    ///si el 100% es width, cuanto sera el 70%?==el 70% es lo que va a ocupar la grafica de barra.
    //aplico regla de tres (70 * width / 100)
    
    let size = (70 * width) / 100;
   // console.log(size);
    let autonomas = horas.autonomas;
    let practicas = horas.practicas;
    let teoricas = horas.teoricas;
    let mayor = 0;
    // let au = 0;
    // let pra = 0;
    // let teo = 0;
    if(autonomas >= practicas && autonomas >= teoricas){
        mayor = autonomas;

    }else if(practicas >= autonomas && practicas >= teoricas){
        mayor = practicas;
    }else if(teoricas >= practicas && teoricas >= autonomas){
        mayor = teoricas;
    }
    ///divido el size para 3 pues son 3 tipos de horas
    //con esto obtengo el maximo tamano que puede tener cada uno si cada uno es el mismo valor
    //por ejm: autonomas, teoricas y practicas todas tienen 54 horas, significa que 
    //las barras vana ser del mismo tamano
    let maxSizePerTypeHours = size / 3;

    //calculo por cada tipo de hora a cuanto equivale lo que posee cada uno
    //tomo como base al mayor.
    //si mayor es igual a maxSizePerTypeHours, entonces autonomas, practicas teoricas a cuanto sera
    //aplico regla de tres
    let hautonomas = (autonomas * maxSizePerTypeHours) / mayor;
    let hpracticas = (practicas * maxSizePerTypeHours) / mayor;
    let hteoricas = (teoricas * maxSizePerTypeHours) / mayor;
    
    return {
        autonomas: hautonomas,
        practicas: hpracticas,
        teoricas: hteoricas,
        size: size,
        mayor: mayor
    }
}

function hideRectChart(){
    d3.selectAll(".headerDesapear")
        .style('display', 'none');
}
export function showLineChartFromWorkLoad(){
    d3.select('.headerWorkload').remove();
    d3.select('.divworkload').remove();
    d3.selectAll(".headerDesapear")
        .style('display', 'flex');
}