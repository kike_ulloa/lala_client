import * as d3 from 'd3';

export function showLineChart (data, dates, coursesIds, grades, averageApproved, avverageDesaproved, totalAsignaturas, estudiante, aniosPorPeriodo, cursando){
    //console.log(data);
    let fechas = dates;
    //console.log(fechas);
    const nestById = d3.nest()
        .key(d => d.id)
        .sortKeys((v1, v2) => (parseInt(v1, 10) > parseInt(v2, 10) ? 1 : -1))
        .entries(data);
    //console.log(nestById);
    //agrego los nombres al objeto dataNamesId
    const dataNamesById = {};
    let comapaneros = [];
    let companerosAux = [];
    let student = [];
    let studentAux = [];
    let totalCursesPerTerm = [];
    let fechaCom = [];
    //let contadorEst = 0;

    let mapYears = new Map();

    let lastMonth = -1;
    let lastYear = 0;
    let auxCompa = [];
    nestById.forEach(item => {
       // console.log(item.key+ "   "+item.values);
        dataNamesById[item.key] = item.values[0].tipo;//estudiante o companeros
        // let compa = {};
        // let estudiante = {};
        if(parseInt(item.key) === 1){
            let index = 0;
            let contadorCompa = 0;
           // console.log("Entraaaaaaa "+item.values.length);
           //for(i in item.values)
            for (index in item.values){
              comapaneros[index] = {
                x: contadorCompa,
                y: item.values[index].data
            }
            companerosAux[index] = {
              grade: item.values[index].data
            }
            //console.log(index);
            //index++;
            
            contadorCompa+=10;
            }
            // item.values.map(val => {
            //     comapaneros[index] = {
            //         x: contadorCompa,
            //         y: val.data
            //     }
            //     companerosAux[index] = {
            //       grade: val.data
            //     }
            //     console.log(index);
            //     index++;
                
            //     contadorCompa+=10;
            //     return comapaneros;
            // });
            
        }
         //onsole.log(comapaneros);
        //en caso de que sea la llave  = 2 significa q es el estudiante (1 = companeros, 2 = estudiante)
        let indexCompa = 0;
        
        if(parseInt(item.key) === 2 ){
              //obtengo la fecha par apoder ubicar los anios
            let fech = new Date(Date.parse(item.values[0].fecha));//digo que la pos cero proq necesito saber si empieza en febrero-marzo o en septiembre
            if(fech.getMonth() === 2 || fech.getMonth() === 1){//si la fecha es igual a febrero o marzo (el get month devuelve a enero como 0)
                mapYears.set(fech.getFullYear(), fech.getFullYear()); 
            }else if(fech.getMonth() === 8){//si es igual a septiembre
                mapYears.set(fech.getUTCFullYear(), null);
            }
            let index = 0;
            let indexDos = 0;
            let contadorEst = 0;
            item.values.map(val => {
              let d = new Date(item.values[index].fecha);
             // console.log(d.getMonth()+" "+lastMonth+"   "+d.getFullYear() + " "+(lastYear));
              if((d.getMonth() === lastMonth) && (d.getFullYear() === (lastYear + 1))){ //si es el mismo mes el mes anterior en el bucle con el mes actual del bucle (val de pos anterior con val de pos actual) significa que se salto un ciclo
                student[indexDos] = null;//no ha tomaod un ciclo
                auxCompa[indexDos] = null;
                //fechaCom [indexDos] = null;
                contadorEst+=10;
                //console.log("entra aqui");
                indexDos++;
                student[indexDos] = {//
                  x: contadorEst,
                  y: val.data
                }

                auxCompa[indexDos] = {
                  x: contadorEst,
                  y: comapaneros[indexCompa].y
                }

                //fechaCom [indexDos] = fech;
              }else if(d.getFullYear() > (lastYear+1) && (lastYear !== 0)){//en caso de que se haya saltado mas de un ciclo
                //console.log("contador est: "+contadorEst);
                for(var j = (lastYear + 1); j< d.getFullYear(); j++){
                  if(j === (lastYear + 1)){
                    if(lastMonth === 1 || lastMonth === 2){
                      student[indexDos] = null;
                      auxCompa[indexDos] = null;
                  //    fechaCom[indexDos] = null;
                      contadorEst+=10;
                      indexDos++;
                    }
                    
                    student[indexDos] = null;
                    auxCompa[indexDos] = null;
                    //fechaCom[indexDos] = null;
                    contadorEst+=10;
                    indexDos++;
                    student[indexDos] = null;
                    auxCompa[indexDos] = null;
                    //fechaCom[indexDos] = null;
                    contadorEst+=10;
                    indexDos++;
                  }
                }
                //en el caso de que sea igual al primer mes lectivo
                if(d.getMonth() === 1 || d.getMonth() === 2){
                  student[indexDos] = {
                    x: contadorEst,
                    y: val.data
                  }
                  auxCompa[indexDos] = {
                    x: contadorEst,
                    y: comapaneros[indexCompa].y
                  }
                  //fechaCom[indexDos] = fech;
                }else if(d.getMonth() === 8){//si es el mes de septiembre, necesito llenar con null el mes de febrro o marzo
                  student[indexDos] = null;
                  auxCompa[indexDos] = null;
                  //fechaCom[indexDos] =null;
                  contadorEst+=10;
                  indexDos++;
                  student[indexDos] = {
                    x: contadorEst,
                    y: val.data
                  }
                  auxCompa[indexDos] = {
                    x: contadorEst,
                    y: comapaneros[indexCompa].y
                  }
                  //fechaCom[indexDos] = fech;
                }
              }else{
                student[indexDos] = {
                    x: contadorEst,
                    y: val.data
                }
                auxCompa[indexDos] = {
                  x: contadorEst,
                  y: comapaneros[indexCompa].y
                }
                //fechaCom[indexDos] = fech;
              }
                totalCursesPerTerm[index] = val.totalAsignaturasEnCiclo;
                if(index !== 0){
                  fech = new Date(Date.parse(item.values[index].fecha));
                  mapYears.set(fech.getFullYear(), fech.getFullYear());
                }
                studentAux[index] = {
                  grade: val.data
                }
                fechaCom[index] = fech;
                index++;
                indexDos++;
                contadorEst+=10;
                lastMonth = d.getMonth();
                lastYear= d.getFullYear();
                indexCompa++;
                return student;
            });
        }
        
        
    });
    
    //console.log(auxCompa, student);
    comapaneros= [];
    comapaneros = auxCompa;
    //console.log(fechaCom);
    // console.log(comapaneros.length);
    // console.log(nestById);
   // console.log(comapaneros, companerosAux);
    //puedo agregar anios al array si inicia con null entonces son 5 anios en total a mostrar, si inicia sin null entonces son 6 anios en total a msotrar
    let iniciaConNulo = false;
    let years = [];
    let index = 0;
    //mapYears.set(2008, null);

    mapYears.forEach(function(value, key, map){
      //console.log(key+" "+value);
      if(value === null){
        if(index === 0){//si inicia con nulo entonces son slo 5 anios que debo poner
          iniciaConNulo = true;
        }else{//si es nulo el valor y no es le inicio, entonces debo agregar ese valor ya que puede haberse saltado un ciclo o mas
          years.push(years[years.length-1]+1);
        }
        
        //years.push(2006);
      }else{
        years.push(value);
        //iniciaConNulo= false;
      }
      index++;
    });
    //console.log(years);
    ///si se salto ciclos debo llenar los anios faltantes
    //years.push(2009);
    
    let yearsValida = [];
    //let ind = 0;
    for(var i in years){

      if(i > 0){
        if(years[i] > (years[i-1]+1)){
          for(let j = (years[i-1]+1); j <= years[i]; j++){
            yearsValida.push(j);
          }
        }else{
          yearsValida.push(years[i]);
        }
      }else{
        yearsValida.push(years[i]);
      }
    }
    //console.log(yearsValida);
    years = [];
    years = yearsValida;
    //console.log(years);
    if(iniciaConNulo === true){
      //console.log("entrap");
      if(years.length < 5){
        for(var i = years.length; years.length < 5; i++ ){
          years[i] = years[i-1] + 1;
        }
      }
    }else{
      if(years.length < 6){
        for(var i = years.length; years.length < 6; i++ ){
          years[i] = years[i-1] + 1;
        }
      }
    }
    //si el ultimo semestre esta cursando entonces debe ser nulo para que no se muestre la linea
    let cursandoUltimo = false;
    let ultimoPeriodo = cursando[cursando.length - 1];
    if(ultimoPeriodo[ultimoPeriodo.length - 1] === true){
      cursandoUltimo = true;
      student[student.length - 1] = null;
      comapaneros[comapaneros.length - 1] = null;
    }
    
    ///comparo companeros con estudiantes (si hay algun nulo en companeros, en estudiantes tambien lo habra)
    //recorro student
    console.log(student, comapaneros, companerosAux);
    // let da = comapaneros[comapaneros.length -1];
    // let con = 0;
    // let compaAux = [];
    // for (var i in student){
    //   if(student[i] === null){
    //   //  console.log(comapaneros[i]);
    //     //da = comapaneros[i];
    //     comapaneros[i] = null;
    //     con ++;
    //   }else{
    //     if(compaAux.length > 0){

    //     }
    //   }
    // }
    // da = {
    //   x: da.x + (con * 10),
    //   y:da.y
    // }
    // if(con > 0){
    //   comapaneros.push(da);
    // }

        
    //console.log(years);
  // console.log(comapaneros);
    //console.log("Student "+student);
    /**Esto debo eliminar */
   // student.push(null);//solo lo uso para simular que no ha cursado esa asignatura ese ciclo
    // comapaneros.push({x: 40, y: 90});
    // comapaneros.push({x: 60, y: 88});

    // student.push({x:50, y:85});
    // student.push({x:60, y:85});
    /**Aqui termina la eliminada */

    //console.log("este es el datanames "+dataNamesById);
    /* Aqui se definen los datos a usar, son datos estaticos, y como los datos los recibo de la base entonces son solo para test estos
    por eso estan comentados

    var data = d3.range(40).map(function(i) {
        return i % 5 ? {x: i / 39, y: (Math.sin(i / 3) + 2) / 4} : null;
      });
     // console.log("holaaaaaa");
      //console.log("este es el data "+data);
      data = [{y:20,x:0},{y:30,x:5},{y:10,x:10},{y:15,x:15},{y:8,x:20},{y:9,x:25},null,{y:17,x:35},{y:19,x:40},{y:11,x:45},{y:2,x:50},{y:7,x:55},{y:5,x:60},{y:25,x:65},{y:18,x:70}];
      let datados = [{y:30,x:0},{y:10,x:5},{y:8,x:10},{y:13,x:15},{y:12,x:20},{y:20,x:25},{y:25,x:30},{y:17,x:35},{y:12,x:40},{y:19,x:45},{y:22,x:50},{y:1,x:55},{y:10,x:60},{y:13,x:65},{y:25,x:70}];
     // console.log(data);
     //obtengo el alto del div denominado chartLine
     //esto, con el fin de que el alto del svg sea el mismo que el del div mensionado
     //asi se le puede hacer responsivo
*/
    // let height = d3.select('.chartLine').node().offsetHeight;
    //defino los margenes para ubicar la grafica, asi como tambien el alto y ancho del svg
      var margin = {top: 15, bottom: 10, right: 15, left: 10},
        //no importa si los valores son estaticos, pues con el preserveAspectRatio y el viewBox s ehce responsivo sin importar si son o no valores estaticos los definidos
          width = 540 - margin.left - margin.right,
          height = 225 - margin.top - margin.bottom;
          //console.log(d3.select('.chartTwo').style('height'));
          //let height = d3.select('.chartLine').node().offsetHeight - margin.top - margin.bottom;
      //domino de 0 a 110, como cada punto va a ser cada 10 y como son 12 puntos (12 ciclos) maximo, y como el primer punto es cero entonces 10 * 11 = 110
      var x = d3.scaleLinear().domain([0,110])
          .range([0, width]);//el rango es el tamano total, ene ste caso el ancho.
      
      var y = d3.scaleLinear().domain([0,100])//la nota maxima es 100 por eso va de 0 a 100
          .range([(height - 40), 0]);//el alto menos 40 para asi poder poner en el eje x los meses y los anios
      //con esto creo la linea y tambien con el define() indico que si hay valores nulos en los datos, no los grafique, que se salte
      var line = d3.line()
          .defined(function(d) { return d; })
          .x(function(d) { return x(d.x); })
          .y(function(d) { return y(d.y); });
      
      var svg = d3.select(".chartLine").append("svg")
          .attr('id', 'chartLineComparative')
          // .attr("width", width + margin.left + margin.right)
          // .attr("height", height + margin.top + margin.bottom)
          .attr("preserveAspectRatio", "xMinYMin meet")
          //.attr("viewBox", "0 0 "+(width + margin.left + margin.right)+" "+(height + margin.top + margin.bottom))
          .attr("viewBox", "0 0 540 225")///se pueden cambiar los valores de 540 y 225 pero con estos se ve bien el grafico
          ///creo un g e indico a ese g que datos se vana  usar(datum)
          var svgUno= svg.append("g")
            .datum(comapaneros)
            .append("g")
            .attr("transform", "translate(" + (margin.left + 5) + "," + margin.top + ")");
        let indexDate = -1;
        //creo el axis en x con las fechas 
        //antes de nada, debo verificar si no hay fechas saltadas, es decir, si no hay dos meses iguales uno a continuacion d eotro
        //es decir, pos 0 marzo pos 1 septiembre pos 2 septiembre pos tres marzo
        let datesAux = [];
        let lastData = "";
        for(var i in dates){
          if(dates[i].toUpperCase() === lastData.toUpperCase()){//si el mes de la pos anterior es igual al de la pos actual
            if(dates[i].toUpperCase() === "SEPT"){ //si es igual a septiembre debo agregar el mes de marzo o febrero
              datesAux.push("Mar");
            }else { //si no es igual a septiembre, significa q es marzo, esto significa q debo agregar septiembre
              datesAux.push("Sept");
            }
            datesAux.push(dates[i]);//agrego el valor actual despues de  agregar el valor faltante
          }else{//en caos de que sean diferentes simplemente agrego el valor que tiene el dates
            datesAux.push(dates[i]);
          }
          lastData = dates[i];//actualizo el valor anterior al valor actual
        }
        dates = [];//borro todo lo que tiene dates
        dates = datesAux;//indico que dates va a ser igual al auxiliar que ya tiene los valores faltantes
        //primero verifico si son 12 meses que se encuentran en la sfechas, caso contrario agrego los flatantes
        if(dates.length < 12){
          
          let size = dates.length;
          for(var i = size; size < 12; size++){
            let lastMonth = dates[dates.length-1];
            if(lastMonth.toUpperCase() === "SEPT"){
              dates.push("Mar");
            }else if(lastMonth.toUpperCase() === "MAR"){
              dates.push("Sept");
            }
          }
        }
        var xAxis  = d3.axisBottom(x)
        .tickFormat(function(){
            indexDate++;
            //console.log("index "+indexDate);
            return dates[indexDate];
        })
        .tickSize(-height - 40);
        
        
        
    //   svgUno.append("g")
    //       .attr("class", "axis axis--x")
    //       .attr("transform", "translate(0," + height + ")")
    //       .call(d3.axisBottom(x));
        
      svgUno.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + (height - 40) + ")")
        .call(xAxis);
        //selecciono todos los tick del eje x para mostrar todos de forma alternada( es decir una claro, otra opaca y asi.)
        d3.selectAll("g.axis--x g.tick line")
        .attr("stroke-opacity", function(d,i){
            //console.log(i%2);
          return (i%2)===0 ? 0.7:0.2;
           
        });
        //para el xAxis de Anios
        //funcion que encuentra la distancia entre dos ticks
        // ticksDistance is constant for a specific x_scale
        const getTicksDistance = (scale) => {
          const ticks = scale.ticks();
          const spaces = []
          for(let i=0; i < ticks.length - 1; i++){
            spaces.push(scale(ticks[i+1]) - scale(ticks[i]))
          }
          return spaces;
        };
        //console.log(getTicksDistance(x));
        // const sizeText= (text) => {
        //   let size =  Math.round(text.length / 2);
        //   return size;
        // }
        //console.log(sizeText(years[0]));
        let distancia =  getTicksDistance(x);
       // console.log(distancia);
        let mitad = distancia[0] / 2;
        if(years.length === 5){
          let yearsAxis = svgUno.append('g')
              .attr("transform", "translate("+(distancia[0] + mitad)+", "+(height - 20) + ")")
              .attr("font-size", 10);
          let year1 = years[0]+"";
          let index=-(year1.length /2)*7; ///el 7 es cuantos pixeles ocupa un caracter de tamano 10
          for(var i in years){
            yearsAxis.append('text')
              .attr("class", 'bold-text')
              .attr('x', index)
              .attr('y', 5)
              .text(years[i]);
            let dist = (distancia[0] * 2) ;
            index+=dist;
          }
        }else if(years.length === 6){
         // console.log("entra");
          let yearsAxis = svgUno.append('g')
              .attr("transform", "translate("+(mitad)+", "+(height - 20) + ")")
              .attr("font-size", 10);
          let year1 = years[0]+"";
          let index=-(year1.length /2)*7; ///el 7 es cuantos pixeles ocupa un caracter de tamano 10
          for(var i in years){
            yearsAxis.append('text')
              .attr("class", 'bold-text')
              .attr('x', index)
              .attr('y', 5)
              .text(years[i]);
            let dist = (distancia[0] * 2) ;
            index+=dist;
          } 
        }
        
        
      svgUno.append("g")
          .attr("class", "axis axis--y")
          .call(d3.axisLeft(y).tickFormat(function(d, i){
               return i === 6 ? d : "";
            
          })
          //.tickSize(tickSize)
          ).attr("stroke-opacity",0.7);
          //selecciono todos los tick del eje y para ocultar todos menos el de 60 (posicion 6)
          d3.selectAll("g.axis--y g.tick line")
          .attr("x2", function(d,i){
            return i===6 ? width:0
             
          })
          .attr("stroke", "green")
          .attr("stroke-width", '2px')
          ;//.attr("stroke-opacity", 0.9);
          d3.selectAll("g.axis--y g.tick text")
          .attr("x", 0);
          //grafica de los partners
      svgUno.append("path")
          .attr("class", "line linePartner")
          .attr("d", line)
          .attr("id", "partner")
          .style("cursor", "pointer")
          .on("mouseover", function(){
              d3.select(this).classed("hover", true);
              d3.select("#partnerLegend").style("font-weight", "bold");
            })
          .on("mouseout", function() {
              d3.select(this).classed("hover", false); 
              d3.select("#partnerLegend").style("font-weight", "normal");
            })
          .on("click", function(){
            if(clickedEstudiante===true){
              d3.select('#student').attr("visibility", 'visible');
              d3.selectAll(".student").attr("visibility", 'visible');
              d3.select(this).classed('hover', false);
              d3.select("#partnerLegend").style("font-weight", "normal");
              clickedEstudiante = false;
            }else{
              d3.select('#student').attr("visibility", 'hidden');
              d3.selectAll(".student").attr("visibility", 'hidden');
              d3.select(this).classed('hover', true);
              d3.select("#partnerLegend").style("font-weight", "bold");
              clickedEstudiante = true;
            } 
          });
      
      svgUno.selectAll(".dot")
        .data(comapaneros.filter(function(d) { return d; }))
        .enter().append("circle")
          .attr("class", "dot partner dp")
          .attr("cx", line.x())
          .attr("cy", line.y())
          .attr("r", 5)
          //.style("cursor", "pointer")
          .on('click', function(d){alert("click"+d.x+ " "+d.y);})
          .on("mouseover", function (){
            d3.select(this).attr("r", 7);
            d3.select("#partnerLegend").style("font-weight", "bold");
          })
          .on('mouseout', function (){
              //if(clickedCOmpanero!==true){
                d3.select(this).attr("r", 5);
                d3.select("#partnerLegend").style("font-weight", "normal");
              //}
          })
          ;
      //agrego los promedis como texto
    //  console.log(comapaneros);
      svgUno.selectAll(".average")
          .data(comapaneros.filter(function(d) {return d}))
          .enter().append("text")
          .attr("class", "averageGrade partner ptext")
          .attr("x", line.x())
          .attr("y", line.y())
          .text(function(d){return d.y;});
 
      //obtengo todos los text con clase averageGrade (los textos de promedio que se hubican sobre el punto cada uno)
      let vect = d3.selectAll('.averageGrade').nodes();
      for( var i in vect){
        let yAttribute = d3.select(vect[i]).attr("y") - 7;
        let xAttribute = d3.select(vect[i]).attr("x") - 7;
        //console.log(yAttribute);
        d3.select(vect[i]).attr("y", yAttribute);
        d3.select(vect[i]).attr("x", xAttribute);
      }
          //.attr("y");
      //console.log(vect);
          //data dos
         /// console.log(comapaneros);
        var svgDos = svg.append("g")
            .datum(student)
            .append("g")
            .attr("transform", "translate(" + (margin.left + 5) + "," + margin.top + ")");
      svgDos.append("path")
          .attr("class", "line lineStudent")
          .attr("d", line)
          .attr("id", "student")
          .attr('stroke', 'black !important')
          .style("cursor", "pointer")
          .on("mouseover", function(){
              d3.select(this).classed("hover", true);
              d3.select("#studentLegend").style("font-weight", "bold");
            })
          .on("mouseout", function() {
              d3.select(this).classed("hover", false); 
              d3.select("#studentLegend").style("font-weight", "normal");
            })
          .on("click", function(){
            if(clickedCOmpanero===true){
              d3.select('#partner').attr("visibility", 'visible');
              d3.selectAll(".partner").attr("visibility", 'visible');
              d3.select(this).classed('hover', false);
              d3.select("#studentLegend").style("font-weight", "normal");
              clickedCOmpanero = false;
            }else{
              d3.select('#partner').attr("visibility", 'hidden');
              d3.selectAll(".partner").attr("visibility", 'hidden');
              d3.select(this).classed('hover', true);
              d3.select("#studentLegend").style("font-weight", "bold");
              clickedCOmpanero = true;
            } 
          });
      // let clicked = false;
      // let valAnt = -1;
      //creo el tooltip
      d3.select(".ttooltip").remove();
      let tooltip = d3.select('body')
        .append('div')
        .attr('class', 'ttooltip')
        .html(tooltipContent)
        .attr("widht","fit-content")
        .attr("height", "fit-content");
      svgDos.selectAll(".dot")
        .data(student.filter(function(d) { return d; }))
        .enter().append("circle")
          .attr("class", "dot student")
          .attr("id", function(d, i){
           // console.log(fechaCom[i]);
           let key = fechaCom[i].getTime();
            return key;
          })
          .attr("cx", line.x())
          .attr("cy", line.y())
          .attr("r", 5)
          .style("cursor", "pointer")
          .on('click', function(d, i){
            //alert("click"+d.x+ " "+d.y);
            // if(valAnt == i){
            //   hideTooltip();
            //   //clicked = !clicked;
            //   valAnt = -1;
            // }else{
            //   hideTooltip();
            //   showTootltip(null, dates[i], d.y, totalCursesPerTerm[i], averageApproved[i], avverageDesaproved[i], comapaneros[i].y);
            //   //clicked = !clicked;
            //   valAnt = i;
            // }
          })
          .on("mouseover", function (d, i){
              //console.log("este es el d del circulo "+i);
              //alert(comapaneros[i]);
              //console.log(companerosAux[i], studentAux[i]);
              lightCourses(i, coursesIds, grades, true);
              // console.log(dates[i]+"  "+aniosPorPeriodo[i]+"   "+i );
              // console.log(dates);
              //=============el totalCursesPerterm[i] no es, debe ser dos valores, el de aprobadas y el desaprobadas
              showTootltip(tooltip,estudiante, fechas[i], aniosPorPeriodo[i], d.y, totalAsignaturas[i], totalCursesPerTerm[i], averageApproved[i], avverageDesaproved[i], companerosAux[i].grade);
            d3.select(this).attr("r", 7);
            d3.select("#studentLegend").style("font-weight", "bold");
            //resalto los rectangulos con la fecha correspondiente
            let clas = fechaCom[i].getTime()+"";
            d3.selectAll('.rect-'+clas).style('stroke', 'black').style('stroke-width', '2px');
          })
          .on('mouseout', function (d, i){
            d3.select(this).attr("r", 5);
            d3.select("#studentLegend").style("font-weight", "normal");
            
            lightCourses(i, coursesIds, grades, false);
            hideTooltip(tooltip);
            //quito el resalto de los rectangulos con la fecha correspondiente
            let clas = fechaCom[i].getTime()+"";
            d3.selectAll('.rect-'+clas).style('stroke', 'none');//.attr('stroke-width', '2px');
          });
          //para mover los promedio en caso de que les tapen los puntos de student
          let dots = d3.selectAll('.student').nodes();//seleciono todos los puntos de estudiante
          let averages = d3.selectAll(".ptext").nodes(); //selecciono todos los texto donde estan los promedios de companeros
          let dotsPartner = d3.selectAll(".dp").nodes(); //selecciono todos los puntos de companeros
            for(i in dots){
              //console.log(d3.select(dots[i]).attr("cy"));
              let plus = parseFloat(d3.select(dots[i]).attr("cy"))+10;//seelcciono el cy mas 5
              let min = parseFloat(d3.select(dots[i]).attr("cy"))-10;//selecciono el cy menos 5
              let y = parseFloat(d3.select(averages[i]).attr('y')); //selecciono al pos y 
              let ydot = parseFloat(d3.select(dotsPartner[i]).attr("cy")); //selecciono el cy de partners
              //console.log((plus+10), min, y, ydot);
              //en caso de que y este entre plus y min va a mover al texto 
              if(y <= plus && y >= min){
                let value = parseInt(ydot)+20;//se va a mover debajo del punto 
                d3.select(averages[i]).attr('y', value);
              } 
            }

          var clickedCOmpanero = false;
          var clickedEstudiante = false;
      //agrego los promedis como texto
      // svgUno.selectAll(".average")
      //     .data(student.filter(function(d) {return d}))
      //     .enter().append("text")
      //     .attr("class", "averageGrade student")
      //     .attr("x", line.x())
      //     .attr("y", line.y())
      //     .text(function(d){return d.y;});

    let svgLegend = d3.select(".legendChart")
          .append("svg")
          .attr("class", "svgLegend")
          .attr("preserveAspectRatio", "xMinYMin meet")
          //.attr("viewBox", "0 0 "+(width + margin.left + margin.right)+" "+(height + margin.top + margin.bottom))
          .attr("viewBox", "0 0 190 225");
    //agrego la leyenda a los partners
    let svgPartners = svgLegend.append('g')
        .attr("id", "partnerLegend");
    svgPartners.append("line")
        .attr("x1", 5)
        .attr("y1", 40)
        .attr("x2", 35)
        .attr("y2", 40)
        .attr("stroke", "black")
        .attr("stroke-width", "3");
    svgPartners.append("circle")
        .attr("class", "dot-legend")
        .attr("cx", 20)
        .attr("cy", 40)
        .attr("r", 5)
        .attr("stroke", "black")
        .attr("fill", "black");
    svgPartners.append("text")
          .text("Compañeros de aula en cada asignatura")
          .attr("x", 45)
          .attr("y", 46)
          .attr("class", "partnerLegend");
    svgPartners.select(".partnerLegend")
          .call(wrap, 130);

    
          //para la legend de estudiante
    const posY = lineas*10+40;
    let svgStudent = svgLegend.append('g')
          .attr("id", "studentLegend");
    svgStudent.append("line")
        .attr("x1", 5)
        .attr("y1", posY)
        .attr("x2", 35)
        .attr("y2", posY)
        .attr("stroke", "steelblue")
        .attr("stroke-width", "3");
    svgStudent.append("circle")
        .attr("class", "dot-legend")
        .attr("cx", 20)
        .attr("cy", posY)
        .attr("r", 5)
        .attr("stroke", "steelblue")
        .attr("fill", "steelblue");
    svgStudent.append("text")
          .text("Estudiante")
          .attr("x", 45)
          .attr("y", posY+5)
          .attr("class", "studentLegend");

          
returnYear(aniosPorPeriodo);
    
      
}

function tooltipContent(){
  let content = "<div class='container-tooltip'> <div><strong>Periodo Académico: </strong> <label class='tip-periodo' id='tip-periodo'>45</label></div><div><br/></div><div><strong class='tip-estudiante' id='tip-estudiante'>{estudiante} </strong></div><div class='tip-margin'><strong class='ttooltip-font'>Promedio Asignaturas Cursadas: </strong><label class='tip-promedio' id='tip-promedio'>60</label></div><div><br/></div><div><strong>RENDIMIENTO DE ASIGNATURAS CURSADAS </strong></div><div class='tip-margin'><strong class='ttooltip-font'>Asignaturas Aprobadas: </strong><label class='tip-n-approbed' id='tip-n-approbed' ></label></div><div class='tip-margin'><strong class='ttooltip-font'>Promedio de Asignaturas Aprobadas: </strong><label class='tip-average-approbed' id='tip-average-approbed' ></label></div><div class='tip-margin'><strong class='ttooltip-font'>Promedio de Asignaturas Reprobadas: </strong><label class='tip-average-repprobed' id='tip-average-repprobed'></label></div><div><br/></div><div><strong> RENDIMIENTO DE COMPAÑEROS DE AULA</strong></div><div class='tip-margin'><strong class='ttooltip-font'>Nota Promedio: </strong><label class='tip-partners-average' id='tip-partners-average'></label></div></div>";
return content;
}
let lineas =1;
function wrap(text, width) {
  lineas =1;
    text.each(function() {
      var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = 1.1, // ems
          y = text.attr("y"),
          x = text.attr("x"),
          //dy = parseFloat(text.attr("y")),
          tspan = text.text(null).append("tspan").attr("x", x).attr("y", y);
      while (word = words.pop()) {
        line.push(word);
        tspan.text(line.join(" "));
        lineas ++;
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
         // console.log(lineNumber+"     "+lineHeight+ "  dy    "+dy);
          tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", (++lineNumber * lineHeight ) + "em").text(word);
        }
      }
    });
  }

  function lightCourses(index, ids, grades, typemouse){//type, true or false, true cuando es mouse over y false cuando es mouseout
      
    let idList = ids[index];//obtengo el listado de ids de curso solo para ese index(solo para ese semestre)
    let gradesList = grades[index];//obtengo el litsado de calificaciones para cada uno de los cursos anteriores
    //console.log(idList[0]+"   asignatura-"+idList[0]);
    
    for(var i=0; i<idList.length; i++){
        let id = "#asignatura-"+idList[i];
        //console.log(id);
        if(typemouse === true){
            let color = "green";
            if(gradesList[i]<60){
                color = "lightcoral";
            }
            let div = d3.select(id)
                .style("border", "5px "+color+" solid");
                //console.log(div);
                //showTootltip();
        }else {
            d3.select(id)
                .style("border", "1px black solid");
               // hideTooltip();
        }
    }
    
  }
////muestra el tooltip al hacer un mouseover sobre cada circulo o punto de la grafica, con la respectiva
////infomacion
  function showTootltip(tooltip,estudiante, periodo, anio, student_av, aprobadas, totalAsignaturas, aprobadasAverage, reprobadasAverage, partnersAverage){
    //console.log(partnersAverage);
    //console.log(tooltip.select("#tip-partners-average")
    //.html(Math.round(partnersAverage)));
    tooltip.select("#tip-periodo")
      .html(periodo+"-"+anio);
    d3.select("#tip-estudiante")
      .html(estudiante);
    tooltip.select("#tip-promedio")
      .html(student_av);
    tooltip.select("#tip-n-approbed")
      .html(aprobadas+" de "+Math.round(totalAsignaturas));
      tooltip.select("#tip-average-approbed")
      .html(Math.round(aprobadasAverage));
      tooltip.select("#tip-average-repprobed")
      .html(Math.round(reprobadasAverage));
      tooltip.select("#tip-partners-average")
      .html(Math.round(partnersAverage));   
  
  // let height = d3.select('.ttooltip').node().getBoundingClientRect().height;  
  // console.log(height);
  // if(height == 0){
  //   height = 245;  
  //   console.log(height);
  // }
  d3.select(".ttooltip")
      // .style("left", (d3.event.pageX + 5) + "px")		
      .style("left", (d3.event.pageX + 15) + "px")		
      //.style("top", (d3.event.pageY - height) + "px")
      //con esto obtengo la coordenada en y al dar clic que me devuelve en pixeles, luego resto usando css estos pixeles con el tamano del tooltip que es de 22 em
      // .style("top", 'calc('+d3.event.pageY+'px - 22em')
      .style("top", 'calc('+d3.event.pageY+'px - 13em')
      .transition(500)
      .style('opacity', 0.9)
      .style("display", 'block');
  }
  function hideTooltip(tooltip){
    d3.select(".ttooltip")
      .transition(500)
      .style('opacity', 0)
      .style('display', "none");
  }

  //funcion en la que calcula el anio por cada ciclo, es decir, de acuerdo a los meses calcula el anio
  function returnYear(years, dates){
    let yearsShow = [];
    let valAnt = 0;
    for(var i in years){
      if(valAnt !== years[i]){
        yearsShow.push(years[i]);
        
      }
      valAnt = years[i];
    }
    //console.log(yearsShow);
    return yearsShow;
  }

//   d3.select(window).on("resize", function() {
//     var chart = d3.select("#chartLineComparative"),
//  // aspect = chart.attr('width') / chart.attr('height');
//     aspect = 560 / 225;
//   //container = chart.node();
//   let container = d3.select(".chartLine");
//   let c = document.getElementsByClassName("chartLine")[0];
  
// //   let c = chart.select(function(){
// //       return this.parentNode
// //     });
//     console.log(container.node().getBoundingClientRect().width+"       ");
//     console.log(c.offsetWidth);
//     var targetWidth = c.offsetWidth*0.75;//attr('width');
//     var targetHeight = c.offsetHeight;
//     console.log(targetWidth + "  "+aspect);
//     let height = Math.round(targetWidth / aspect);
//     // if(height  < targetHeight){
//     //   console.log("Entraaaaaaaaaaa");
//     //   height = targetHeight;
//     // }
//     chart.attr("width", targetWidth);
//     chart.attr("height", height);
//    //chart.attr("height", targetHeight);
//     chart.attr("viewBox", '0 0 '+targetWidth+' '+targetHeight);
//  });