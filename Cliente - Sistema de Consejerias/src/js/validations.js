

export function maxLevelsToGraph(dataMax){
  // d3.select('body');
    let labelGrids = [];
    
    for(var i=0; i<=100;i+=10){
        if(dataMax<=i){
            labelGrids.push(i);
            return labelGrids;
        }else{
            labelGrids.push(i);
        }
    }
}

export function grades(){
    let grades = [];
    for(var i = 10; i <= 100; i+=10){
        grades.push(i);
    }
    return grades;
}

//calculo el promedio tanto del estudiante como de los companeros de aula para todos los semestres que el estudiante ya curso
export function calculaAverage(averagePerCourse, historyAcademic){
    ///en el average per course deberia ser un vector donde tenga los averague por course para cada curriculum que tenga el estudiante, eso se debe tomar en cuenta
    let averagesByTermStudents = [];
    let averagesByTermStudent = [];
    //array de meses 
    let months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sept','Oct','Nov','Dic'];
    //companeros de aula
    let comapenrosDeAulaAverage = new Map();
    let comapenrosDeAulaAverageSizePerNCourses = new Map();
    let promedioEstudiante = new Map();
    let promedioEstudianteSizePerNCouses = new Map();

    let promedioConFecha = new Map();
    let soloAnios = new Map();
    
    let asignaturasPorSemestre = new Map();
    let aprobadasPorSemestre = new Map();
    let desaprobadasPorSemestre = new Map();
    let naprobadasPorSemestre = new Map();
    let ndesaprobadasPorSemestre = new Map();
    let aniosPeriodos = [];//son solo los anios que petenecen a cada mes en el que inicio un nuevo periodo el estudiante
    let index = 0;//sirve para ajustar las posiciones en las que se va a guardar cada anio en aniosPeriodo

    let cursando = new Map();

    let averageGrade = new Map();
    let averageGradeTermsSize = new Map();
    for(var i=0; i<historyAcademic.length; i++){
        
        historyAcademic[i].map(his => {
            let asignaturas = [];
            let grades = [];
            // let date = new Date(Date.parse(his.term.start_Date));
            // let key = his.term.semester+""+months[date.getMonth()]+""+date.getFullYear();
            // if(averageGrade.has(key)){
            //     let au = averageGrade.get(key).data;
            //     //au.push(his.group_average_grade);
            //     au = au + his.group_average_grade;
            //     let d = {
            //         date: months[date.getMonth()],
            //         data: au
            //     }
            //     averageGrade.set(key, d);
            //     averageGradeTermsSize.set(key, averageGradeTermsSize.get(key) + 1);
            // }else{
            //     let d = {
            //         date: months[date.getMonth()],
            //         data: his.group_average_grade
            //     }
            //     averageGrade.set(key, d);
            //     averageGradeTermsSize.set(key, 1);
            // }
            // //let key = his.group_average_grade;
            // averagePerCourse.map(av => {
            //     if(his.course_id === av.course_id){

                  //  console.log("este es el mapa "+his.course_id);
                    let date = new Date(Date.parse(his.term.start_Date));
                    let llave = his.term.semester+""+months[date.getMonth()]+""+date.getFullYear();
                    if(comapenrosDeAulaAverage.has(llave)){
                        //console.log("si contiene");
                        comapenrosDeAulaAverage.set(llave, (comapenrosDeAulaAverage.get(llave)+his.group_average_grade))
                        comapenrosDeAulaAverageSizePerNCourses.set(llave, (comapenrosDeAulaAverageSizePerNCourses.get(llave)+1));
                        let fechaProm = {
                            date: months[date.getMonth()],
                            comp: date
                            //grade: promedioConFecha.get(llave).grade+av.average
                        };
                        promedioConFecha.set(llave, fechaProm);


                        
                    }else{
                        comapenrosDeAulaAverage.set(llave, his.group_average_grade);
                        comapenrosDeAulaAverageSizePerNCourses.set(llave, 1);
                        let fechaProm = {
                            date: months[date.getMonth()],
                            comp: date
                            //grade: av.average
                        };
                        promedioConFecha.set(llave, fechaProm);

                        
                            
                    }
                    if(promedioEstudiante.has(llave)){
                        promedioEstudiante.set(llave, (promedioEstudiante.get(llave)+his.grade));
                        promedioEstudianteSizePerNCouses.set(llave, (promedioEstudianteSizePerNCouses.get(llave)+1))

                        //para obtener los ids de cada asignatura y los agrupo por cada semestres igual al de los promdios
                        //asi se que asignaturas tomo el estudiante en cada periodo de que poseo el semestre
                        //si ya tiene la llave(osea el semestre) lo que va a hacer es a ....
                        
                        let ids = asignaturasPorSemestre.get(llave).arrayIds;//recupera el array de ids que posee
                        ids.push(his.course_id);//agrega el nuevo id al array recuperado
                        let notas = asignaturasPorSemestre.get(llave).grades;//recupera el array de notas de cada asignatura
                        notas.push(his.grade);
                        let dataAsignaturas = {//da el formato 
                            arrayIds: ids,
                            grades: notas
                        }
                        asignaturasPorSemestre.set(llave, dataAsignaturas);//vuelve a agregar el array recuperado pero con el nuevo id agregado
                        let cursandoArray = cursando.get(llave).cursando;
                        let cursand = {};
                        if(his.state === "CURSANDO"){
                            cursandoArray.push(true);
                            cursand = {
                                arrayIds: ids,
                                cursando: cursandoArray
                            }
                        }else{
                            cursandoArray.push(false);
                            cursand = {
                                arrayIds: ids,
                                cursando: cursandoArray
                            }
                        }
                        cursando.set(llave, cursand);
                        
                    }else{
                        promedioEstudiante.set(llave, his.grade);
                        promedioEstudianteSizePerNCouses.set(llave, 1);
                        //como aqui en el else siempre sera una nueva llave, significa que tambien sera un nuevo periodo, por lo cual guardo el anio
                        aniosPeriodos[index] = date.getFullYear();
                        index++;//incremento el index 
                        // if(asignaturasPorSemestre.has(llave)){
                        //     console.log("entraaaaaaaaaaaaaaaaaa");
                        //     let ids = asignaturasPorSemestre.get(llave).arrayIds;//recupera el array de ids que posee
                        //     ids.push(his.course_id);//agrega el nuevo id al array recuperado
                            
                        //     let dataAsignaturas = {//da el formato 
                        //         arrayIds: ids
                        //     }
                        //     asignaturasPorSemestre.set(llave, dataAsignaturas);//vuelve a agregar el array recuperado pero con el nuevo id agregado
                        // }else{//en caso de que no poseea la llave significa que o bien es el primero(i=0) o bien es un nuevo semestre
                        asignaturas = [];//reseteo el asignaturas
                        grades = [];
                        asignaturas.push(his.course_id);//agrego el id al array
                        grades.push(his.grade);//agrego la nota de la asignatura
                        let dataAsignaturas = {//da formato
                            arrayIds: asignaturas,
                            grades: grades 
                        }
                        asignaturasPorSemestre.set(llave, dataAsignaturas);//agrega el array con la llave respectiva en el mapa

                        let cursand = {};
                        let cursandoArray = [];
                        
                        if(his.state === "CURSANDO"){
                            cursandoArray.push(true);
                            cursand = {
                                arrayIds: asignaturas,
                                cursando: cursandoArray
                            }
                        }else{
                            cursandoArray.push(false);
                            cursand = {
                                arrayIds: asignaturas,
                                cursando: cursandoArray
                            }
                        }
                        cursando.set(llave, cursand);
                    //}
                    }
                    if(date.getMonth() == 1 || date.getMonth() == 2){
                        if(!soloAnios.has(date.getFullYear())){
                            soloAnios.set(date.getFullYear(), date.getFullYear());
                        }
                        
                    }

                    //aprobadas
                    if(aprobadasPorSemestre.has(llave)){
                        if(his.grade >= 60){
                            aprobadasPorSemestre.set(llave, aprobadasPorSemestre.get(llave) + his.grade);
                            naprobadasPorSemestre.set(llave, naprobadasPorSemestre.get(llave) + 1);
                        }else{
                            if(desaprobadasPorSemestre.has(llave)){//si tiene la llave le sumo al valor que ya posee
                                desaprobadasPorSemestre.set(llave, desaprobadasPorSemestre.get(llave) + his.grade);
                                ndesaprobadasPorSemestre.set(llave, ndesaprobadasPorSemestre.get(llave) + 1);
                            }else{
                                desaprobadasPorSemestre.set(llave, his.grade);//si no tiene la llave solo seteo el valor del grade
                                ndesaprobadasPorSemestre.set(llave, 1);
                            }
                        }
                    }else{ //si no tiene la llave significa q es la primera vez que esta entrando a ese term
                        // for (var i in student){
                        //     if(his    //   if(student[i] === null){rade >= 60){
                        //         ap    //     comapaneros[i] = null;badasPorSemestre.set(llave, his.grade);
                        //         na    //   }obadasPorSemestre.set(llave, 1);
                        if(his.grade >= 60){
                            aprobadasPorSemestre.set(llave, his.grade);
                            naprobadasPorSemestre.set(llave, 1);
                        }else{    // }
                            if(desaprobadasPorSemestre.has(llave)){
                                desaprobadasPorSemestre.set(llave, desaprobadasPorSemestre.get(llave) + his.grade);
                                ndesaprobadasPorSemestre.set(llave, ndesaprobadasPorSemestre.get(llave) + 1);
                            }else{
                                desaprobadasPorSemestre.set(llave, his.grade);//si no tiene la llave solo seteo el valor del grade
                                ndesaprobadasPorSemestre.set(llave, 1);
                            }
                        }
                    }
               
            //     }
                
            // });
            
        });

    }
    // averageGrade.forEach(function(value, key, map){
    //     console.log(key, value, averageGradeTermsSize.get(key));
    // });
    // comapenrosDeAulaAverage.forEach(function(value, key, map){
    //     console.log("Estos son los arrays de ids: ");
    //     console.log(key);
    //     console.log(value);
    // });
   // map.set(1,"holap");
    //console.log("Tamano "+comapenrosDeAulaAverage.size+" "+comapenrosDeAulaAverage.get('3S')+" "+promedioEstudiante.size);
    //console.log("este es el promedio con fecha: "+promedioConFecha);
    let companerosData = [];
    let studentData = [];
    let datesData = [];
    let coursesId = [];
    let gradesPerCourse = [];
    let statesPerCourse = [];
    ///el dataset donde se van a guardar absolutamente todos los datos necesarios
    let dataset = [];
    //divido para el numero de asignaturas que posee cada semestre
    comapenrosDeAulaAverage.forEach(function (value, key, map){
        comapenrosDeAulaAverage.set(key, value/comapenrosDeAulaAverageSizePerNCourses.get(key));
       // console.log(key, value, promedioEstudiante.get(key));
    });
    //divido para el numero de asignaturas por semestre para el estudiante y, al mismo tiempo, guardo los datos en arrays para graficar.
    let cont = 0;
    let aprobadasXSemestre = [];
    let desaprobadasXSemestre = [];
    let naprobadasXSemestre = [];
    promedioEstudiante.forEach(function(value, key, map){
        promedioEstudiante.set(key, value/promedioEstudianteSizePerNCouses.get(key));
        companerosData[cont] = parseInt(Math.round(comapenrosDeAulaAverage.get(key)));
        studentData[cont] = parseInt(Math.round(promedioEstudiante.get(key)));
        datesData[cont] = promedioConFecha.get(key).date;
        
        coursesId[cont] = asignaturasPorSemestre.get(key).arrayIds;
        gradesPerCourse[cont] = asignaturasPorSemestre.get(key).grades;
        statesPerCourse[cont] = cursando.get(key).cursando;
        let daC = {
            id: 1,
            tipo: "Compañeros de aula en cada asignatura",
            fecha: promedioConFecha.get(key).comp,
            data: companerosData[cont]
            
        }
        let daE = {
            id: 2,
            tipo: "Estudiante",
            fecha: promedioConFecha.get(key).comp,
            data: studentData[cont],
            totalAsignaturasEnCiclo: promedioEstudianteSizePerNCouses.get(key) //total de asignaturas en el periodo
        }
        // console.log(daC);
        // console.log(daE);
        dataset.push(daC);
        dataset.push(daE);

        //aprobadas por semestre
        if(aprobadasPorSemestre.has(key)){
            aprobadasPorSemestre.set(key, aprobadasPorSemestre.get(key) / naprobadasPorSemestre.get(key));    
            aprobadasXSemestre[cont] = aprobadasPorSemestre.get(key);
            naprobadasXSemestre[cont] = naprobadasPorSemestre.get(key);
        }else{
            aprobadasXSemestre[cont] = 0;
            naprobadasXSemestre[cont] = 0;
        }
        //desaprobadas
        if(desaprobadasPorSemestre.has(key)){
            desaprobadasPorSemestre.set(key, desaprobadasPorSemestre.get(key) / ndesaprobadasPorSemestre.get(key));
            desaprobadasXSemestre[cont] = desaprobadasPorSemestre.get(key);    
        }else{
            desaprobadasXSemestre[cont] = 0;
        }
        cont++;
        //console.log("Este es el mapeado de promedio con fecha "+promedioConFecha.get(key).date+" "+promedioConFecha.get(key).grade);
        
    });
    
    //console.log(aprobadasXSemestre);
    //console.log(desaprobadasXSemestre)
    // cont = 0;
    // //pongo en un array el promedio de las aprobadas pro semestre
    // aprobadasPorSemestre.forEach(function(value, key, map) {
    //     aprobadasPorSemestre.set(key, value / naprobadasPorSemestre.get(key));
    //     aprobadasXSemestre[cont] = aprobadasPorSemestre.get(key);
    //     cont++;
    // });
    // //pongo en un array el promedio de las desaprobadas pros emestre
    // cont = 0;
    // desaprobadasPorSemestre.forEach(function(value, key, map){
    //     desaprobadasPorSemestre.set(key, value / ndesaprobadasPorSemestre.get(key));
    //     desaprobadasXSemestre[cont] = desaprobadasPorSemestre.get(key);
    //     cont++;
    // });
    let soloAniosArray = [];
    soloAnios.forEach(function(value, key, map){
        soloAniosArray.push(key);
    });
    //console.log(datesData[0]);
    const data = {
        partners: {name: "Compañeros de aula en cada asignatura",
                   data: companerosData
                  },
        student: {name: "Estudiante",
                  data: studentData}
    }
    //console.log(dataset);
   // console.log(aniosPeriodos);
    //console.log("Solo los anios "+dataset[1].tipo+" "+dataset[1].fecha+"  "+dataset[1].data);
    let retorna = [data, datesData, soloAniosArray, dataset, coursesId, gradesPerCourse, aprobadasXSemestre, desaprobadasXSemestre, naprobadasXSemestre, aniosPeriodos, statesPerCourse];
    // console.log(data.student.data.length);
    return retorna;
}
/**
 * metodo que calcula el promedio general del estudiante desde que inicio hasta donde se encuentray lo retorna 
 * @param {*id, tipo, fecha, data} dataset parametro que contiene todos los promedios por semestre del estudiante y tambien de sus companeros de aula de cada semestre
 */
export function returnGeneralAverage(dataset){
    let average = 0;
    let cont = 0;
    dataset.map(data => {
        if(data.id == 2){//si es estudiante
            average += data.data;
            cont++;
        }
    });
    return average/cont;
}

export function retakenCoursesTimes(data){
    let retaken = 0;
    data.forEach(function(value, key, map){
        if(value.veces > 1){
            retaken ++;
        }
    });
    return retaken;
}

export function returnDigitsFromString(text){
        var num = text.replace(/[^0-9]/g, ''); 
        return parseInt(num,10); 
}