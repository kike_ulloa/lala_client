import React from 'react';
import StudentMenuBar from '../components/StudentMenuBar';
import PopUpAsignatura from '../components/PopUps/PopUpAsignatura';
import PopUpAsignaturaGrow from '../components/PopUps/PopUpAsignaturaGrow';
import {calculaAverage} from '../js/validations';
import AverageStudentPartners from '../components/charts/LineChartAverageStudentPartners';
import { mostrarGrafico } from './showLineChart';
import { showLineChart } from './showLineComparativeChart';
import {individualCoursesChart} from './showRectsChart';
import {addComplexity} from './showRectsChart';
import {nRetakeCourses} from './showRectsChart';
import {addText} from './showRectsChart';
import Switch from 'react-switch';
import {createChart} from './workLoadChart';
import {showLineChartFromWorkLoad} from './workLoadChart';
import * as d3 from 'd3';
import {returnDigitsFromString} from '../js/validations';
//import "../node_modules/jquery/dist/jquery.min.js";
import jquery from 'jquery';
let $ = window.jQuery = jquery;
const request = require('request-promise')
const btoa = require('btoa')


const ISSUER = 'https://dev-878998.oktapreview.com/oauth2/ausj08qk5vYaB8HWc0h7';
const TEST_CLIENT_ID = '0oaj19qfa1c2F66WD0h7';
const TEST_CLIENT_SECRET = 'LxhIkvex-1Z1JAx7vhW9LL4VhSYb-yuSlvUGUPlp';
const DEFAULT_SCOPE = '';
//const ENDPOINT = 'http://localhost:3000/api/v1/students/00918df7-5843-c53e-eec9-1595af6fdcd9?program=223';
//const ENDPOINT = 'http://localhost:3000/api/v1/students/E26?program=1';
let ENDPOINT = 'http://localhost:3000/api/v1/getStudentCurriculum/';

let carrera="";
let estudiante="";
let email = "";
let numeroCiclos = "";
//let materia ="default";
let ciclos=[];
let anios=[];

let totalAsignaturasCursadasPerdidas = new Map();
let totalAsignaturasAprobadas = 0;

let xHasValue = false; //para cuando seleccione workload y el valor d ela variable x ya aumente para graficar los nuevos cuadros
let cursosTomados = []; //para cuando seleccione workload necesito saber el numero de vez que tomaria la asignatura en caso de que se la seleccione

let isSelected = new Map();

let id_estudiante = 0;
let chang = false;//variable para cuando cambia el switch a complejidad (sirve para cuando se hace un resize de la pantalla que se actualice el switch y no se quede en una pos fija el circulo del switch)
let changeWorkload = false;//variable para cuando cambia el switch a complejidad (sirve para cuando se hace un resize de la pantalla que se actualice el switch y no se quede en una pos fija el circulo del switch)
class Dash extends React.Component {
    constructor (props){
        super(props);
        this.state = { data: [], 
                        materia: 'default',
                        showPopUp: false,
                        popUpPos: [],
                        style: {
                            top:25,
                            left:25
                        },
                        aprobacion: "base",
                        calificaciones: [],
                        datagraph: [],
                        positionStudentGraph: 0,
                        showPopUpGrow: false,
                        aprovechamientoOne: 0,
                        interciclo: 0,
                        aprovechamientoTwo: 0,
                        final: 0,
                        suspension: 0,
                        notaF: 0,
                        faltas: 0,
                        faprobacion: '',
                        hteoricas: 0,
                        htrabajoautonomo: 0,
                        hpracticas: 0,
                        nStudentsGroup: 0,
                        dat: {
                            partners: {
                                name: "",
                                data: []
                            },
                            student: {
                                name: "",
                                data: []
                            }
                        },
                        dates: [], //las fechas (los meses) de cada periodo academico (Feb, Sept, Feb, Sept.....)
                        anios: [], //los anios que corresponden a cada mes de febrero y septiembre por ejm, entre febrero y septiembre es 2006, no cuenta si es 2006 - 2007
                        changed: false,
                        requisites: [],
                        dependents: [],
                        idAnterior: 0,
                        asignaturasSeleccionadas: new Map(), //sirve para guardar todas las asignaturas seleccionadas para la carga de trabajo
                        todasAsignaturas: [], //sirve para guardar todas las horas de trabajo practico, teorico y autonomo por asignatura de la malla
                        changedWorkload: false,
                        lockedCourses: new Map(),
                        promediosStudent: [],
                        idCurso: 0
                    };
       // console.log(this.props.idStudent);
        this.test();
        chang =false;
        
    }
    async test() {
 // const token = btoa(`${TEST_CLIENT_ID}:${TEST_CLIENT_SECRET}`)
  try {
    //   const { token_type, access_token } = await request({
    //   uri: `${ISSUER}/v1/token`,
    //   json: true,
    //   method: 'POST',
    //   headers: {
    //     authorization: `Basic ${token}`,
    //   },
    //   form: {
    //     grant_type: 'client_credentials',
    //     scope: DEFAULT_SCOPE,
    //   },
    // })
   // console.log("Ejecutando");

    ENDPOINT="http://localhost:3000/api/v1/getStudentCurriculum/?studentid="+this.props.idStudent;
    //reinicializo variables globales
    carrera="";
    estudiante="";
    email = "";
    numeroCiclos = "";
    //let materia ="default";
    ciclos=[];
    anios=[];

    totalAsignaturasCursadasPerdidas = new Map();
    totalAsignaturasAprobadas = 0;

    xHasValue = false; //para cuando seleccione workload y el valor d ela variable x ya aumente para graficar los nuevos cuadros
    cursosTomados = []; //para cuando seleccione workload necesito saber el numero de vez que tomaria la asignatura en caso de que se la seleccione

    isSelected = new Map();

    id_estudiante = 0;

    const response = await request({
      uri: ENDPOINT,
      json: true,
      rejectUnauthorized: false,
    //   headers: {
    //     authorization: [token_type, access_token].join(' '),
    //   },
    })
    
    if(response === null || response === undefined || response.length === 0){
        let charging = document.getElementById("ipl-progress-indicator");
            charging.classList.add("available");
            d3.select(".centro").style('display', 'none');
            d3.select('.not-found').style('display', 'block');
        return;
    }
    // //console.log(JSON.parse(response).student_curriculum.curriculum.program_term.program_course,access_token);
     //console.log(response[0].student_curriculum[0].curriculum.program_term[0].program_course);
     //console.log(response[0].student_curriculum[0].curriculum.program_term);

     //para la complejida que necesito en la grafica de cuadritos
     this.setState({data: []});
     anios = [];
     let complejidadPorCurso =  new Map();
     //student
        let averagePerCoursePerTerm = [];
        let contador = 0;
        let historiesPerStudent = [];//aqui se agregan todos los historiales que tiene el estudiante para cada mallla que haya tomado, si tomo dos mallas, dos historiales tendra.
        let cargaTrabajoArray = []; 
        
    for (var i=0;i<response.length;i++){
         estudiante = response[i].name;//nombre del estudiante
         email = response[i].email;
         //console.log(email);
         let idStudent = response[i].id;
         id_estudiante = response[i].id;
         //console.log ("Este es el id del estudiante "+idStudent);
         //student_curriculum
         for(var j=0; j<response[i].student_curriculum.length;j++){
            //curriculum
            carrera = response[i].student_curriculum[j].curriculum.program.name;//nombre de la carrera
            let idcurriculum = response[i].student_curriculum[j].curriculum.id;
            //si el numero de ciclos es 10 entonces el 
            //FONT-SIZE ES 0.75VW, si es 12 u 11 entonces es 0.6vw
            numeroCiclos = response[i].student_curriculum[j].curriculum.program_term.length; 
            //llamo a la fincion que me devuelve el historic academico de los estudiantes
            //end point para el hostorico de los estudiantes
            const ENDPOINT2 = 'http://localhost:3000/api/v1/getStudentAcademics/?studentid='+this.props.idStudent+'&curriculumid='+idcurriculum; //el historico academico del estudiante
            const history = await request({
                uri: ENDPOINT2,
                json: true,
                rejectUnauthorized: false,
              //   headers: {
              //     authorization: [token_type, access_token].join(' '),
              //   },
              })
           //   console.log("este es el tam de hostory "+history[0].state);
           //console.log("numero de ciclos: "+numeroCiclos);
            //program_term
            historiesPerStudent.push(history);
            for(var h=0;h<numeroCiclos; h++){
                //program course + course
                let materiasPorCiclo = [];
                //este for sirve pa crear un div para cada materia conjuntamente con la barra base pintada de un color especifico de acuerdo a si perdio o aprovo o esta cursando al materia.
                for(var k = 0; k<response[i].student_curriculum[j].curriculum.program_term[h].program_course.length; k++){
                    let asignatura = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_nombre;
                    let courseid = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id;
                    let v = asignatura;
                    let cursoid=0;
                    let termid = 0;
                    let grupo = '';
                    let cont = 0;

                    //datos necesarios del hostorial academico para mostrar en el popup de la asignatura mas detallado
                    let notaUno = 0.0;
                    let notaMedia = 0.0;
                    let notaDos = 0.0;
                    let notaFinal = 0.0;
                    let retake = 0.0;
                    let missed = 0;
                    let formaaproba = '';
                    let finalG = 0.0;
                    let nGroup =0;

                   ///mapa para saber cuantas asignaturas ha cursado
                   //como key tendra el id de la asignatura, como valor, tendra el numero de veces repetidas
                   //con ello tendre cuantas asignaturas curso y cuantas veces repitio sumando los valores del mapa
                   //let cursadasPerdidas = new Map();
                   //totalAsignaturasCursadasPerdidas = new Map();
                    ///chequeo si ha perdido y cuantas veces a repetido la asignatura recorriendo el hitoryacademic
                    const hi = history.map(his => {
                        if(his.course_id === response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id){
                            //if(his.state.toUpperCase() === 'RREPROVADO'){
                              //  console.log("este es el mapeados: "+his.grade+" "+his.state+ " cont "+cont);
                                cursoid = his.course_id;
                                termid = his.term_id;
                                grupo = his.group;
                                idcurriculum = his.curriculum_id;
                                cont=cont+1;

                                ///informacion necesaria para mostrar en el popUp mas grande de la asignatura
                                notaUno = his.workshop1_grade;
                                notaMedia = his.midterm_grade;
                                notaDos = his.workshop2_grade;
                                notaFinal = his.final_exam_grade;
                                retake = his.retake_grade;
                                missed = his.n_missed_classes;
                                formaaproba = his.approval_method;
                                finalG = his.grade;
                                nGroup = his.n_students_in_group;
                                //console.log(his.term.start_Date);
                                const retorna= {
                                    grade: Math.round(his.grade),
                                    state: his.state,
                                    course_id: his.course_id,
                                    start_Date: new Date(Date.parse(his.term.start_Date))
                                };
                               // console.log(retorna.start_Date);
                                //en caso de que ya tenga el mapa a esa asignatura, significa que perdio y que repitio
                                if(totalAsignaturasCursadasPerdidas.has(his.course_id)){
                                    let value = {
                                        veces: totalAsignaturasCursadasPerdidas.get(his.course_id).veces+1,
                                        aprobacion: his.state
                                    }
                                    totalAsignaturasCursadasPerdidas.set(his.course_id, value);
                                }else{
                                    let value = {
                                        veces: 1,
                                        aprobacion: his.state
                                    }
                                    totalAsignaturasCursadasPerdidas.set(his.course_id, value);
                                }
                                return retorna;
                            //}
                        }else{
                           // console.log("entral cuando no es igual");
                          //  cursoid=0;
                            return null;
                        }
                    });
                    let grades = [];
                    let lost = [];
                    let gradesgrow= [];
                    let lostgrow=[];
                  //  console.log("Este es el hi del mapeado: "+hi.length);
                    // for(var t =0; t<hi.length;t++){
                    //     console.log(t);
                    // }
                    let aprobada = "NO CURSADA"//variable para saber si aprobo, reprobo o aun no cursa la asignatura
                    let aprobacion="base";//con esta variable defino la clase a la que pertence, si es aprobado, se pinta de color lightgreen, caso contrario, lightcoral
                    ///mapa para agregar las perdidas, en caso de que el mapa ya contiene una perdida significa que la segunda es la ultima asignatura cursada, por lo cual tendra una clase last
                    let repite = new Map();
                    let fecha= '';
                    let fechaDos = '';
                    let fechas = new Map();
                    if(hi.length>0){
                        let bandera = false;
                        for(var x = 0; x < hi.length; x++){
                            if(hi[x]!=null){
                                if(hi[x].course_id === response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id){
                                    if(hi[x].start_Date !== undefined){
                                        fechaDos = hi[x].start_Date;
                                        
                                    }
                                }
                                if(hi[x].state === "REPROBADO"){
                                    let classP = "perdida";
                                    let classPG = "perdidagrow";
                                    let classNum = "numero";
                                    if(repite.has(hi[x].course_id)){
                                        classP += " last";
                                        classPG += " last";

                                        classNum += " l";

                                        repite.set(hi[x].course_id, repite.get(hi[x].course_id) + 1);
                                        
                                    }else{
                                        // repite.set(hi[x].course_id, hi[x].course_id);
                                        repite.set(hi[x].course_id, 1);
                                    }
                                    let idc = 0;
                                    
                                    if(hi[x] !== undefined){
                                        idc = hi[x].course_id;
                                        fecha = hi[x].start_Date;
                                        if(fechas.has(hi[x].course_id)){
                                            let f = fechas.get(hi[x].course_id);
                                            f.push(hi[x].start_Date);
                                            fechas.set(hi[x].course_id, f);
                                        }else{
                                            fechas.set(hi[x].course_id, [hi[x].start_Date]);
                                        }
                                    }
                                   // console.log(fechas);
                                    //grades.push(<div className="perdidas"><div className="perdida"><span className="numero">{hi[x].grade}</span></div></div>);
                                    lost.push(<div className={classP} onMouseOver={(e) => this.highlightRectHover(e, idc, true, fechas)} onMouseOut={(e) => this.highlightRectHover(e, idc, false, fechas)}><span className={classNum}>{hi[x].grade}</span></div>);
                                    lostgrow.push(<div className={classPG}><span className="numero">{hi[x].grade}</span></div>);
                                    aprobacion="base reprobado";
                                    aprobada = "REPROBADA";
                                }else if(hi[x].state== "APROBADO"){
                                    grades.push(<div className="perdidas">{lost.length ? lost : ''}</div>);
                                    gradesgrow.push(<div className="perdidas">{lostgrow.length ? lostgrow : ''}</div>);
                                    bandera = true;
                                    grades.push(<div className="finalGrade"><div className="grade"><span className="numero">{hi[x].grade}</span></div></div>)
                                    gradesgrow.push(<div className="finalGrade"><div className="gradegrow"><span className="numero">{hi[x].grade}</span></div></div>)
                                    lost = [];   
                                    lostgrow =[];
                                    aprobacion = "base aprobado";
                                    aprobada = "APROBADA"
                                    ///aumento el numero de asignaturas aproibadas, al final tendre el total de asignaturas aprobadas
                                    totalAsignaturasAprobadas+=1;
                                }else if(hi[x].state === "CURSANDO"){
                                    grades.push(<div className="perdidas">{lost.length ? lost : ''}</div>);
                                    gradesgrow.push(<div className="perdidas">{lostgrow.length ? lostgrow : ''}</div>);
                                    bandera = true;
                                    grades.push(<div className="finalGrade"><div className="grade"><span className="numero"></span></div></div>)
                                    gradesgrow.push(<div className="finalGrade"><div className="gradegrow"><span className="numero"></span></div></div>)
                                    lost = [];   
                                    lostgrow =[];
                                    aprobacion = "base cursando";
                                    aprobada = "CURSANDO"
                                    ///aumento el numero de asignaturas aproibadas, al final tendre el total de asignaturas aprobadas
                                    totalAsignaturasAprobadas+=1;
                                }
                            }
                        }
                        if(bandera === false && lost.length>0){//en caso de q solo haya desaprobado y nunca aprobado esa masignatura
                            grades.push(<div className="perdidas">{lost.length ? lost : ''}</div>);
                            gradesgrow.push(<div className="perdidas">{lostgrow.length ? lostgrow : ''}</div>);
                            grades.push(<div className="finalGrade"><div className="grade"><span className="numero"></span></div></div>)
                            gradesgrow.push(<div className="finalGrade"><div className="gradegrow"><span className="numero"></span></div></div>)
                                    
                            aprobacion = "base reprobado";
                        }
                    }
                    ///seteo en la variable global el mapa con todas las asignaturas que curso, asi como tambien con el numero de veces que
                    //repitio cada una.
                   //totalAsignaturasCursadasPerdidas = cursadasPerdidas;
                    ///debo mandar a poner la ultima nota en la seccion final grade y quitar de la sperdidas si esdque no ha aprobado
                    //primero creo una funcion en la que clone objetos completamente, tanto padres como hijos, mas adelante se explica el porque
                    let deepCopy = arr => {
                        if (typeof arr !== 'object') return arr//si es un objeto retorna el mismo objeto
                        if(arr!=null)//si es diferente de nulo
                            if(arr.pop) return [...arr].map(deepCopy)//si tiene objetos en la promera posicion (lengh del array mayor a cero) va a clonar dicho array [ ...arr] con eso clona y cada objeto de ese array manda a clonar haciendo llamadas recursivas
                        //else return React.createRef();
                        const copy = {}
                        for (let prop in arr) //luego de clonar cada hijo recorro el array
                          copy[prop] = deepCopy(arr[prop])//clono lo faltante mediante recursividad
                        return copy
                      }
                    let clone = "";//varibale para clonar
                    let cloneGrow= ""; //variable para clonar para el popup
                    if(grades !== undefined && grades !== null){//si el componecte react de calificaciones es diferente de indefinido y nulo(en caso de que aun no tome esas asignaturas)
                        //grades[0] son las perdidas(es el div en el que estan los divs y los numeros de las notas perdidas), grades[1] es donde esta la nota de apribacion (finalGrade)
                        if(grades[1] !== undefined){//si la posicion 1 de las calificaciones es diferente de indefinido (practicamente el grades es un array de componentes react)
                            if(grades[1].props.children.props.children.props.children === undefined){///si la califiacion o la nota final es igual a indefinida (aun no aprueba, por lo cual se debe tomar la ultima nota de las perdidas yubicar en nota final)
                                 if(grades[0].props.children.length > 1 && aprobada !== "CURSANDO"){//si hay dos notas en perdidas y no este cursand la asignatura
                                    // console.log(grades[0].props.children[1].props.children.props.children);
                                    // console.log(grades[0].props.children[1].props.onMouseOut);
                                    //tests = { ...grades[1] };
                                    //tests = JSON.parse(JSON.stringify(grades[1]));
                                     clone = deepCopy(grades[1]);//clono el array, esto porque no se pueden agregar mas campos a un componente react, eso es una limitaciond e react y yo necesito agregar la nota y los eventos del mouse
                                     cloneGrow = deepCopy(gradesgrow[1]);
                                     // // React.cloneElement(grades[1], tests, null);
                                    // let val = React.Children.map(grades[1].props.children.props.children.props.children, (child) => React.cloneElement(child, grades[0].props.children[1].props.children.props.children, null));
                                    // console.log(val);
                                    let val = grades[0].props.children[1].props.onMouseOut;//guardo el evento del mouse out
                                    clone.props.children.props.children.props.children = grades[0].props.children[1].props.children.props.children;//la calificacion de la ultima vez q perdio agrego como ultima nota
                                    cloneGrow.props.children.props.children.props.children = gradesgrow[0].props.children[1].props.children.props.children;//la calificacion de la ultima vez q perdio agrego como ultima nota
                                    
                                    clone.props.children.props.onMouseOut = val;//agrego el evento de mouseout 
                                    clone.props.children.props.onMouseOver = grades[0].props.children[1].props.onMouseOver; //agrego el evento del mouseOver
                                    clone.props.children.props.className = "grade la";//agrego la clase la, esto porque al momento de hacer mouse over verifico si exiist una l en las clases, en caso de que asi sea, va a marcar la ultima vez q curso la asignatura
                                    clone.props.children.props.children.props.className = "numero la";//lo mismo que la linea anterior, pero esta vez al span

                                    grades[0].props.children[1] = '';//elimino la ultima nota de las perdidas
                                    gradesgrow[0].props.children[1] = '';//elimino la ultima nota de las perdidas
                                    grades[1] = clone;//ahora indico que el array anterior va a ser el array clonado y modificado.
                                    gradesgrow[1] = cloneGrow;//ahora indico que el array anterior va a ser el array clonado y modificado.
                                    //grades[1].props.children.props.children.props.children = grades[0].props.children[1].props.children.props.children;
                                   //  tests = grades[0].props.children[1].props.children.props.children;
                                 }else if(grades[0].props.children.length === 1 && aprobada !== "CURSANDO"){//en caso de que haya perdido solo una vez, esa nota va a ir como nota final siempre y cuando no este cursando la asignatura
                                    clone = deepCopy(grades[1]);//clono el array, esto porque no se pueden agregar mas campos a un componente react, eso es una limitaciond e react y yo necesito agregar la nota y los eventos del mouse
                                    cloneGrow = deepCopy(gradesgrow[1]);//clono el array, esto porque no se pueden agregar mas campos a un componente react, eso es una limitaciond e react y yo necesito agregar la nota y los eventos del mouse
                                    // // React.cloneElement(grades[1], tests, null);
                                    // let val = React.Children.map(grades[1].props.children.props.children.props.children, (child) => React.cloneElement(child, grades[0].props.children[1].props.children.props.children, null));
                                    // console.log(val);
                                    let val = grades[0].props.children[0].props.onMouseOut;//guardo el evento del mouse out
                                    clone.props.children.props.children.props.children = grades[0].props.children[0].props.children.props.children;//la calificacion de la ultima vez q perdio agrego como ultima nota
                                    cloneGrow.props.children.props.children.props.children = gradesgrow[0].props.children[0].props.children.props.children;//la calificacion de la ultima vez q perdio agrego como ultima nota
                                    
                                    clone.props.children.props.onMouseOut = val;//agrego el evento de mouseout 
                                    clone.props.children.props.onMouseOver = grades[0].props.children[0].props.onMouseOver; //agrego el evento del mouseOver
                                    // clone.props.children.props.className = "grade la";//agrego la clase la, esto porque al momento de hacer mouse over verifico si exiist una l en las clases, en caso de que asi sea, va a marcar la ultima vez q curso la asignatura
                                    // clone.props.children.props.children.props.className = "numero la";//lo mismo que la linea anterior, pero esta vez al span
                                    
                                    ///creo nuevas referencias a los objetos clonados ya que en una parte del metodo dep pregunto si es diferente de nul
                                    ///pero no retorna nada cuando no es diferente de nulo por lo cual se pierde la referencia dentro del contexto react de los elementos
                                    clone.props.children.props.children.ref = React.createRef();
                                    clone.props.children.ref = React.createRef();
                                    clone.ref = React.createRef();
                                    cloneGrow.props.children.props.children.ref = React.createRef();
                                    cloneGrow.props.children.ref = React.createRef();
                                    cloneGrow.ref = React.createRef();


                                    grades[0].props.children[0] = '';//elimino la ultima nota de las perdidas
                                    gradesgrow[0].props.children[0] = '';//elimino la ultima nota de las perdidas

                                    grades[1] = clone;//ahora indico que el array anterior va a ser el array clonado y modificado.
                                    gradesgrow[1] = cloneGrow;//ahora indico que el array anterior va a ser el array clonado y modificado.
                                    //console.log(clone);
                                 }
                                // console.log(grades[0].props.children.length > 1);

                                // tests = grades[0]
                            }
                            
                        }
                        
                    }
                    //React.cloneElement(grades[1], grades[1], grades[1]);
                   // let test2 = deepCopy(grades[1]); 
                    
                    
                    //grades[1] = test2;
                   // console.log(grades, grades[1].props.children.props.children.props.children, grades.length);
                    // console.log(d3.select(grades[grades.length-1]).nodes()[0]);//.nodes());
                    // let nodo = d3.select(grades[grades.length-1]).node();
                    // let ndos = d3.selectAll(nodo+">'div'");
                    // console.log(ndos);
                    //para saber el area y colorear al area a la que pertenece la asignatura (basica, formacion, titulacion)
                    let area="main_div "+response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].area.toLowerCase();
                    let calificacion = gradesgrow;
                    let idAsignatura = "asignatura-"+response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id;
                    //let idAsignatura = "asignatura-"+response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_codigo;
                    let teorical = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].theoretical_hours;
                    let practical = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].practical_hours;
                    let autonomous = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].autonomous_hours;
                    
                    //pongo la infor necesaria en formato ma sfacil para pasar los parametros
                    let informacion = {
                        notaOne: notaUno,
                        notaMiddle: notaMedia,
                        notaTwo: notaDos,
                        final: notaFinal,
                        retakeGrade: retake,
                        fgrade: finalG,
                        missedClasses: missed,
                        approval: formaaproba,
                        teoricas: teorical,
                        practicas: practical,
                        autonomas: autonomous,
                        nSGroup: nGroup
                    }
                    //para la complejidad
                    const complex = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].complexity;
                    let styleC = {
                        display: 'none'
                    }
                    let styleM = {
                        display: 'none'
                    }
                    let styleE = {
                        display: 'none'
                    }
                    if(parseInt(complex)===75){
                        styleC = {
                            display: 'block'
                        }
                        styleM = {
                            display: 'block'
                        }
                        styleE = {
                            display: 'block'
                        }
                    }else if(parseInt(complex)===50){
                        styleC = {
                            display: 'none'
                        }
                        styleM = {
                            display: 'block'
                        }
                        styleE = {
                            display: 'block'
                        }
                    }else if(parseInt(complex)===25){
                        styleC = {
                            display: 'none'
                        }
                        styleM = {
                            display: 'none'
                        }
                        styleE = {
                            display: 'block'
                        }
                    }
                    complejidadPorCurso.set(response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id, complex);

                    ///para guardar en un array todos los ids de los cursos y respectivos promedios en cada semestre
                    averagePerCoursePerTerm[contador] = {
                        course_id: response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id,
                        average: response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].avg_last_semester
                    }
                    contador++;

                    ///guardo en un array todos los ids de los curss que son prerequisito y postrequisito de la asignatura

                    let requisites = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].requisites;
                    let dependents = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].dependents;
                    let id_curso = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_id;//guardo el id del curso de la malla
                    let codigo_curso = response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].course_codigo;//guardo el codfigo del curso de la malla(sirve para validar los requisitos y dependientes)
                    let todasAsignaturas = { //sirve para guardar todas las horas de trabajo practico, teorico y autonomo por asignatura de la malla
                        id: id_curso,
                        code: codigo_curso,
                        practicas: practical,
                        nteoricas: teorical,
                        autonomo: autonomous,
                        aprobada: aprobada,
                        dependientes: dependents,
                        requisitos: requisites,
                        complejida: complex
                    }
                    cargaTrabajoArray.push(todasAsignaturas);
                  //  console.log("esta es la informacion que recibe "+informacion.final);
                   // console.log("este es el estudiante: "+asignatura+" "+(response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].position-1));
                    //en caso de que la asignatura sea optativa necesito anadir la clase optative para que tome el estilo de oblicua
                    let classOptative="titleAsignatura";
                    if(asignatura.toLowerCase().includes("optativa")){
                        classOptative+=" optative";
                    }
                    if(asignatura.toLowerCase().includes("optativa i")){
                        v = "Contabilidad";
                    }
                    //console.log(asignatura);
                    materiasPorCiclo[response[i].student_curriculum[j].curriculum.program_term[h].program_course[k].position-1] = <div 
                        id={idAsignatura} 
                        className={area}  
                        onClick={(e)=>this.clickedCourse(e,v, aprobacion, calificacion, idStudent, idcurriculum,cursoid,termid,grupo, informacion, requisites, dependents, id_curso)}>
                            <div 
                                className="interno" 
                                onMouseOver={(e)=>this.highlightRect(cursoid, true, fechaDos)} 
                                onMouseOut={(e)=>this.highlightRect(cursoid, false, fechaDos)}>
                                    <div 
                                        className="complexity">
                                            <div className="bottom">
                                                <span className="item complex" style={styleC} >.</span>
                                                <span className="item middle" style={styleM}>.</span>
                                                <span className="item easy" style={styleE}>.</span>
                                            </div>
                                    </div>
                                    <div 
                                        className={classOptative}>
                                            <span className="span-asignatura">{asignatura}</span>
                                    </div>
                            </div>
                            <div 
                                className={aprobacion} >
                                    {grades.length ? grades : ''}
                            </div>
                        </div>;
                    grades = [];
                    gradesgrow=[];
                    lost=[];
                    lostgrow=[];

                    
                }
                let marginclass="main2";//es la clas epara los margenes izquierdos del dashboard, por default el margin es de 2em pero al inicio debe ser de 0 em
                let ciclosPeriod="ciclosPeriodos";
                let classMargin = "margin";
                //console.log("ciclos", ciclos);
                if(h!=0 && ((h+1)%2)==0){//en caso de que h sea par entonces significa que se debe agregar al grupo de anio (array anio)
                    
                    ciclos[response[i].student_curriculum[j].curriculum.program_term[h].position-1] = <div className="main">{materiasPorCiclo.length ? materiasPorCiclo : ''}</div>;
                    //console.log("este es el estudiante:  "+ciclos.length)
                    let anioText="";
                    //primer anio
                    if(response[i].student_curriculum[j].curriculum.program_term[h].position===1 || response[i].student_curriculum[j].curriculum.program_term[h].position === 2){
                        anioText = "Primer Año";
                        marginclass="mainInicio";
                        ciclosPeriod="ciclosPeriodosInicio";
                        classMargin = "";
                    }
                    //segundo anio
                    else if(response[i].student_curriculum[j].curriculum.program_term[h].position===3 || response[i].student_curriculum[j].curriculum.program_term[h].position === 4){
                        anioText = "Segundo Año";
                        marginclass="main2";
                        ciclosPeriod="ciclosPeriodos";
                    }
                    //tercer anio
                    else if(response[i].student_curriculum[j].curriculum.program_term[h].position===5 || response[i].student_curriculum[j].curriculum.program_term[h].position == 6){
                        anioText = "Tercer Año";
                        marginclass="main2";
                        ciclosPeriod="ciclosPeriodos";
                    }
                    //cuarto anio
                    else if(response[i].student_curriculum[j].curriculum.program_term[h].position===7 || response[i].student_curriculum[j].curriculum.program_term[h].position === 8){
                        anioText = "Cuarto Año";
                        marginclass="main2";
                        ciclosPeriod="ciclosPeriodos";
                    }
                    //quinto anio
                    else if(response[i].student_curriculum[j].curriculum.program_term[h].position===9 || response[i].student_curriculum[j].curriculum.program_term[h].position === 10){
                        anioText = "Quinto Año";
                        marginclass="main2";
                        
                    }
                  //  console.log(response[i].student_curriculum[j].curriculum.program_term[h].position);
                    let head = <div className={ciclosPeriod}>
                                <div className="izq">
                                    <div className="numc">{(h)}</div>
                                </div>
                                <div className="medio">{anioText}</div>
                                <div className="der">
                                    <div className="numc">{h+1}</div>
                                </div>
                            </div>; 
                    
                    anios.push(<div className={classMargin}>{head}<div className={marginclass}>{ciclos.length ? ciclos : ''}</div></div>);
                    ciclos = [];
                }else{
                   // console.log(response[i].student_curriculum[j].curriculum.program_term[h].position);
                    ciclos[response[i].student_curriculum[j].curriculum.program_term[h].position-1] = <div className="main">{materiasPorCiclo.length ? materiasPorCiclo : ''}</div>;
                }
                //ciclos[response[i].student_curriculum[j].curriculum.program_term[h].position-1] = 
            }
            //en caso de que temrine en ciclos impares, por ejemplo, tiene 9 ciclos no 8 ni 10 ni 12, entonces debo anadir ese unico ciclo.
            if(ciclos.length > 0 ){
                //console.log(anios.length);
                let anioText = "";
                let ciclosPeriod = "ciclosPeriodos"
                let classMargin = "margin";
                let marginclass = "main2";
                if(anios.length === 1){
                    anioText ="Segundo Año";
                }else if(anios.length === 2){
                    anioText ="Tercer Año";
                }else if(anios.length === 3){
                    anioText ="Cuarto Año";
                }else if(anios.length === 4){
                    anioText ="Quinto Año";
                }else if(anios.length === 5){
                    anioText ="Sexto Año";
                }
                let head = <div className={ciclosPeriod}>
                                <div className="izq">
                                    <div className="numc" style={{width: '48%'}}>{(h)}</div>
                                </div>
                                <div className="medioImpar">{anioText}</div>
                                {/* <div className="der">
                                    <div className="numc">{h+1}</div>
                                </div> */}
                            </div>; 
                anios.push(<div className={classMargin}>{head}<div className={marginclass}>{ciclos.length ? ciclos : ''}</div></div>);
                ciclos = [];
            }

         }
     }
     //console.log("este es el tamano del average per course id "+averagePerCoursePerTerm.length+" "+averagePerCoursePerTerm[0].course_id+" "+averagePerCoursePerTerm[0].average+" histories: "+historiesPerStudent.length);
     this.setState({ data: anios });
     //llamo a la funcion que calcula los promedios tanto del estudiante como de los companeros
     //console.log(averagePerCoursePerTerm, historiesPerStudent);
     let promediosData = calculaAverage(averagePerCoursePerTerm, historiesPerStudent);
     let dataset = promediosData[3];
    // console.log("Solo los anios "+dataset[1].tipo+" "+dataset[1].fecha+"  "+dataset[1].data);
     
     //console.log(promediosData[0].partners.data.length+"    "+promediosData[0].student.data.length);
     //console.log(this.state.dat);
     this.setState({dates: promediosData[1]});
     this.setState({dat: promediosData[0]});
     this.setState({anios: promediosData[2]});
     this.setState({todasAsignaturas: cargaTrabajoArray});
     
     ///indico que el valor de cursos tomados es el de promedios[4]
     cursosTomados = promediosData[4];
     ///seteo el estado de promedios student con los promedios obtenidos por semestre
     //de esta manera sumo todos los promedios y divido para el total de promedios y obtengo
     //un promedio general
     this.setState({promediosStudent: dataset});
     //console.log(promediosData[4]);
     //la llamada a una funcion en otor archivo se la debe hacer luego de todas las actualizaciones de los estados de esta clase, no antes.
     //mostrarGrafico(dataset);
     //console.log(estudiante);
     ////muevo el switch a top cero para que no se vea brincado
    // let switchR = d3.select(".react-switch-handle").style('top', '0px');
    // console.log(switchR.node());
     //dataset,fechas, courses ids, nota spro curso, promedioaprobados por semestre, promedio desaprobados por semestre, numero de asignaturas aprobadas por semestre, nombre del estudiante, anios por periodo
     showLineChart(dataset, promediosData[1], promediosData[4], promediosData[5], promediosData[6], promediosData[7], promediosData[8], estudiante, promediosData[9], promediosData[10]);
     individualCoursesChart(promediosData[4], promediosData[1], promediosData[5], complejidadPorCurso, dataset, promediosData[10]);//courseIds, dates, notas por curso
    //  complejidadPorCurso.forEach(function(value, key, map){
    //      console.log(value+" "+ key);
    //  });

    //con esto, quito el cargando, porque una vez finalizada esta funcion, se va amostrar el dashboard
    let charging = document.getElementById("ipl-progress-indicator");
      charging.classList.add("available");
      //charging.classList.remove("ipl-progress-indicator");
     //console.log(promediosData[5]);
    return anios;
    //console.log(JSON.parse(JSON.parse(response).student_curriculum));
    //console.log(JSON.parse(response).student_curriculum);
    //console.log(JSON.parse(response)[0].student_curriculum[0].curriculum.program_term[0].program_course);
  } catch (error) {
    console.log(`Error: ${error.message}`)
  }
}

returnIdsDependentsRequisites (dependents, requisites){
    let dependent = [];
    let requisite = [];
    for(let i in this.state.todasAsignaturas){
        //let val = this.state.todasAsignaturas[i].code.substring(2, this.state.todasAsignaturas[i].code.length);
        let val = this.state.todasAsignaturas[i].code;
        for( let j in dependents){
            
            if(dependents[j] === val){
                //console.log("dep", val, dependents[j], this.state.todasAsignaturas[i].id);
                dependent.push(this.state.todasAsignaturas[i].id);
            }
            
        }
        for( let j in requisites){
            
            if(requisites[j] === val){
                // console.log("req", val, requisites[j], this.state.todasAsignaturas[i].id);
                requisite.push(this.state.todasAsignaturas[i].id);
            }
            
        }
    }
    return [dependent, requisite];
}
async clickedCourse(e, asignatura, aprobacion, grades, idStudent, idcurriculum,cursoid,termid,grupo, informacion, requisites, dependents, id_curso){
    e.preventDefault();
    //console.log(requisites);
    //console.log(dependents);
    //si no esta activado el switch de workload
   if(this.state.changedWorkload === false){
    
    var top=e.screenY;
    var left=e.screenX;
    var rect = e.target.getBoundingClientRect();
    this.setState({materia: asignatura});
    var divReferencia = e.target;
    top=rect.top;
    left=rect.left;
    //console.log("estas son las calificaciones "+idStudent+""+grades.length);
    let porcentajes = [];
    let datos = [];

    ///valido los requisitos y prerequisitos
    let depReq = this.returnIdsDependentsRequisites(dependents, requisites);
    dependents = depReq[0];
    requisites = depReq[1];
    if(grades.length>0){
        // ////para extraer los estudiantes en porcentajes por calificasiones y saber donde se hubica el estudiante dado
        const ENDPOINTSTUDENTPARTNERS = 'http://localhost:3000/api/v1/getAveragePartners/?studentid='+idStudent+'&curriculumid='+idcurriculum+'&courseid='+cursoid+'&termid='+termid+'&group='+grupo;
        porcentajes = await request({
            uri: ENDPOINTSTUDENTPARTNERS,
            json: true,
            rejectUnauthorized: false,
            //   headers: {
            //     authorization: [token_type, access_token].join(' '),
            //   },
            })
        datos = porcentajes.data.concat([0,0,0]);//para graficar, los tres ultimos valores indican las barras de estado de debajo de la grafioca de barras
            //los estados serviran para pasar como parametros a otros componentes.
    let position = this.calcula(divReferencia, rect.top, rect.left);

    ////los requisitos y prerequisitos
    if(this.state.dependents !== null){
        this.dependientes(this.state.dependents, false);
        this.dependientes([id_curso], false, "blue");
        this.setState({dependents: []});
    }
    if(this.state.requisites !== null){
        this.requisitos(this.state.requisites, false);
        this.setState({requisites: []});
    }
    if(dependents !== null){
        this.dependientes(dependents, true);
        this.dependientes([id_curso], true, "blue");
        this.setState({dependents: dependents});
        this.setState({idCurso: id_curso});
    }else{
        this.dependientes([id_curso], false, "blue");
        this.setState({dependents: []});
    }
    
    if(requisites !== null){
        this.requisitos(requisites, true);
        this.setState({requisites: requisites});
    }else{
        this.setState({requisites: []});
    }
    if(this.state.idAnterior !== id_curso){
        this.dependientes([this.state.idAnterior], false, "blue");
    }
    this.setState({idAnterior: id_curso});
    //console.log(porcentajes);
    //console.log("esta es la informacion que recibe "+informacion.missedClasses);
    this.setState({showPopUp: true});
    this.setState({style:{top: rect.top, left: rect.left}});
    this.setState({aprobacion: aprobacion});
    this.setState({calificaciones: grades});
    this.setState({datagraph: datos});
    this.setState({positionStudentGraph: porcentajes.position});

    //solo para informaicon
    this.setState({aprovechamientoOne: informacion.notaOne});
    this.setState({aprovechamientoTwo: informacion.notaTwo});
    this.setState({interciclo: informacion.notaMiddle});
    this.setState({final: informacion.final});
    this.setState({suspension: informacion.retakeGrade});
    this.setState({notaF: informacion.fgrade});
    this.setState({faltas: informacion.missedClasses});
    this.setState({faprobacion: informacion.approval});
    this.setState({hteoricas: informacion.teoricas});
    this.setState({htrabajoautonomo: informacion.autonomas});
    this.setState({hpracticas: informacion.practicas});
    this.setState({nStudentsGroup: informacion.nSGroup});
    this.setState({popUpPos: position});

    //agrego el evento de key press para cuando presione el esc se cierre el popup pequeno
    let closeLittlePopUp = () => {
        this.togglePopup();
    }
    document.addEventListener('keydown', function(event){
        if(event.keyCode === 27){//27 es la tecla escape
            closeLittlePopUp();
        }
    });

    }else{
        
        porcentajes = [];
        datos = [];
        if(id_curso === this.state.idAnterior){
            
            if(dependents !== null){
                //console.log("llega al else "+this.state.idAnterior+"   "+cursoid);
                this.dependientes(dependents, false);
                this.dependientes([id_curso], false, "blue");
                this.setState({dependents: []});
            }else{
                this.dependientes([id_curso], false, "blue");
                this.setState({dependents: []});
            }

            ///requisites
            if(requisites !== null){
                this.requisitos(requisites, false);
                this.setState({requisites: []});
            }else{
                this.setState({requisites: []});
            }
            this.dependientes([id_curso], false, "blue");
            this.setState({idAnterior: 0});
        }else{
            this.setState({idAnterior: id_curso});
            if(this.state.dependents !== null){
                this.dependientes(this.state.dependents, false);//quito todos los anteriores
                this.dependientes([id_curso], false, "blue");
                this.setState({dependents: []});
            }else{
                this.dependientes([id_curso], false, "blue");
                this.setState({dependents: []});
            }
            this.dependientes([this.state.idAnterior], false, "blue");
            this.dependientes([id_curso], true, "blue");
            this.dependientes(dependents, true);
           // console.log("entraaaaaaaa");
            this.setState({dependents: dependents});
            //requisites
            if(this.state.requisites !== null){
                this.requisitos(this.state.requisites, false);//quito todos los anteriores
                this.setState({requisites: []});
            }else{
                
                this.setState({requisites: []});
            }
            this.requisitos(requisites, true);
            this.setState({requisites: requisites});
            
        }
        //requisites
        
       // alert("Aun no ha cursado esta asignatura!");
    }
}else{//en caso de que sea verdadero, es decir, de que si esta activado el switch de carga de trabajo, debe mostrar la carga de trabajo
    
    //console.log("carga de trabajo");

    //obtengo la grafica de rectangulos para poder agregar nuevos
    
    // let svg =  d3.select(".svgRectChart");
    let svg =  d3.select(".drag");
    let childs = svg.selectAll('rect').nodes();//selecciono todos los cuadrados dibujados en la grafica de rectangulos
    let texts = svg.selectAll("text").nodes();//selecciono todos los textos de la grafica, (incluye anios, meses y numeros de repeticiones)
    let x = parseInt(d3.select(childs[childs.length - 1]).attr('x'));//obtengo la posicion en x del ultimo rctangulo
    let month = "Sept";//mes por default septiembre
    for(let va = 0; va < texts.length; va++){//recorro todos los textos
        let tex = d3.select(texts[va]).node();//selecciono el nodo del texto (<text class="..." x=......)
        if(x === parseInt(d3.select(tex).attr('x'))){//si el valor de x del ultimo rectangulo obtenido anteriormente es igual al valor de x del texto actul del bucle entonces:
            month = d3.select(tex).text();// indico que el mes va a ser igual al mes que indica en el texto de ese nodo del bucle   
        }
    }
    let rectSize = 20;
    let spaceBetweenTerms = 5;
    let spaceBetweenYears = 20;
    
    //console.log(x, texts.length, month);
    if (xHasValue === false){
        if(month === "Sept"){//si es el mes de septiembre, tiene que ubicarse el nuvo cuadro en el mes de marzo del siguiente anio
            x = x + rectSize + spaceBetweenYears; //sumo 40 porque cada recta es 20, y cada espacio entre anios es 20 = 40
        }else{//si es marzo, significa que debe uibicarse el cuadro en el mes de septiembre del mismo anio
            x = x + 25;//25 porque cada recta es de 20 y 5 es el espacio entre ciclos del mismo anio.
        }
        xHasValue=true;
    }

    //primero pregunto si ya existe esa asignatura en el mapa, si esque ya existe, la quito
    let selectedCourses = this.state.asignaturasSeleccionadas;
    //console.log(selectedCourses.size);
    //eleimino las rectas para volver a grafocar y aumentado o disminuido uno dependiendo el caso
    let size = selectedCourses.size;
    for( let a = childs.length - size; a < childs.length; a++){
        d3.select(childs[a]).remove();
        d3.selectAll(".added").remove();//quito todos los elementos que tienen la clase added (esta clase solo tinen los nuevos rectangulos)
        // a--;
    }
    if(selectedCourses.has(id_curso)){//si esque el mapa ya tiene el id del curso en el que di clic, entonces significa que el curso esta siendo quitado la seleccion
        selectedCourses.delete(id_curso);//quito el curso seleccionado del mapa
        isSelected.delete(id_curso);//quito tambien del otro mapa
        if(selectedCourses.size===0){//en caso de que no haya seleccionado ningun curso o que haya quitado la seleccion de todos y quiera volver a seleccionar debe iniciars een falso para que x aumente
            xHasValue = false;
        }
        d3.selectAll(".rect-course-"+id_curso).style("pointer-events", "");//quito todos los evntos del mouse al rectangulo que pertenece a la asignatura que acabo de dar clic
        d3.selectAll(".rep-"+id_curso).style("pointer-events", "");
        this.setState({asignaturasSeleccionadas: selectedCourses});
        //console.log("Elimina "+id_curso)
        //invoco al metodo que calcula el total de horas de trabajo en teoricas, practicas y autonomas
        this.sumaHorasCargaDeTrabajo();
        //invoco al metodo para que quite la selecicon de la asignatura seleccionada
        this.dependientes([id_curso], false);
    }else{//en caso de que no tenga el id de la asignatura seleccionada
        //obtengo las horas teoricas, practicas y autonomas del curso seleccionado
        let valores = {};
        let allCourses = this.state.todasAsignaturas;
        for(var i in allCourses){
           if(allCourses[i].id === id_curso){
                valores = {
                    teorica: allCourses[i].nteoricas,
                    practica: allCourses[i].practicas,
                    autonoma: allCourses[i].autonomo
                }
            }
         
        }
        //agrego los nuevo valores en los cursos seleccionados
        selectedCourses.set(id_curso, valores);

        this.setState({asignaturasSeleccionadas: selectedCourses});
        //invoco al metodo que calcula el total de horas de trabajo en teoricas, practicas y autonomas
        this.sumaHorasCargaDeTrabajo();
        //invoco al metodo para que seleccione o resalte la asignatura seleccionada
        this.dependientes([id_curso], true);

       
       // console.log(selectedCourses);
        
    }

    //grafico los cuadros nuevos
    size = selectedCourses.size;
    //para el alto
    const alto = rectSize * size + (size * spaceBetweenTerms) + 40;
    let y = 200 - alto;//200 es el height - margin top - margin bottom del archivo showRectChasrt.js
    let g = svg.append('g')
    .attr("transform", `translate(${10}, ${15})`);
    let dep = (id, is) => {
        if( is === false){
            d3.select("#asignatura-"+id)
                .style("box-shadow","");
        }else{
            d3.select("#asignatura-"+id)
                .style("box-shadow","3px 3px 3px");
        }
    }
    
    let a = 0;
    let allCourses = this.state.todasAsignaturas;
    
    selectedCourses.forEach(function(value, key, map){
        let complejidad = "";
        for( i in allCourses){
            if(allCourses[i].id === key){
                complejidad = allCourses[i].complejida;
            }
        }
        g.append('rect')
        .attr('x', x)
        .attr('y', y)
        //.attr("id", "rect-course-"+key)
        .attr('width', rectSize)
        .attr('height', rectSize)
        //.attr('class', 'rect-course-'+key)
        .attr('class', 'rect-added')
        // .style("fill", 'darkseagreen')

        .on("mouseover", function(){
           dep(key, true); 
        })
        .on("mouseout", function(){
            dep(key, false);
        });
        //console.log(complejidad);
        ///AKI FALTA GRAFICAR LOS NUMERO DE VECES QUE REPITE EL CURSO, PA ESO NECESITO LLAMAR A LA FINCION NRETAKECOURSES DEL ARCHIVO SHOWRECTCHART.JS 
        //EL PROBLEMA ES QUE ESA FUNCION RECIBE UN ARRAY DE CURSOS QUE YO NO OBTENGO EN ESTA FUNCION, ESOS CURSOS SON EL PROMEDIODATA[4] QUE ESTA EN LA LINEA 467
        let repetidoss = nRetakeCourses(cursosTomados);
        //console.log(repetidoss[1]);
        let mapRepetidos = repetidoss[1];
        let veces = 1;
        if(mapRepetidos.has(key)){
            d3.selectAll(".rect-course-"+key).style("pointer-events", "none");
            d3.selectAll(".rep-"+key).style("pointer-events", "none");
            isSelected.set(key, true);
            veces = parseInt(mapRepetidos.get(key)) + 1;
        }
        //para saber si estan o no visibles la complejidad en los cuadritos (switch complejidad dactivo)
        let valueVisible = d3.selectAll(".complexityRect").style('visibility');
        //console.log(valueVisible);
        //agrego el numero de veces que tomaria la asignatura
        if(valueVisible === "visible"){
            addText(g, x, y, rectSize, veces, key, null, true, "hidden");
            addComplexity(g,x,y, rectSize, complejidad, key, 70, true, "visible");
        }else{   
            addText(g, x, y, rectSize, veces, key, null, true, "visible");    
            addComplexity(g,x,y, rectSize, complejidad, key, 70, true, "hidden");
        }
        //agrego la complejidad de la asignatura
        
        y = y + rectSize + spaceBetweenTerms;
    });
    // for(let a = 0; a < size; a++){
    //     g.append('rect')
    //     .attr('x', x)
    //     .attr('y', y)
    //     .attr('width', rectSize)
    //     .attr('height', rectSize)
    //     //.attr('class', 'rect-course-'+id)
    //     .style("fill", 'orange')
    //     .on("mouseover", function(){
    //        dep(selectedCourses.get(a), true); 
    //     })
    //     .on("mouseout", function(){
    //         dep(selectedCourses.get(a), false);
    //     });

    //     y = y + rectSize + spaceBetweenTerms;
    // }
   // console.log(this.state.asignaturasSeleccionadas);
    
}
        //console.log("calificaciones tam "+grades.length);
    //console.log("estos son los porcentajes pro amteria: "+porcentajes.position+" "+porcentajes.data );
    //if((top))
  //  alert("se dio un click "+asignatura+" "+e.className+" "+top+" "+left+" "+rect.top+" "+e.target.offsetHeight+" "+el.offsetHeight);
    
    return true;
}

highlightRectHover(e,id, ismouseover, fechas){
    e.preventDefault();
    
   // alert("dfgh "+id);
  // $('#asignatura-'+id).mouseout();
    let clase=d3.select(e.target).attr("class");
    
    //let key  = fecha.getTime()+"";
    //console.log(key);
    //si es la ultima nota no aprobada
    if(clase.includes("l")){
        //console.log(d3.selectAll(".rect-course-"+id).nodes()[1]);
        let nodo = d3.selectAll(".rect-course-"+id).nodes()[1];
        let key = fechas.get(id)[1].getTime()+"";  
        if(ismouseover === true){
            d3.select(nodo)
                .style("stroke", 'black')
                .style("stroke-width", '2px');    
            d3.select('[id="'+key+'"]').attr("r", 7);
        }else{
            d3.select(nodo)
                .style("stroke", 'none')
            d3.select('[id="'+key+'"]').attr("r", 5);
        }
                //.style("stroke-width", '2px');    
    }else{
        let key = fechas.get(id)[0].getTime()+"";
        if(ismouseover === true){
            d3.select(".rect-course-"+id)
                .style("stroke", 'black')
                .style("stroke-width", '2px');
            d3.select('[id="'+key+'"]').attr("r", 7);

        }else{
            d3.select(".rect-course-"+id)
                .style("stroke", 'none');
            d3.select('[id="'+key+'"]').attr("r", 5);
        }
                //.style("stroke-width", '2px');
    }
}
sumaHorasCargaDeTrabajo(){
    let selected = this.state.asignaturasSeleccionadas;
    let teoricas = 0;
    let practicas = 0;
    let autonomas = 0;
    selected.forEach(function(value, key, map){
        teoricas += value.teorica;
        practicas += value.practica;
        autonomas += value.autonoma;
    });
    let horas = {
        teoricas: teoricas,
        practicas: practicas,
        autonomas: autonomas
    }
  //  console.log("Teoricas: "+teoricas+" practicas: "+practicas+" autonomas:"+autonomas);
    createChart(horas);
}
calcula(divReferencia, top, left){
    var el = document.getElementsByClassName("centro")[0];
    //console.log("Este es el centro " +document.getElementsByClassName("centro"));
    //console.log("este es el tamano de centro "+el.offsetWidth);
    var centroX = (divReferencia.offsetWidth / 2) + left;
    var centroY = (divReferencia.offsetHeight / 2) + top;
    var MinimoX = el.getBoundingClientRect().left;
    var MaximoX = el.offsetWidth;
    var MaximoY = el.offsetHeight;
    var MinimoY = el.getBoundingClientRect().top;

    let data = {
        centroX: centroX,
        centroY: centroY,
        MinimoX: MinimoX,
        MinimoY: MinimoY,
        MaximoX: MaximoX,
        MaximoY: MaximoY
    }
    return data;
}
highlightRect (id, ismouseover, fecha){
    let key='';
    //console.log(fecha);
    if(fecha !== '' && fecha !== undefined)
        key = fecha.getTime()+"";
    if(ismouseover === true){
        d3.selectAll(".rect-course-"+id)
            .style('stroke', 'black')
            .style('stroke-width', '2px');
        d3.select('[id="'+key+'"]').attr('r', 7);
    }else {
        //en caso de que se quiera mantener seleccionada el rectangulo que corresponde a la asignatura 
        //donde se dio clic en la carga de trabajo, es decir, si se quiere que se quede con borde de 2px 
        //aunque se quite el cursor de la asignatura, seguira estando con dos px el borde del rectangulo que le corresponde
        //sirva para las repetudas el if comentado //if(isSelected.has(id) === false)
        //if(isSelected.has(id) === false)
        d3.selectAll(".rect-course-"+id)
            .style('stroke', 'none');
            //.style('stroke-width', '2px');
        d3.select('[id="'+key+'"]').attr('r', 5);
    }
}
dependientes(dependents, isEmpty, color){
    if(dependents !== null){
        if(isEmpty == true){
            for(var i = 0; i < dependents.length; i++){
                if(color !== undefined && color !== null){
                    d3.select("#asignatura-"+dependents[i])
                    .style("border", "5px "+color+" solid");    
                }else
                d3.select("#asignatura-"+dependents[i])
                    .style("border", "5px black solid");
            }
        }else{
            for(var i = 0; i < dependents.length; i++){
                d3.select("#asignatura-"+dependents[i])
                    .style("border", "1px black solid");
            }
        }
    }
}
requisitos(requisites, isEmpty){
    if(requisites !== null){
        if(isEmpty === true){
            for(var i = 0; i < requisites.length; i++){
                d3.select("#asignatura-"+requisites[i])
                    .style("border", "5px black solid");
            }
        }else{
            for(var i = 0; i < requisites.length; i++){
                d3.select("#asignatura-"+requisites[i])
                    .style("border", "1px black solid");
            }
        }
    }
}
togglePopup(){
    this.dependientes(this.state.dependents, false);
    this.requisitos(this.state.requisites, false);
    this.setState({dependents: []});
    this.setState({requisites: []});
    this.dependientes([this.state.idCurso], false, "lightblue");
    this.setState({showPopUp: false});
    //eimino el evento de key press para cuando presione el esc se cierre el popup pequeno
    let closeLittlePopUp = () => {
        this.togglePopup();
    }
    document.removeEventListener('keydown', function(event){
        if(event.keyCode === 27){//27 es la tecla escape
            closeLittlePopUp();
        }
    });
}
togglePopupGrow(){
    this.setState({showPopUpGrow: false});

    //agrego el evento de key press para cuando presione el esc se cierre el popup pequeno
    let closeGrowPopUp = () => {
        this.togglePopupGrow();
    }
    document.removeEventListener('keydown', function(event){
        if(event.keyCode === 27){//27 es la tecla escape
            closeGrowPopUp();
        }
    });
}
showPopUpGrow(){
    
    this.togglePopup();
    this.setState({showPopUpGrow: true});
    //agrego el evento de key press para cuando presione el esc se cierre el popup pequeno
    let closeGrowPopUp = () => {
        this.togglePopupGrow();
    }
    document.addEventListener('keydown', function(event){
        if(event.keyCode === 27){//27 es la tecla escape
            closeGrowPopUp();
        }
    });
}

changeSwitch(){
    if(this.state.changed === true){
        d3.selectAll(".nRetake")
            .style('visibility', 'visible');
        d3.selectAll(".complexityRect")
            .style('visibility', 'hidden');
        chang =false;
        this.setState({changed: false});
    }else{
        d3.selectAll(".nRetake")
            .style('visibility', 'hidden');
        d3.selectAll(".complexityRect")
            .style('visibility', 'visible');
            chang=true;
        this.setState({changed: true});
    }
    
}

changeToWorkload(){
   // console.log(this.state.changedWorkload);
   
    if(this.state.changedWorkload === true){
        this.setState({changedWorkload: false});
        changeWorkload = false;
        this.unlockCourses();
        showLineChartFromWorkLoad();
        let asi = this.state.asignaturasSeleccionadas; 

        //elimino los bordes de los rectangulos en la grafica de las asignaturas seleccionadas y de paso elimino del mapa esas asginaturas
        let hi = (key, is) =>{
            this.highlightRect(key, is);
        }
        //elimino del mapa las asignaturas seleccionadas
        isSelected.forEach(function (value, key, map){
            isSelected.delete(key);
            d3.selectAll(".rect-course-"+key).style("pointer-events", "");
            d3.selectAll(".rep").style("pointer-events", "");
            hi(key, false);
            
        });
        isSelected = new Map();
        this.highlightRect();
        let ids = [];
        // let svg =  d3.select(".svgRectChart");
        let svg =  d3.select(".drag");
        let childs = svg.selectAll('rect').nodes();
        let childsTexts = svg.selectAll('.added').remove();
        let a = childs.length - asi.size;
        //console.log(asi.size);
        xHasValue = false;
        asi.forEach(function(value, key, map){
            ids.push(key);
            //console.log(childs[a], a);
            d3.select(childs[a]).remove();//borro los cuadros por cad aasignatura que estaba seleccionada
            a++;
        });
        this.dependientes(ids, false);
        this.setState({asignaturasSeleccionadas: new Map()});
    }else{
        this.disableCourses();
        let horas = {
            teoricas: 0,
            practicas: 0,
            autonomas: 0
        }
        //console.log("Teoricas: "+teoricas+" practicas: "+practicas+" autonomas:"+autonomas);
        createChart(horas);
        changeWorkload = true;
        this.setState({changedWorkload: true});
      //  intervaloCambio = this.move();
    }
    
}
disableCourses(){
    let todas = this.state.todasAsignaturas;
    let idsDisable = new Map();
    for (var i in todas){
        //primero todas las que aprobo
        if(todas[i].aprobada === "APROBADA" || todas[i].aprobada === "CURSANDO"){
            idsDisable.set(todas[i].id, todas[i].aprobada);
        }else if(todas[i].requisitos != null){
            let req = this.returnIdsDependentsRequisites([],todas[i].requisitos)[1];
            //console.log("id: "+todas[i].id);
            //console.log(todas[i].requisitos);
            for (var r in req){
                //console.log("Entra aqui "+req[r]+"  "+todas[i].id+"  "+idsDisable.has((req[r])));

                if((idsDisable.has((req[r]))&&idsDisable.get(req[r])==="APROBADA") || (idsDisable.has((req[r]))&&idsDisable.get(req[r])==="CURSANDO")){
                   
                }else{
                   // console.log("todas: "+todas[i].id+" "+req[r]);
                       idsDisable.set(todas[i].id, todas[i].aprobada);
                }
            }
        }
    }
    
   /// console.log(idsDisable);
    this.lock(idsDisable);
}
lock(idsDisable){
    idsDisable.forEach(function(value, key, map){
        d3.select('#asignatura-'+key)
            .attr('disabled', true)
            .attr('style', 'pointer-events: none; opacity: 0.3');
    });
    this.setState({locked: idsDisable});
}
unlockCourses(){
    let lockedCourses = this.state.locked;
    lockedCourses.forEach(function(value, key, map){
        d3.select('#asignatura-'+key)
            .attr('disabled', false)
            .attr('style', 'pointer-events: all; opacity: 1');
    });
}
move(){
    var intervaloCambio = setInterval(function(){
        let sizeMove = d3.select('.react-switch-bg')
            .node().offsetWidth;
        //console.log("este es cuanto se mueve: "+sizeMove);
        let move = d3.select(".react-switch-handle")
            .style('transform', `translateX(${sizeMove})`);
    //console.log("evento "+move);
    }, 50);
   return intervaloCambio;
}
componentDidUpdate(){
    ///actualizo el tamano de fuente para cada asignatura, esto con el fin de hacer responsivo todo el dashboard
    if(numeroCiclos === 9){
        d3.selectAll('.main_div').style('font-size', '0.835vw');
    }else if(numeroCiclos === 10){
        d3.selectAll('.main_div').style('font-size', '0.75vw');
    
    // if(numeroCiclos === 9  || numeroCiclos === 10){
    //     d3.selectAll('.main_div').style('font-size', '0.75vw');
    }else if(numeroCiclos === 11 || numeroCiclos === 12){
        d3.selectAll('.main_div').style('font-size', '0.6vw');
    }else if(numeroCiclos === 7 || numeroCiclos === 8){
        d3.selectAll('.main_div').style('font-size', '0.96vw');
    }
    if(this.state.changedWorkload === true){
        let sheight = d3.select('.react-switch-handle').node().offsetHeight;//retorna el tamano en alto
        let sizeMove = d3.select('.react-switch-bg')
            .node().offsetWidth;
        //sheight = returnDigitsFromString(sheight);//llamo a la funcion a que me devuelva solo el numero no el px
        let size = sizeMove - sheight;
       // console.log("este es cuanto se mueve: "+sizeMove +" "+sheight+ " ");
        
        //sizeMove = sizeMove - sheight;
        d3.select(".react-switch-handle")
            .style('transform', 'translateX('+size+'px)');
       // console.log(d3.select(".react-switch-handle")
       // .style('transform'));
    }
    ///para el segundo swotch
    if(this.state.changed === true){
        //como hay dos swicth q tienen la misma clase, y se que el segundo es el del segundo switch por eso 
        //hago un select all, luego obtengo los nodos de ese y solo esocjo el de la posicion 1 no cero.
        let sheight = d3.selectAll('.react-switch-handle').nodes()[1].offsetHeight;//retorna el tamano en alto
        //console.log(d3.selectAll('.react-switch-handle').nodes()[1]);
        //hago lo mismo que el anterior pero con otra clase
        let sizeMove = d3.selectAll('.react-switch-bg')
            .nodes()[1].offsetWidth;
        let size = sizeMove - sheight; 
        let nodo = d3.selectAll(".react-switch-handle").nodes()[1];
        d3.select(nodo)
            .style('transform', 'translateX('+size+'px)');
        

    }
}
updateSwitches(){
     ///para el segundo swotch
    // console.log(chang);
     if(chang === true){
        //como hay dos swicth q tienen la misma clase, y se que el segundo es el del segundo switch por eso 
        //hago un select all, luego obtengo los nodos de ese y solo esocjo el de la posicion 1 no cero.
        let sheight = d3.selectAll('.react-switch-handle').nodes()[1].offsetHeight;//retorna el tamano en alto
        //console.log(d3.selectAll('.react-switch-handle').nodes()[1]);
        //hago lo mismo que el anterior pero con otra clase
        let sizeMove = d3.selectAll('.react-switch-bg')
            .nodes()[1].offsetWidth;
        let size = sizeMove - sheight; 
        let nodo = d3.selectAll(".react-switch-handle").nodes()[1];
        d3.select(nodo)
            .style('transform', 'translateX('+size+'px)');
    }
    if(changeWorkload === true){
        let sheight = d3.select('.react-switch-handle').node().offsetHeight;//retorna el tamano en alto
        let sizeMove = d3.select('.react-switch-bg')
            .node().offsetWidth;
        //sheight = returnDigitsFromString(sheight);//llamo a la funcion a que me devuelva solo el numero no el px
        let size = sizeMove - sheight;
       // console.log("este es cuanto se mueve: "+sizeMove +" "+sheight+ " ");
        
        //sizeMove = sizeMove - sheight;
        d3.select(".react-switch-handle")
            .style('transform', 'translateX('+size+'px)');
       // console.log(d3.select(".react-switch-handle")
       // .style('transform'));
    }
}
componentDidMount(){
    window.addEventListener('resize', this.updateSwitches);
}
//test();
//console.log("Este es lo sanios "+anios.length);

    render () {
        
        return (
            <div className="dashboard">
               <StudentMenuBar nombre={estudiante} email={email} carrera={carrera} promediosStudent={this.state.promediosStudent}
               totalAsignaturas={this.state.todasAsignaturas.length}
               totalAsignaturasAprobadas={totalAsignaturasAprobadas}
               totalAsignaturasCursadasPerdidas={totalAsignaturasCursadasPerdidas}
               student_id={id_estudiante}/> 
            <div className="main1">
            {this.state.data.length ? this.state.data : ''}
            </div>
            {/* <h1>{this.state.materia}</h1> */}
            {/*<div><AverageStudentPartners data={this.state.dat} dates={this.state.dates} anios={this.state.anios}/></div>*/}
            

              {this.state.showPopUpGrow ?
              <PopUpAsignaturaGrow
              onKeyPress={(event) =>{
                if(event.key === "Enter"){
                    alert("enter");
                    this.togglePopupGrow();
                }
            }}
              style={this.state.style}
              closePopUpGrow={this.togglePopupGrow.bind(this)}
              asignatura={this.state.materia}
              datagraph = {this.state.datagraph}
              position = {this.state.positionStudentGraph}
            
              
              aprovechamientoOne={this.state.aprovechamientoOne}
                aprovechamientoTwo={this.state.aprovechamientoTwo}
                interciclo={this.state.interciclo}
                final={this.state.final}
                suspension={this.state.suspension}
                faltas={this.state.faltas}
                formaAprovacion={this.state.faprobacion}
                hteoricas={this.state.hteoricas}
                htrabajoautonomo={this.state.htrabajoautonomo}
                hpracticas={this.state.hpracticas}
                notaF={this.state.notaF}
                nStudentsGroup={this.state.nStudentsGroup}
                popUpPos={this.state.popUpPos}
            />
            :null
            }
            <div className="legendDashBoard">
                <div className="legendComplexityContainer">
                
                </div>
                <div className="legendDashContainer">
                <div className="legendComplexity">
                    
                    <div className="legendAlta">
                        <div>
                            <div className="clegends alta"></div>
                            <div className="clegends alta"></div>
                            <div className="clegends alta"></div>
                        </div>
                        Complejidad Alta
                    </div>
                    <div className="legendMedia">
                        <div>
                            <div className="clegends media"></div>
                            <div className="clegends media"></div>
                        </div>
                        Complejidad Media
                    </div>
                    <div className="legendBaja">
                        <div className="clegends baja"></div>
                         Complejidad Baja
                    </div>
                </div>
                    <div className="legendDash">
                        <div className="legendBasica">
                            <div className="dlegends basica"></div>
                            Básica
                        </div>
                        <div className="legendFormacion">
                            <div className="dlegends formacion"></div>
                            Formación
                        </div>
                        <div className="legendTitulacion">
                            <div className="dlegends titulacion"></div>
                            Titulación
                        </div>
                    </div>
                </div>
            </div>
            <div className="charts">
                <div className="switchCargaTrabajo">
                    <label className="labelCursos">Cursos</label>
                    <label className="switch-workload">
                        <Switch className="workload-switch" checked={this.state.changedWorkload} onChange={this.changeToWorkload.bind(this) }
                            onColor="#86d3ff"
                            onHandleColor="#2693e6"
                            uncheckedIcon={false}
                            checkedIcon={false}
                            width = {75}
                        />
                    </label>
                    <label className="labelWorkload">Carga de Trabajo</label>
                </div>
                <div className="chartOne">
                    <div className="headerChart headerDesapear">
                        <h5 className="titleChart">
                            Comparativo de promedios por periodo académico
                        </h5>
                    </div>
                    <div className="rootChart headerDesapear">
                        <div className="chartLine"></div>
                        <div className="legendChart"></div>
                    </div>
                </div>
                <div className="chartTwo">
                    <div className="headerChart">
                        <h5 className="titleChart">
                            Asignaturas cursadas por periodo académico
                        </h5>
                    </div>
                    <div className="rootChart">
                        <div className="chartRect"></div>
                        <div className="legendChartRect">
                            <div className="legendText"></div>
                            <div className="switchRect">
                                <p className="labelMatricula">Matricula #</p>
                                <label className="switchMatriculaComplejidad">
                                    <Switch className="react-switch" checked={this.state.changed} onChange={this.changeSwitch.bind(this)}
                                        onColor="#86d3ff"
                                        onHandleColor="#2693e6"
                                        uncheckedIcon={false}
                                        checkedIcon={false}
                                    />
                                </label>
                                <p className="labelComplejidad">Complejidad Asignatura</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
                {/**CREO EL TOOL TIP PARA MOSTRAR LA INFORMACION AL USUARIO CUANDO HAGA UN MOUSEOVER SOBRE LOS PUNTOS 
                EN LA GRAFICA DE LINEAS COMPARATIVA CON LOS PROMEDIOS DE SUS COMPANEROS */}
                {/* <div className="ttooltip">
                    <table>
                        <tbody>
                            <tr>
                                <td><strong>Periodo Académico: </strong> <label className="tip-periodo" id="tip-periodo">45</label></td>
                                
                            </tr>
                            <tr>
                                <td><br></br></td>
                            </tr>
                            <tr>
                                <td><strong className="tip-estudiante" id="tip-estudiante">{estudiante} </strong></td>
                            </tr>
                            <tr>
                                <td><strong className="ttooltip-font">Promedio Aignaturas Cursadas: </strong><label className="tip-promedio" id="tip-promedio">60</label></td>
                                
                            </tr>
                            <tr>
                                <td><br></br></td>
                            </tr>
                            <tr>
                                <td><strong>RENDIMIENTO DE ASIGNATURAS CURSADAS </strong></td>
                            </tr>
                            <tr>
                                <td><strong className="ttooltip-font">Asignaturas Aprobadas: </strong><label className="tip-n-approbed" id="tip-n-approbed" ></label></td>
                            </tr>
                            <tr>
                                <td><strong className="ttooltip-font">Promedio de Asignaturas Aprobadas: </strong><label className="tip-average-approbed" id="tip-average-approbed" ></label></td>
                            </tr>
                            <tr>
                                <td><strong className="ttooltip-font">Promedio de Asignaturas Reprobadas: </strong><label className="tip-average-repprobed" id="tip-average-repprobed"></label></td>
                            </tr>
                            <tr>
                                <td><br></br></td>
                            </tr>
                            <tr>
                                <td><strong> RENDIMIENTO DE COMPANEROS DE AULA</strong></td>
                            </tr>
                            <tr>
                                <td><strong className="ttooltip-font">Nota Promedio: </strong><label className="tip-partners-average" id="tip-partners-average"></label></td>
                            </tr>
                        </tbody>
                    </table>
                </div> */}
                {/*Lo pongo al ultimo porq sino con el bootstrap da error*/}
                {this.state.showPopUp ? 
                <PopUpAsignatura
                    
                  asignatura={this.state.materia}
                  closePopup={this.togglePopup.bind(this)}
                  style={this.state.style}
                  aprobacion={this.state.aprobacion}
                  grades = {this.state.calificaciones}
                  datagraph = {this.state.datagraph}
                  position = {this.state.positionStudentGraph}
                  closePopupOpenGrow = {this.showPopUpGrow.bind(this)}
                  popUpPos={this.state.popUpPos}
                />
                : null
              }
            </div>
        );
    }
}
// function showAlert(e, index){
//     e.preventDefault();
//     this.setState({materia: index});
//     alert("se dio un click "+index+" "+e.className);
//     return true;
// }
export default Dash;
// export function authentication (){
//     console.log("entraaaaaaaaaaaaaaa ");
//     test();
// }

