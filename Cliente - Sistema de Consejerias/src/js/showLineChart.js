import * as d3 from 'd3';

//defino la opcaidad cuando haga click sobre la leyenda en algun nombre (companeros o estudiante)
//con esto se va a habilitar o desabilitar.
/*const ENABLED_OPACITY = 1;
const DISABLED_OPACITY = .2;

//formato para la fecha
const timeFormatter = d3.timeFormat('%d-%m-%Y');
export function mostrarGrafico(dataset){
    //console.log("Solo los anios "+dataset[1].tipo+" "+dataset[1].fecha+"  "+dataset[1].data);
    //defino los margenes para la grafica
    const margin = {top: 20, right: 20, bottom: 50, left: 50};
    const previewMargin = {top: 10, right: 10, bottom: 15, left: 30};
    const width = 600 - margin.right - margin.left;
    const height = 500 - margin.top - margin.bottom;

    //defino el ratio para el preview
    const ratio = 4;
    const previewWidth = width / ratio;
    const previewHeight = height /ratio;

    //defino la escala con el rango para x
    const x = d3.scaleTime()
        .range([0, width]);
    //defino la escala con el rango para y
    const y = d3.scaleLinear()
        .range([height, 0]);
    
    let rescaledX = x;
    let rescaledY = y;
    //defino la escala con el rango para el preview de x
    const previewX = d3.scaleTime()
        .range([0, previewWidth]);
    //defino la escala con el rango para el preview de y
    const previewY = d3.scaleLinear()
        .range([previewHeight, 0]);
    //defino la escala de colores, en este caso solo dos, uno para companeros de aula y otro para estudiante
    const colorScale = d3.scaleOrdinal()
        .range([
            'black',
            '#4c78a8'
        ]);
    //defino el ancho del area de la grafica
    const chartAreaWidth = width + margin.right + margin.left;
    const chartAreaHeight = height + margin.top + margin.bottom;

    ///defino el zoom para la grafica.
    const zoom = d3.zoom()
    .scaleExtent([1, 10])
    .translateExtent([[0, 0], [chartAreaWidth, chartAreaHeight]])
    .on('start', () => {
      hoverDot
        .attr('cx', -5)
        .attr('cy', 0);
    })
    .on('zoom', zoomed);

    //creo la imagen svg en el div de clase chartLine
    const svg = d3.select('.chartLine')
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', `translate(${ margin.left },${ margin.top })`);

    ///doy formato a los datos que voy a usar
    dataset.forEach(function(d){
        d.fecha = d.fecha;
        d.data = +d.data;
       // console.log(d.fecha+" =============== "+d.data);
    });

    //defino el dominio para x
    x.domain(d3.extent(dataset, d => d.fecha));
    //defino el dominio para y
    y.domain([0, d3.max(dataset, d => d.data)]);
    //defino el dominio para previewX
    previewX.domain(d3.extent(dataset, d => d.fecha));
    //defino el dominio para previewY
    previewY.domain([0, d3.max(dataset, d => d.data)]);
    //defino el dominio para la escala de color
    colorScale.domain(d3.map(dataset, d => d.id).keys());

    //defino el axis para x
    const xAxis = d3.axisBottom(x)
        .ticks((width + 2) / (height + 2) * 5)
        .tickSize(-height - 6)
        .tickPadding(10);

    //defino el axis para x preview
    const xAxisPreview = d3.axisBottom(previewX)
        .tickSize(4)
        .tickValues(previewX.domain())
        .tickFormat(d3.timeFormat('%b %Y'));

    //defino el axis para el y preview
    const yAxisPreview = d3.axisLeft(previewY)
        .tickValues(previewY.domain())
        .tickSize(3)
        .tickFormat(d => d + '');

    //defino el axis para y
    const yAxis = d3.axisRight(y)
        .ticks(5)
        .tickSize(7 + width)
        .tickPadding(-11 - width)
        .tickFormat(d => d + '');
    //agrego el axis x en el svg
    const xAxisElement =  svg.append('g')
        .attr('class', 'axis x-axis')
        .attr('transform', `translate(0, ${height + 6})`)
        .call(xAxis);
    //agrego el axis y  en el svg
    const yAxisElement = svg.append('g')
        .attr('transform', 'translate(-7, 0)')
        .attr('class', 'axis y-axis')
        .call(yAxis);
    //agrego una linea como el eje x
    svg.append('g')
        .attr('transform', `translate(0,${ height })`)
        .call(d3.axisBottom(x).ticks(0));
    //agrego una linea como el eje y
    svg.append('g')
        //.attr('transform', 'translate(5,0)')
        .call(d3.axisLeft(y).ticks(0));
    //agrego defs (son como agrupaciones de elementos que no se visualizan sin que se los invoque desde algun lugar,
    //esto significa que no se vana  visualizar aunque los este agregando en este momento,
    //puesto que el agregarlos no significa que los invoque, mas adelante se los va a invocar.
    svg.append('defs').append('clipPath')
        .attr('id', 'clipLine')
        .append('rect')
        .attr('width', width)
        .attr('height', height);
    //agrupo los datos de acuerdo al id (1=companeros, 2= estudiante)
    const nestById = d3.nest()
        .key(d => d.id)
        .sortKeys((v1, v2) => (parseInt(v1, 10) > parseInt(v2, 10) ? 1 : -1))
        .entries(dataset);
    //console.log("este es el nested del dataset "+nestById);
    //agrego los nombres al objeto dataNamesId
    const dataNamesById = {};
    nestById.forEach(item => {
      //  console.log(item);
        dataNamesById[item.key] = item.values[0].tipo;
    });

    //aun no se para que es ggg
    const tipos = {};
    d3.map(dataset, d => d.id)
        .keys()
        .forEach(function(d, i){
            tipos[d] = {
                data: nestById[i].values,
                enabled: true
            };
        });
    
    ///indico que la variable tiposIds es igual a las llaves de tipos
    const tiposIds = Object.keys(tipos);

    //genero las lineas
    const lineGenerator = d3.line()
        .x(d => rescaledX(d.fecha))
        .y(d => rescaledY(d.data));
    
    ///agrupo por fecha
    const nestByDate = d3.nest()
        .key(d => d.fecha)
        .entries(dataset);
    
    //extraigo los valores o datos por fecha
    const dataBydate = {};
    nestByDate.forEach(dateItem => {
        dataBydate[dateItem.key] = {};

        dateItem.values.forEach(item => {
            dataBydate[dateItem.key][item.id] = item.data;
        });
    });

    //defino el container para la leyenda
    const legendContainer = d3.select('.legendChart');

    //defino la imagen svg para la leyenda
    const legendsSvg = legendContainer
        .append('svg');
    
    //agrego la fecha a las leyendas
    const legendsDate = legendsSvg.append('text')
        .attr('visibility', 'hidden')
        .attr('x', 0)
        .attr('y', 10);
    //ahora agrego las leyendas
    //linea numero 186 es la que debo hacer
    const legends = legendsSvg.attr('width', 210)
        .attr('height', 353)
        .selectAll('g')
        .data(tiposIds)
        .enter()
        .append('g')
        .attr('class', 'legend-item')
        .attr('transform', (id, index) => `translate(0,${ index * 20 + 20})`)
        .on('click', clickLegendRectHandler);
    ///agrego los valores de las leyendas
    const legendsValues = legends
        .append('text')
        .attr('x', 0)
        .attr('y', 10)
        .attr('class', 'legend-value');
    
    ///ahora agrego los rectangulos con sus respectivos colores en la leyenda
    legends.append('rect')
        .attr('x', 58)
        .attr('y', 0)
        .attr('width', 12)
        .attr('height', 12)
        .style('fill', id => colorScale(id))
        .select(function() { return this.parentNode; })
        .append('text')
        .attr('x', 78)
        .attr('y', 10)
        .text(id => dataNamesById[id])
        .attr('class', 'legend-text')
        .style('text-anchor', 'start');

    //agrego el container(div) de las opciones exra (mostrar todas las graficas, ocultar todas las graficas)
    const extraOptionsContainer = legendContainer.append('div')
        .attr('class', 'extra-options-container');
    
    //agrego un div denominado hide all options que al darle click ocultara todo
    extraOptionsContainer.append('div')
        .attr('class', 'hide-all-option')
        .text('hide all')
        .on('click', () => {
            tiposIds.forEach(id => {
                tipos[id].enabled = false;
            });

            singleLineSelected = false;

            redrawChart();
        });

    //agrego ahora el div que al dar clic mostrara todos
    extraOptionsContainer.append('div')
        .attr('class', 'show-all-option')
        .text('show all')
        .on('click', () => {
            tiposIds.forEach(id => {
                tipos[id].enabled = true;
            });

            singleLineSelected = false;

            redrawChart();
        });
        //agrego el contenedor de lineas
    const linesContainer = svg.append('g')
        .attr('clip-path', 'url(#clip)');
        //declaro al variable singleLineSelected, esta variable servira para saber si se a seleccionado solo un nombre
    //en la leyenda o no
    let singleLineSelected = false;

    //creo el grupo para el diagrama voronoi
    const voronoi = d3.voronoi()
        .x(d => x(d.fecha))
        .y(d => y(d.data))
        .extent([[0, 0], [width, height]]);

    //parsa cuandos e hace un hover en la linea, que aparezca los puntos
    const hoverDot = svg.append('circle')
        .attr('class', 'dot')
        .attr('r', 3)
        .attr('clip-path', 'url(#clip)')
        .style('visibility', 'hidden');

    //creo el grupo veronoi
    const voronoiGroup = svg.append('g')
        .attr('class', 'voronoi-parent')
        .attr('clip-path', 'url(#clip)')
        .append('g')
        .attr('class', 'voronoi')
        .on('mouseover', () => {
            legendsDate.style('visibility', 'visible');
            hoverDot.style('visibility', 'visible');
        })
        .on('mouseout', () => {
            legendsValues.text('');
            legendsDate.style('visibility', 'hidden');
            hoverDot.style('visibility', 'hidden');
        });
    
    //creo el nodo para hacer zoom
    const zoomNode = d3.select('.voronoi-parent').call(zoom);

    //creo el checknbox para resetear el zoom, es decir, que vuelva a la normalidad
    d3.select('.reset-zoom-buttonChart').on('click', () => {
        rescaledX = x;
        rescaledY = y;
        ///creo la animacion para el reseteo del zoom (durara 750 milisegundos)
        d3.select('.voronoi-parent').transition()
        .duration(750)
        .call(zoom.transform, d3.zoomIdentity);
    });

    ///que el checkbox muestre y oculte el voronoi
    d3.select('#show-voronoiChart')
    .property('disabled', false)
    .on('change', function () {
        voronoiGroup.classed('voronoi-show', this.checked);
    });
    
    ///creo el preview con los dato snecesarios
    const preview = d3.select('.previewChart')
        .append('svg')
        .attr('width', previewWidth - previewMargin.left - previewMargin.right)
        .attr('height', previewHeight - previewMargin.top - previewMargin.bottom)
        .append('g')
        .attr('transform', `translate(${ previewMargin.left}, ${ previewMargin.top})`);
    
    //creo el contenedor del preview
    const previewContainer = preview.append('g');

    //agrego el axis x en el preview
    preview.append('g')
        .attr('class', 'preview-axis x-axis')
        .attr('transform', `translate(0, ${ previewHeight})`)
        .call(xAxisPreview);

    //agrego el axis y en el preview
    preview.append('g')
        .attr('class', 'preview-axis y-axis')
        .attr('transform', 'translate(0,0)')
        .call(yAxisPreview);

    //croe un rectangulo donde se va a ubicar la grafica de preview con un color
    previewContainer.append('rect')
        .attr('x',0)
        .attr('y', 0)
        .attr('width', previewWidth)
        .attr('height', previewHeight)
        .attr('fill', '#dedede');

    //genero las lineas para el preview
    const previewLineGenerator =  d3.line()
        .x(d => previewX(d.fecha))
        .y(d => previewY(d.data));

    //agrego una recta para cuiando se este arrastrando el mouse sobre la grafica principal
    //para que se haga el zoom
    const draggedNode = previewContainer
        .append('rect')
        .data([{x: 0, y: 0}])
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', previewWidth)
        .attr('height', previewHeight)
        .attr('fill', 'rgba(250, 235, 215, 0.78)')
        .call(d3.drag().on('drag', dragged));

    redrawChart();
    //funcion que refresca la grafica
    function redrawChart(showingTiposIds) {
        
        //activo los nombres estudiante y companeros
        const enabledTiposIds =  showingTiposIds || tiposIds.filter(id => tipos[id].enabled);

        const paths = linesContainer
            .selectAll('.line')
            .data(enabledTiposIds);
        
        paths.exit().remove();

        if(enabledTiposIds.length ===1 ) {
            const previewPath = previewContainer
                .selectAll('path')
                .data(enabledTiposIds);
            previewPath.exit().remove();
            //dibuljo las lienas en el preview
            previewPath
                .enter()
                .append('path')
                .merge(previewPath)
                .attr('class', 'line')
                .attr('d', id => previewLineGenerator(tipos[id].data))
                .style('stroke', id => colorScale(id));
        }
        //dibujo todas las lineas de la grafica principal
        paths
            .enter()
            .append('path')
            .merge(paths)
            .attr('class', 'line')
            .attr('id', id => `tipo-${ id }`)
            .attr('d', id => lineGenerator(tipos[id].data))
            .style('stroke', id => colorScale(id));

        //doy la opcidad a la leyenda si esq esta enabled o disabled
        legends.each(function(id){
            const opacityValue = enabledTiposIds.indexOf(id) >=0 ? ENABLED_OPACITY : DISABLED_OPACITY;

            d3.select(this).attr('opacity', opacityValue);
        });

        const filteredData = dataset.filter(dataItem => enabledTiposIds.indexOf(dataItem.id) >= 0);

        //muestro las lineas voronoi
        const voronoiPaths = voronoiGroup.selectAll('path')
            .data(voronoi.polygons(filteredData));

        voronoiPaths.exit().remove();
        
        //agtego lineas al voronoi y tambien los eventos del mouse para cada linea
        voronoiPaths
            .enter()
            .append('path')
            .merge(voronoiPaths)
            .attr('d', d => (d ? `M${d.join('L')}Z` : null))
            .on('mouseover', voronoiMouseOver)
            .on('mouseout', voronoiMouseOut)
            .on('click', voronoiClick);
            
    }

    function clickLegendRectHandler(id) {
        if (singleLineSelected) {
          const newEnabledRegions = singleLineSelected === id ? [] : [singleLineSelected, id];
    
          tiposIds.forEach(currentTipoId => {
            tipos[id].enabled = newEnabledRegions.indexOf(currentTipoId) >= 0;
          });
        } else {
          tipos[id].enabled = !tipos[id].enabled;
        }
    
        singleLineSelected = false;
    
        redrawChart();
      }
    //cuando paso con el mouse sobre una linea
    function voronoiMouseOver(d){
        console.log("entraaaaaaaa");
        //obtengo el zoom o nodo donde se va a hacer zoom
        const transform = d3.zoomTransform(d3.select('.voronoi-parent').node());

        //no estoy seguro si es d.data o d.tipos
        legendsDate.text(timeFormatter(d.data.fecha));
        //igual no se si es d.data o d.tipo
        legendsValues.text(dataItem => {
            const value =  dataBydate[d.data.fecha][dataItem];
            return value;
        });
    
        //indico que la region de la area tal esta siendo pasado el mouse
        d3.select(`#tipo-${ d.data.id}`).classed('region-hover', true);

        //agrego los datos al preview, los datos solo de la linea que esta l mosuse sobre ella
        const previewPath = previewContainer
            .selectAll('path')
            .data([d.data.id]);
        previewPath.exit().remove();

        //dibujo la linea en el prview que se esta pasado el mouse por encima
        previewPath
            .enter()
            .append('path')
            .merge(previewPath)
            .attr('class', 'line')
            .attr('d', id => previewLineGenerator(tipos[id].data))
            .style('stroke', id => colorScale(id));

        hoverDot
            .attr('cx', () => transform.applyX(x(d.data.fecha)))
            .attr('cy', () => transform.applyY(y(d.data.data)))
    }

    ///cuando se queita el mouse hover
    function voronoiMouseOut(d){
        console.log("Mouse out");
        if(d) {
            //quito la seleccion , es decir, quito la clase region hover, por lo cual el css tambn
            d3.select(`#region-${ d.data.id }`).classed('region-hover', false);
        }
    }

    //cuando se da click en una linea
    function voronoiClick(d){
        console.log("mouse click");
        //en caso de que solo una linea haya sido seleccionada
        if(singleLineSelected) {
            singleLineSelected = false;

            redrawChart();//refresco el chart
        }else{//en caso de que sea falso mando a graicar solo la linea seleccionada, las otras desapareceran
            const id = d.data.id;

            singleLineSelected = id;

            redrawChart([id]);//refresco la grafica pero solo con la linea seleccionada
        }
    }

    function clamp(number, bottom, top) {
        let result = number;

        if(number < bottom) {
            result = bottom;
        }

        if(number > top){
            result = top;
        }
        return result;
    }

    ///funcion para cuando el mouse esta siendo arrastrado sobre la grafica principal
    function dragged(d) {
        console.log("mouse daggeeeeeeeedddddd");
        const draggedNodeWidth = draggedNode.attr('width');
        const draggedNodeHeight = draggedNode.attr('height');
        const x = clamp(d3.event.x, 0, previewWidth - draggedNodeWidth);
        const y = clamp(d3.event.y, 0, previewHeight - draggedNodeHeight);

        d3.select(this)
            .attr('x', d.x = x)
            .attr('y', d.y = y);
        
        zoomNode.call(zoom.transform, d3.zoomIdentity
            .scale(currentTransformationValue)
            .translate(-x * ratio, -y * ratio)
            );
    }

    let currentTransformationValue = 1;

    function zoomed () {
        console.log("Mouse zoooooommm");
        const transformation = d3.event.transform;

        const rightEdge = Math.abs(transformation.x) / transformation.k + width / transformation.k;
        const bottomEdge = Math.abs(transformation.y) / transformation.k + height / transformation.k;

        if (rightEdge > width){
            transformation.x = -(width * transformation.k - width);
        }

        if(bottomEdge > height){
            transformation.y = -(height * transformation.k - height);
        }

        rescaledX = transformation.rescaleX(x);
        rescaledY = transformation.rescaleY(y);

        xAxisElement.call(xAxis.scale(rescaledX));
        yAxisElement.call(yAxis.scale(rescaledY));

        linesContainer.selectAll('path')
            .attr('d', id => {
                return d3.line()
                    .defined(d => d.parent !==0)
                    .x(d => rescaledX(d.fecha))
                    .y(d => rescaledY(d.data))(tipos[id].data);
            });
        voronoiGroup
            .attr('transform', transformation);

        const xPreviewPosition = previewX.range().map(transformation.invertX, transformation)[0];
        const yPreviewPosition = previewY.range().map(transformation.invertY, transformation)[1];

        currentTransformationValue = transformation.k;

        draggedNode
            .data([{x: xPreviewPosition / ratio, y: yPreviewPosition /ratio}])
            .attr('x', d => d.x)
            .attr('y', d => d.y)
            .attr('width', previewWidth / transformation.k)
            .attr('height', previewHeight / transformation.k);
    }
    //console.log("data names  by id "+dataNamesById[2]);
}*/









function arrayToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}` + '\r\n';

  return array.reduce((str, next) => {
      str += `${Object.values(next).map(value => `"${value}"`).join(",")}` + '\r\n';
      return str;
     }, str);
}

export function mostrarGrafico(dataa){
  console.log(dataa);
  console.log(JSON.stringify(dataa));
  console.log(arrayToCSV(dataa));
  // d3.csv('https://raw.githubusercontent.com/levvsha/d3-in-all-its-glory-en/master/stats/data.csv').then(data => draw(data))
  // }
  d3.csv('https://raw.githubusercontent.com/kikeulloa/testing/master/datos.csv').then(data => draw(dataa))
  }

const ENABLED_OPACITY = 1;
const DISABLED_OPACITY = .2;

const timeFormatter = d3.timeFormat('%d-%m-%Y');

export function draw(data) {
  console.log(data);
    //console.log("Este es el data "+data.length+"");
  const margin = { top: 20, right: 20, bottom: 50, left: 50 };
  const previewMargin = { top: 10, right: 10, bottom: 15, left: 30 };
  const width = 600 - margin.left - margin.right;
  const height = 500 - margin.top - margin.bottom;

  const ratio = 4;

  const previewWidth = width / ratio;
  const previewHeight = height / ratio;

  const x = d3.scaleTime()
    .range([0, width]);

  const y = d3.scaleLinear()
    .range([height, 0]);

  let rescaledX = x;
  let rescaledY = y;

  const previewX = d3.scaleTime()
    .range([0, previewWidth]);

  const previewY = d3.scaleLinear()
    .range([previewHeight, 0]);

  const colorScale = d3.scaleOrdinal()
    .range([
      '#4c78a8',
      '#9ecae9',
      '#f58518',
      '#ffbf79',
      '#54a24b',
      '#88d27a',
      '#b79a20',
      '#439894',
      '#83bcb6',
      '#e45756',
      '#ff9d98',
      '#79706e',
      '#bab0ac',
      '#d67195',
      '#fcbfd2',
      '#b279a2',
      '#9e765f',
      '#d8b5a5'
    ]);
console.log ("Entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
  const chartAreaWidth = width + margin.left + margin.right;
  const chartAreaHeight = height + margin.top + margin.bottom;

  const zoom = d3.zoom()
    .scaleExtent([1, 10])
    .translateExtent([[0, 0], [chartAreaWidth, chartAreaHeight]])
    .on('start', () => {
      hoverDot
        .attr('cx', -5)
        .attr('cy', 0);
    })
    .on('zoom', zoomed);

  const svg = d3.select('.chartLine')
    .append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
    .append('g')
    .attr('transform', `translate(${ margin.left },${ margin.top })`);

  data.forEach(function (d) {
    d.fecha = new Date(d.fecha);
    d.data = +d.data;
  });

  x.domain(d3.extent(data, d => d.fecha));
  y.domain([0, d3.max(data, d => d.data)]);
  previewX.domain(d3.extent(data, d => d.fecha));
  previewY.domain([0, d3.max(data, d => d.data)]);
  colorScale.domain(d3.map(data, d => d.id).keys());

  const xAxis = d3.axisBottom(x)
    .ticks((width + 2) / (height + 2) * 5)
    .tickSize(-height - 6)
    .tickPadding(10);

  const xAxisPreview = d3.axisBottom(previewX)
    .tickSize(4)
    .tickValues(previewX.domain())
    .tickFormat(d3.timeFormat('%b %Y'));

  const yAxisPreview = d3.axisLeft(previewY)
    .tickValues(previewY.domain())
    .tickSize(3)
    .tickFormat(d => Math.round(d) + '%');

  const yAxis = d3.axisRight(y)
    .ticks(5)
    .tickSize(7 + width)
    .tickPadding(-11 - width)
    .tickFormat(d => d + '%');

  const xAxisElement = svg.append('g')
    .attr('class', 'axis x-axis')
    .attr('transform', `translate(0,${ height + 6 })`)
    .call(xAxis);

  const yAxisElement = svg.append('g')
    .attr('transform', 'translate(-7, 0)')
    .attr('class', 'axis y-axis')
    .call(yAxis);

  svg.append('g')
    .attr('transform', `translate(0,${ height })`)
    .call(d3.axisBottom(x).ticks(0));

  svg.append('g')
    .call(d3.axisLeft(y).ticks(0));

  svg.append('defs').append('clipPath')
    .attr('id', 'clip')
    .append('rect')
    .attr('width', width)
    .attr('height', height);

  const nestByRegionId = d3.nest()
    .key(d => d.id)
    .sortKeys((v1, v2) => (parseInt(v1, 10) > parseInt(v2, 10) ? 1 : -1))
    .entries(data);
  console.log("este es el nested del dataset "+nestByRegionId);
  const regionsNamesById = {};

  nestByRegionId.forEach(item => {
    regionsNamesById[item.key] = item.values[0].tipo;
  });

  const regions = {};

  d3.map(data, d => d.id)
    .keys()
    .forEach(function (d, i) {
      regions[d] = {
        data: nestByRegionId[i].values,
        enabled: true
      };
    });

  const regionsIds = Object.keys(regions);

  const lineGenerator = d3.line()
    .x(d => rescaledX(d.fecha))
    .y(d => rescaledY(d.data));

  const nestByDate = d3.nest()
    .key(d => d.fecha)
    .entries(data);

  const percentsByDate = {};

  nestByDate.forEach(dateItem => {
    percentsByDate[dateItem.key] = {};

    dateItem.values.forEach(item => {
      percentsByDate[dateItem.key][item.id] = item.data;
    });
  });

  const legendContainer = d3.select('.legendChart');

  const legendsSvg = legendContainer
    .append('svg');

  const legendsDate = legendsSvg.append('text')
    .attr('visibility', 'hidden')
    .attr('x', 0)
    .attr('y', 10);

  const legends = legendsSvg.attr('width', 210)
    .attr('height', 353)
    .selectAll('g')
    .data(regionsIds)
    .enter()
    .append('g')
    .attr('class', 'legend-item')
    .attr('transform', (id, index) => `translate(0,${ index * 20 + 20 })`)
    .on('click', clickLegendRectHandler);

  const legendsValues = legends
    .append('text')
    .attr('x', 0)
    .attr('y', 10)
    .attr('class', 'legend-value');

  legends.append('rect')
    .attr('x', 58)
    .attr('y', 0)
    .attr('width', 12)
    .attr('height', 12)
    .style('fill', id => colorScale(id))
    .select(function() { return this.parentNode; })
    .append('text')
    .attr('x', 78)
    .attr('y', 10)
    .text(id => regionsNamesById[id])
    .attr('class', 'legend-text')
    .style('text-anchor', 'start');

  const extraOptionsContainer = legendContainer.append('div')
    .attr('class', 'extra-options-container');

  extraOptionsContainer.append('div')
    .attr('class', 'hide-all-option')
    .text('hide all')
    .on('click', () => {
      regionsIds.forEach(id => {
        regions[id].enabled = false;
      });

      singleLineSelected = false;

      redrawChart();
    });

  extraOptionsContainer.append('div')
    .attr('class', 'show-all-option')
    .text('show all')
    .on('click', () => {
      regionsIds.forEach(id => {
        regions[id].enabled = true;
      });

      singleLineSelected = false;

      redrawChart();
    });

  const linesContainer = svg.append('g')
    .attr('clip-path', 'url(#clip)');

  let singleLineSelected = false;

  const voronoi = d3.voronoi()
    .x(d => x(d.fecha))
    .y(d => y(d.data))
    .extent([[0, 0], [width, height]]);

  const hoverDot = svg.append('circle')
    .attr('class', 'dot')
    .attr('r', 3)
    .attr('clip-path', 'url(#clip)')
    .style('visibility', 'hidden');

  let voronoiGroup = svg.append('g')
    .attr('class', 'voronoi-parent')
    .attr('clip-path', 'url(#clip)')
    .append('g')
    .attr('class', 'voronoi')
    .on('mouseover', () => {
      legendsDate.style('visibility', 'visible');
      hoverDot.style('visibility', 'visible');
    })
    .on('mouseout', () => {
      legendsValues.text('');
      legendsDate.style('visibility', 'hidden');
      hoverDot.style('visibility', 'hidden');
    });

  const zoomNode = d3.select('.voronoi-parent').call(zoom);

  d3.select('.reset-zoom-buttonChart').on('click', () => {
    rescaledX = x;
    rescaledY = y;

    d3.select('.voronoi-parent').transition()
      .duration(750)
      .call(zoom.transform, d3.zoomIdentity);
  });

  d3.select('#show-voronoiChart')
    .property('disabled', false)
    .on('change', function () {
      voronoiGroup.classed('voronoi-show', this.checked);
    });

  const preview = d3.select('.previewChart')
    .append('svg')
    .attr('width', previewWidth + previewMargin.left + previewMargin.right)
    .attr('height', previewHeight + previewMargin.top + previewMargin.bottom)
    .append('g')
    .attr('transform', `translate(${ previewMargin.left },${ previewMargin.top })`);

  const previewContainer = preview.append('g');

  preview.append('g')
    .attr('class', 'preview-axis x-axis')
    .attr('transform', `translate(0,${ previewHeight })`)
    .call(xAxisPreview);

  preview.append('g')
    .attr('class', 'preview-axis y-axis')
    .attr('transform', 'translate(0, 0)')
    .call(yAxisPreview);

  previewContainer.append('rect')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', previewWidth)
    .attr('height', previewHeight)
    .attr('fill', '#dedede');
  
  const previewLineGenerator = d3.line()
    .x(d => previewX(d.fecha))
    .y(d => previewY(d.data));

  const draggedNode = previewContainer
    .append('rect')
    .data([{ x: 0, y: 0 }])
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', previewWidth)
    .attr('height', previewHeight)
    .attr('fill', 'rgba(250, 235, 215, 0.78)')
    .call(d3.drag().on('drag', dragged));

  redrawChart();

  function redrawChart(showingRegionsIds) {
    const enabledRegionsIds = showingRegionsIds || regionsIds.filter(id => regions[id].enabled);

    const paths = linesContainer
      .selectAll('.line')
      .data(enabledRegionsIds);

    paths.exit().remove();

    if (enabledRegionsIds.length === 1) {
      const previewPath = previewContainer
        .selectAll('path')
        .data(enabledRegionsIds);

      previewPath.exit().remove();

      previewPath
        .enter()
        .append('path')
        .merge(previewPath)
        .attr('class', 'line')
        .attr('d', id => previewLineGenerator(regions[id].data)
        )
        .style('stroke', id => colorScale(id));
    }

    paths
      .enter()
      .append('path')
      .merge(paths)
      .attr('class', 'line')
      .attr('id', id => `region-${ id }`)
      .attr('d', id => lineGenerator(regions[id].data)
      )
      .style('stroke', id => colorScale(id));

    legends.each(function(id) {
      const opacityValue = enabledRegionsIds.indexOf(id) >= 0 ? ENABLED_OPACITY : DISABLED_OPACITY;

      d3.select(this).attr('opacity', opacityValue);
    });

    const filteredData = data.filter(dataItem => enabledRegionsIds.indexOf(dataItem.id) >= 0);

    const voronoiPaths = voronoiGroup.selectAll('path')
      .data(voronoi.polygons(filteredData));

    voronoiPaths.exit().remove();

    voronoiPaths
      .enter()
      .append('path')
      .merge(voronoiPaths)
      .attr('d', d => (d ? `M${ d.join('L') }Z` : null))
      .on('mouseover', voronoiMouseover)
      .on('mouseout', voronoiMouseout)
      .on('click', voronoiClick);

      //console.log("entra aqui");
  }

  function clickLegendRectHandler(id) {
    
    if (singleLineSelected) {
      const newEnabledRegions = singleLineSelected === id ? [] : [singleLineSelected, id];

      regionsIds.forEach(currentRegionId => {
        regions[currentRegionId].enabled = newEnabledRegions.indexOf(currentRegionId) >= 0;
      });
    } else {
      regions[id].enabled = !regions[id].enabled;
    }

    singleLineSelected = false;

    redrawChart();
  }

  function voronoiMouseover(d) {
    console.log("se dio click");
    const transform = d3.zoomTransform(d3.select('.voronoi-parent').node());

    legendsDate.text(timeFormatter(d.data.fecha));

    legendsValues.text(dataItem => {
      const value = percentsByDate[d.data.fecha][dataItem];

      return value ? value + '%' : 'Н/Д';
    });

    d3.select(`#region-${ d.data.id }`).classed('region-hover', true);

    const previewPath = previewContainer
      .selectAll('path')
      .data([d.data.id]);

    previewPath.exit().remove();

    previewPath
      .enter()
      .append('path')
      .merge(previewPath)
      .attr('class', 'line')
      .attr('d', id => previewLineGenerator(regions[id].data)
      )
      .style('stroke', id => colorScale(id));

    hoverDot
      .attr('cx', () => transform.applyX(x(d.data.fecha)))
      .attr('cy', () => transform.applyY(y(d.data.data)));
  }

  function voronoiMouseout(d) {
    if (d) {
      d3.select(`#region-${ d.data.id }`).classed('region-hover', false);
    }
  }

  function voronoiClick(d) {
    console.log("se dio uin click");
    if (singleLineSelected) {
      singleLineSelected = false;

      redrawChart();
    } else {
      const id = d.data.id;

      singleLineSelected = id;

      redrawChart([id]);
    }
  }

  function clamp(number, bottom, top) {
    let result = number;

    if (number < bottom) {
      result = bottom;
    }

    if (number > top) {
      result = top;
    }

    return result;
  }

  function dragged(d) {
    const draggedNodeWidth = draggedNode.attr('width');
    const draggedNodeHeight = draggedNode.attr('height');
    const x = clamp(d3.event.x, 0, previewWidth - draggedNodeWidth);
    const y = clamp(d3.event.y, 0, previewHeight - draggedNodeHeight);

    d3.select(this)
      .attr('x', d.x = x)
      .attr('y', d.y = y);

    zoomNode.call(zoom.transform, d3.zoomIdentity
      .scale(currentTransformationValue)
      .translate(-x * ratio, -y * ratio)
    );
  }

  let currentTransformationValue = 1;

  function zoomed() {
    const transformation = d3.event.transform;

    const rightEdge = Math.abs(transformation.x) / transformation.k + width / transformation.k;
    const bottomEdge = Math.abs(transformation.y) / transformation.k + height / transformation.k;

    if (rightEdge > width) {
      transformation.x = -(width * transformation.k - width);
    }

    if (bottomEdge > height) {
      transformation.y = -(height * transformation.k - height);
    }

    rescaledX = transformation.rescaleX(x);
    rescaledY = transformation.rescaleY(y);

    xAxisElement.call(xAxis.scale(rescaledX));
    yAxisElement.call(yAxis.scale(rescaledY));

    linesContainer.selectAll('path')
      .attr('d', id => {
        return d3.line()
          .defined(d => d.data !== 0)
          .x(d => rescaledX(d.fecha))
          .y(d => rescaledY(d.data))(regions[id].data);
      });

    voronoiGroup
      .attr('transform', transformation);

    const xPreviewPosition = previewX.range().map(transformation.invertX, transformation)[0];
    const yPreviewPosition = previewY.range().map(transformation.invertY, transformation)[1];

    currentTransformationValue = transformation.k;

    draggedNode
      .data([{ x: xPreviewPosition / ratio, y: yPreviewPosition / ratio }])
      .attr('x', d => d.x)
      .attr('y', d => d.y)
      .attr('width', previewWidth / transformation.k)
      .attr('height', previewHeight / transformation.k);
  }
}







/*

export function mostrarGrafico(datos){
  d3.csv('https://raw.githubusercontent.com/levvsha/d3-in-all-its-glory-en/master/stats/data.csv').then(data => draw(data))
  }
  // d3.csv('https://raw.githubusercontent.com/kikeulloa/testing/master/data.csv').then(data => draw(data))
  // }
  const ENABLED_OPACITY = 1;
  const DISABLED_OPACITY = .2;
  
  const timeFormatter = d3.timeFormat('%d-%m-%Y');
  
  function draw(data) {
      //console.log("Este es el data "+data.length+"");
    const margin = { top: 20, right: 20, bottom: 50, left: 50 };
    const previewMargin = { top: 10, right: 10, bottom: 15, left: 30 };
    const width = 750 - margin.left - margin.right;
    const height = 615 - margin.top - margin.bottom;
  
    const ratio = 4;
  
    const previewWidth = width / ratio;
    const previewHeight = height / ratio;
  
    const x = d3.scaleTime()
      .range([0, width]);
  
    const y = d3.scaleLinear()
      .range([height, 0]);
  
    let rescaledX = x;
    let rescaledY = y;
  
    const previewX = d3.scaleTime()
      .range([0, previewWidth]);
  
    const previewY = d3.scaleLinear()
      .range([previewHeight, 0]);
  
    const colorScale = d3.scaleOrdinal()
      .range([
        '#4c78a8',
        '#9ecae9',
        '#f58518',
        '#ffbf79',
        '#54a24b',
        '#88d27a',
        '#b79a20',
        '#439894',
        '#83bcb6',
        '#e45756',
        '#ff9d98',
        '#79706e',
        '#bab0ac',
        '#d67195',
        '#fcbfd2',
        '#b279a2',
        '#9e765f',
        '#d8b5a5'
      ]);
  
    const chartAreaWidth = width + margin.left + margin.right;
    const chartAreaHeight = height + margin.top + margin.bottom;
  
    const zoom = d3.zoom()
      .scaleExtent([1, 10])
      .translateExtent([[0, 0], [chartAreaWidth, chartAreaHeight]])
      .on('start', () => {
        hoverDot
          .attr('cx', -5)
          .attr('cy', 0);
      })
      .on('zoom', zoomed);
  
    const svg = d3.select('.chartLine')
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', `translate(${ margin.left },${ margin.top })`);
  
    data.forEach(function (d) {
      d.date = new Date(d.date);
      d.percent = +d.percent;
    });
  
    x.domain(d3.extent(data, d => d.date));
    y.domain([0, d3.max(data, d => d.percent)]);
    previewX.domain(d3.extent(data, d => d.date));
    previewY.domain([0, d3.max(data, d => d.percent)]);
    colorScale.domain(d3.map(data, d => d.regionId).keys());
  
    const xAxis = d3.axisBottom(x)
      .ticks((width + 2) / (height + 2) * 5)
      .tickSize(-height - 6)
      .tickPadding(10);
  
    const xAxisPreview = d3.axisBottom(previewX)
      .tickSize(4)
      .tickValues(previewX.domain())
      .tickFormat(d3.timeFormat('%b %Y'));
  
    const yAxisPreview = d3.axisLeft(previewY)
      .tickValues(previewY.domain())
      .tickSize(3)
      .tickFormat(d => Math.round(d) + '%');
  
    const yAxis = d3.axisRight(y)
      .ticks(5)
      .tickSize(7 + width)
      .tickPadding(-11 - width)
      .tickFormat(d => d + '%');
  
    const xAxisElement = svg.append('g')
      .attr('class', 'axis x-axis')
      .attr('transform', `translate(0,${ height + 6 })`)
      .call(xAxis);
  
    const yAxisElement = svg.append('g')
      .attr('transform', 'translate(-7, 0)')
      .attr('class', 'axis y-axis')
      .call(yAxis);
  
    svg.append('g')
      .attr('transform', `translate(0,${ height })`)
      .call(d3.axisBottom(x).ticks(0));
  
    svg.append('g')
      .call(d3.axisLeft(y).ticks(0));
  
    svg.append('defs').append('clipPath')
      .attr('id', 'clip')
      .append('rect')
      .attr('width', width)
      .attr('height', height);
  
    const nestByRegionId = d3.nest()
      .key(d => d.regionId)
      .sortKeys((v1, v2) => (parseInt(v1, 10) > parseInt(v2, 10) ? 1 : -1))
      .entries(data);
    console.log("este es el nested del dataset "+nestByRegionId);
    const regionsNamesById = {};
  
    nestByRegionId.forEach(item => {
      regionsNamesById[item.key] = item.values[0].regionName;
    });
  
    const regions = {};
  
    d3.map(data, d => d.regionId)
      .keys()
      .forEach(function (d, i) {
        regions[d] = {
          data: nestByRegionId[i].values,
          enabled: true
        };
      });
  
    const regionsIds = Object.keys(regions);
  
    const lineGenerator = d3.line()
      .x(d => rescaledX(d.date))
      .y(d => rescaledY(d.percent));
  
    const nestByDate = d3.nest()
      .key(d => d.date)
      .entries(data);
  
    const percentsByDate = {};
  
    nestByDate.forEach(dateItem => {
      percentsByDate[dateItem.key] = {};
  
      dateItem.values.forEach(item => {
        percentsByDate[dateItem.key][item.regionId] = item.percent;
      });
    });
  
    const legendContainer = d3.select('.legendChart');
  
    const legendsSvg = legendContainer
      .append('svg');
  
    const legendsDate = legendsSvg.append('text')
      .attr('visibility', 'hidden')
      .attr('x', 0)
      .attr('y', 10);
  
    const legends = legendsSvg.attr('width', 210)
      .attr('height', 353)
      .selectAll('g')
      .data(regionsIds)
      .enter()
      .append('g')
      .attr('class', 'legend-item')
      .attr('transform', (regionId, index) => `translate(0,${ index * 20 + 20 })`)
      .on('click', clickLegendRectHandler);
  
    const legendsValues = legends
      .append('text')
      .attr('x', 0)
      .attr('y', 10)
      .attr('class', 'legend-value');
  
    legends.append('rect')
      .attr('x', 58)
      .attr('y', 0)
      .attr('width', 12)
      .attr('height', 12)
      .style('fill', regionId => colorScale(regionId))
      .select(function() { return this.parentNode; })
      .append('text')
      .attr('x', 78)
      .attr('y', 10)
      .text(regionId => regionsNamesById[regionId])
      .attr('class', 'legend-text')
      .style('text-anchor', 'start');
  
    const extraOptionsContainer = legendContainer.append('div')
      .attr('class', 'extra-options-container');
  
    extraOptionsContainer.append('div')
      .attr('class', 'hide-all-option')
      .text('hide all')
      .on('click', () => {
        regionsIds.forEach(regionId => {
          regions[regionId].enabled = false;
        });
  
        singleLineSelected = false;
  
        redrawChart();
      });
  
    extraOptionsContainer.append('div')
      .attr('class', 'show-all-option')
      .text('show all')
      .on('click', () => {
        regionsIds.forEach(regionId => {
          regions[regionId].enabled = true;
        });
  
        singleLineSelected = false;
  
        redrawChart();
      });
  
    const linesContainer = svg.append('g')
      .attr('clip-path', 'url(#clip)');
  
    let singleLineSelected = false;
  
    const voronoi = d3.voronoi()
      .x(d => x(d.date))
      .y(d => y(d.percent))
      .extent([[0, 0], [width, height]]);
  
    const hoverDot = svg.append('circle')
      .attr('class', 'dot')
      .attr('r', 3)
      .attr('clip-path', 'url(#clip)')
      .style('visibility', 'hidden');
  
    let voronoiGroup = svg.append('g')
      .attr('class', 'voronoi-parent')
      .attr('clip-path', 'url(#clip)')
      .append('g')
      .attr('class', 'voronoi')
      .on('mouseover', () => {
        legendsDate.style('visibility', 'visible');
        hoverDot.style('visibility', 'visible');
      })
      .on('mouseout', () => {
        legendsValues.text('');
        legendsDate.style('visibility', 'hidden');
        hoverDot.style('visibility', 'hidden');
      });
  
    const zoomNode = d3.select('.voronoi-parent').call(zoom);
  
    d3.select('.reset-zoom-buttonChart').on('click', () => {
      rescaledX = x;
      rescaledY = y;
  
      d3.select('.voronoi-parent').transition()
        .duration(750)
        .call(zoom.transform, d3.zoomIdentity);
    });
  
    d3.select('#show-voronoiChart')
      .property('disabled', false)
      .on('change', function () {
        voronoiGroup.classed('voronoi-show', this.checked);
      });
  
    const preview = d3.select('.previewChart')
      .append('svg')
      .attr('width', previewWidth + previewMargin.left + previewMargin.right)
      .attr('height', previewHeight + previewMargin.top + previewMargin.bottom)
      .append('g')
      .attr('transform', `translate(${ previewMargin.left },${ previewMargin.top })`);
  
    const previewContainer = preview.append('g');
  
    preview.append('g')
      .attr('class', 'preview-axis x-axis')
      .attr('transform', `translate(0,${ previewHeight })`)
      .call(xAxisPreview);
  
    preview.append('g')
      .attr('class', 'preview-axis y-axis')
      .attr('transform', 'translate(0, 0)')
      .call(yAxisPreview);
  
    previewContainer.append('rect')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', previewWidth)
      .attr('height', previewHeight)
      .attr('fill', '#dedede');
    
    const previewLineGenerator = d3.line()
      .x(d => previewX(d.date))
      .y(d => previewY(d.percent));
  
    const draggedNode = previewContainer
      .append('rect')
      .data([{ x: 0, y: 0 }])
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', previewWidth)
      .attr('height', previewHeight)
      .attr('fill', 'rgba(250, 235, 215, 0.78)')
      .call(d3.drag().on('drag', dragged));
  
    redrawChart();
  
    function redrawChart(showingRegionsIds) {
      const enabledRegionsIds = showingRegionsIds || regionsIds.filter(regionId => regions[regionId].enabled);
  
      const paths = linesContainer
        .selectAll('.line')
        .data(enabledRegionsIds);
  
      paths.exit().remove();
  
      if (enabledRegionsIds.length === 1) {
        const previewPath = previewContainer
          .selectAll('path')
          .data(enabledRegionsIds);
  
        previewPath.exit().remove();
  
        previewPath
          .enter()
          .append('path')
          .merge(previewPath)
          .attr('class', 'line')
          .attr('d', regionId => previewLineGenerator(regions[regionId].data)
          )
          .style('stroke', regionId => colorScale(regionId));
      }
  
      paths
        .enter()
        .append('path')
        .merge(paths)
        .attr('class', 'line')
        .attr('id', regionId => `region-${ regionId }`)
        .attr('d', regionId => lineGenerator(regions[regionId].data)
        )
        .style('stroke', regionId => colorScale(regionId));
  
      legends.each(function(regionId) {
        const opacityValue = enabledRegionsIds.indexOf(regionId) >= 0 ? ENABLED_OPACITY : DISABLED_OPACITY;
  
        d3.select(this).attr('opacity', opacityValue);
      });
  
      const filteredData = data.filter(dataItem => enabledRegionsIds.indexOf(dataItem.regionId) >= 0);
  
      const voronoiPaths = voronoiGroup.selectAll('path')
        .data(voronoi.polygons(filteredData));
  
      voronoiPaths.exit().remove();
  
      voronoiPaths
        .enter()
        .append('path')
        .merge(voronoiPaths)
        .attr('d', d => (d ? `M${ d.join('L') }Z` : null))
        .on('mouseover', voronoiMouseover)
        .on('mouseout', voronoiMouseout)
        .on('click', voronoiClick);
    }
  
    function clickLegendRectHandler(regionId) {
      if (singleLineSelected) {
        const newEnabledRegions = singleLineSelected === regionId ? [] : [singleLineSelected, regionId];
  
        regionsIds.forEach(currentRegionId => {
          regions[currentRegionId].enabled = newEnabledRegions.indexOf(currentRegionId) >= 0;
        });
      } else {
        regions[regionId].enabled = !regions[regionId].enabled;
      }
  
      singleLineSelected = false;
  
      redrawChart();
    }
  
    function voronoiMouseover(d) {
      const transform = d3.zoomTransform(d3.select('.voronoi-parent').node());
  
      legendsDate.text(timeFormatter(d.data.date));
  
      legendsValues.text(dataItem => {
        const value = percentsByDate[d.data.date][dataItem];
  
        return value ? value + '%' : 'Н/Д';
      });
  
      d3.select(`#region-${ d.data.regionId }`).classed('region-hover', true);
  
      const previewPath = previewContainer
        .selectAll('path')
        .data([d.data.regionId]);
  
      previewPath.exit().remove();
  
      previewPath
        .enter()
        .append('path')
        .merge(previewPath)
        .attr('class', 'line')
        .attr('d', regionId => previewLineGenerator(regions[regionId].data)
        )
        .style('stroke', regionId => colorScale(regionId));
  
      hoverDot
        .attr('cx', () => transform.applyX(x(d.data.date)))
        .attr('cy', () => transform.applyY(y(d.data.percent)));
    }
  
    function voronoiMouseout(d) {
      if (d) {
        d3.select(`#region-${ d.data.regionId }`).classed('region-hover', false);
      }
    }
  
    function voronoiClick(d) {
      if (singleLineSelected) {
        singleLineSelected = false;
  
        redrawChart();
      } else {
        const regionId = d.data.regionId;
  
        singleLineSelected = regionId;
  
        redrawChart([regionId]);
      }
    }
  
    function clamp(number, bottom, top) {
      let result = number;
  
      if (number < bottom) {
        result = bottom;
      }
  
      if (number > top) {
        result = top;
      }
  
      return result;
    }
  
    function dragged(d) {
      const draggedNodeWidth = draggedNode.attr('width');
      const draggedNodeHeight = draggedNode.attr('height');
      const x = clamp(d3.event.x, 0, previewWidth - draggedNodeWidth);
      const y = clamp(d3.event.y, 0, previewHeight - draggedNodeHeight);
  
      d3.select(this)
        .attr('x', d.x = x)
        .attr('y', d.y = y);
  
      zoomNode.call(zoom.transform, d3.zoomIdentity
        .scale(currentTransformationValue)
        .translate(-x * ratio, -y * ratio)
      );
    }
  
    let currentTransformationValue = 1;
  
    function zoomed() {
      const transformation = d3.event.transform;
  
      const rightEdge = Math.abs(transformation.x) / transformation.k + width / transformation.k;
      const bottomEdge = Math.abs(transformation.y) / transformation.k + height / transformation.k;
  
      if (rightEdge > width) {
        transformation.x = -(width * transformation.k - width);
      }
  
      if (bottomEdge > height) {
        transformation.y = -(height * transformation.k - height);
      }
  
      rescaledX = transformation.rescaleX(x);
      rescaledY = transformation.rescaleY(y);
  
      xAxisElement.call(xAxis.scale(rescaledX));
      yAxisElement.call(yAxis.scale(rescaledY));
  
      linesContainer.selectAll('path')
        .attr('d', regionId => {
          return d3.line()
            .defined(d => d.percent !== 0)
            .x(d => rescaledX(d.date))
            .y(d => rescaledY(d.percent))(regions[regionId].data);
        });
  
      voronoiGroup
        .attr('transform', transformation);
  
      const xPreviewPosition = previewX.range().map(transformation.invertX, transformation)[0];
      const yPreviewPosition = previewY.range().map(transformation.invertY, transformation)[1];
  
      currentTransformationValue = transformation.k;
  
      draggedNode
        .data([{ x: xPreviewPosition / ratio, y: yPreviewPosition / ratio }])
        .attr('x', d => d.x)
        .attr('y', d => d.y)
        .attr('width', previewWidth / transformation.k)
        .attr('height', previewHeight / transformation.k);
    }
  }
  */