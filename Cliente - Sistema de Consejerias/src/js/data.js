const div1="este es el div 1";

//al exportar la funcion puede invocarla desde otro archivo y evaluar para obtener el resultado de esta funcion
export function data(evaluar, a , b){
    let c=a+b;
    let array=[];
    array.push(a);
    array.push(b);
    return "Se evaluo "+evaluar+" "+a+"+"+b+"="+c+"   "+array+"   "+div1;
}

export function primerCiclo(){
    return ["Calculo Diferencial","Algebra Lineal","matematicas Discretas","Sociedad Cultura y Territorio","Introduccion a la Ingenieria","Lenguaje y COmunicacion Tecnica"];   
}

export function segundoCiclo(){
    return ["Calculo Integral","Fisica I","teoria de la computacion","Programacion I (Algoritmos/Datos/Estructuras)","Metodologia de la Investigacion","Suficiencia en Ofimatica"];   
}
export function tercerCiclo(){
    return ["Calculo en varias variables","ecuaciones diferenciales","optaiva 1","fisica II (Electricidad y magnetismo)","Programacion II (estructura de datos/a. algoritmos)","probabilidad y estadistica"];   
}
export function cuartoCiclo(){
    return ["matematicas aplicadas","metodos numericos","microprocesadores","programacion III (estructura de archivos)","Analisis y diseno de software"];   
}
export function quintoCiclo(){
    return ["organizacion y arquitectura de computadores","sistemas lineales y senales","lenguajes de programacion","bases de datos I (diseno y principios)","Investigacion de operaciones"];   
}
export function sextoCiclo(){
    return ["bases de datos II (administracion y optimizacion)","Sistemas operativos","ingenieria del software","inteligencia artificial","gestion de proyectos"];   
}
export function septimoCiclo(){
    return ["Seguridad informatica","optativa 2","redes de computadores","ingenieria de software empirica","verificacion y validacion de software","ingenieria de requerimientos"];   
}
export function octavoCiclo(){
    return ["Programacion web","sistemas distribuidos","interaccion hombre maquina","optativa 3","practica pre-profesional"];   
}
export function novenoCiclo(){
    return ["graficos por computador","responsabilidad profesional y normativa","emprendimiento e innovacion","optativa 4","practica pre-profesional II"];   
}
export function decimoCiclo(){
    return ["introduction to Physics","introduction to Physics","introduction to Physics","introduction to Physics"];   
}