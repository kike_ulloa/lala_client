import * as d3 from 'd3';
import { type } from 'os';

export function individualCoursesChart(courses, dates, grades, complejidadPorCurso, data, cursando){
   //console.log(dates);
    //console.log(courses, dates, grades, complejidadPorCurso, data, cursando);
    /**Inicia proceso para extraer la fecha */
    const nestById = d3.nest()
        .key(d => d.id)
        .sortKeys((v1, v2) => (parseInt(v1, 10) > parseInt(v2, 10) ? 1 : -1))
        .entries(data);
    //console.log(nestById);
    //agrego los nombres al objeto dataNamesId
    let student = [];
    let totalCursesPerTerm = [];
    

    let mapYears = new Map();

    let lastMonth = -1;
    let lastYear = 0;

    let aux = [];

    let fechaIds = [];//es para agregar todas las fecha sy ponerlas como timestap para el id de cada recta
    //el formato de id de cada recta sera: id=rect-id-fechaIds.
    nestById.forEach(item => { 
        //en caso de que sea la llave  = 2 significa q es el estudiante (1 = companeros, 2 = estudiante)
        if(parseInt(item.key) === 2 ){
              //obtengo la fecha par apoder ubicar los anios
            let fech = new Date(Date.parse(item.values[0].fecha));//digo que la pos cero proq necesito saber si empieza en febrero-marzo o en septiembre
            if(fech.getMonth() === 2 || fech.getMonth() === 1){//si la fecha es igual a febrero o marzo (el get month devuelve a enero como 0)
                mapYears.set(fech.getFullYear(), fech.getFullYear()); 
               // console.log(fech.getFullYear());
            }else if(fech.getMonth() === 8){//si es igual a septiembre
                mapYears.set(fech.getUTCFullYear(), null);
                //console.log(fech.getFullYear());
            }
            let index = 0;
            let contadorEst = 0;
            let indexDos = 0;
            item.values.map(val => {
                ///AQUI DEBO HACER LO MISMO QUE HICE EN EL ARCHIVO SHOWLINECOMPARATIVECHART.JS A PARTIR DE LA LINEA 60
                let d = new Date(item.values[index].fecha);
             // console.log(d.getMonth()+" "+lastMonth+"   "+d.getFullYear() + " "+(lastYear));
              if((d.getMonth() === lastMonth) && (d.getFullYear() === (lastYear + 1))){ //si es el mismo mes el mes anterior en el bucle con el mes actual del bucle (val de pos anterior con val de pos actual) significa que se salto un ciclo
                student[indexDos] = null;//no ha tomaod un ciclo
                aux[indexDos]=0;
                contadorEst+=10;
                //console.log("entra aqui");
                indexDos++;
                student[indexDos] = {//
                  x: contadorEst,
                  y: val.data
                }
                aux[indexDos] = 1;
              }else if(d.getFullYear() > (lastYear+1) && (lastYear !== 0)){//en caso de que se haya saltado mas de un ciclo
                //console.log("contador est: "+contadorEst);
                for(var j = (lastYear + 1); j< d.getFullYear(); j++){
                  if(j === (lastYear + 1)){
                    if(lastMonth === 1 || lastMonth === 2){
                      student[indexDos] = null;
                      aux[indexDos] = 0;
                      contadorEst+=10;
                      indexDos++;
                    }
                    
                    student[indexDos] = null;
                    aux[indexDos] = 0;
                    contadorEst+=10;
                    indexDos++;
                    student[indexDos] = null;
                    aux[indexDos] = 0;
                    contadorEst+=10;
                    indexDos++;
                  }
                }
                //en el caso de que sea igual al primer mes lectivo
                if(d.getMonth() === 1 || d.getMonth() === 2){
                  student[indexDos] = {
                    x: contadorEst,
                    y: val.data
                  }
                  aux[indexDos] = 1;
                }else if(d.getMonth() === 8){//si es el mes de septiembre, necesito llenar con null el mes de febrro o marzo
                  student[indexDos] = null;
                  aux[indexDos] = 0;
                  contadorEst+=10;
                  indexDos++;
                  student[indexDos] = {
                    x: contadorEst,
                    y: val.data
                  }
                  aux[indexDos] = 1;
                }
              }else{
                student[indexDos] = {
                    x: contadorEst,
                    y: val.data
                }
                aux[indexDos] = 1;
              }
                // student[index] = {
                //     x: contadorEst,
                //     y: val.data
                // }
                totalCursesPerTerm[index] = val.totalAsignaturasEnCiclo;
                if(index !== 0){
                  fech = new Date(Date.parse(item.values[index].fecha));
                  mapYears.set(fech.getFullYear(), fech.getFullYear());
                }
                fechaIds[index] = d;
                index++;
                indexDos++;
                contadorEst+=10;
                lastMonth = d.getMonth();
                lastYear= d.getFullYear();
                return student;
            });
        }
        
        
    });
    // console.log(courses.length, student.length);
    // console.log(aux);
    //modifico el array de fechas
    //lo que hago es agregar cada valor cero de aux por el mes que corresponde
    let datesAux = [];
    let indexDates = 0;
    let coursesAux = [];
    let gradesAux = [];
    let cursandoAux = [];
    for(var inde = 0; inde < aux.length; inde++){
        if(aux[inde] === 1){
            datesAux[inde] = dates[indexDates];
            coursesAux[inde] = courses[indexDates];
            gradesAux[inde] = grades[indexDates];
            cursandoAux[inde] = cursando[indexDates];
            indexDates++;
        }else{//si es cero, significa que tengo que llenar el mes que corresponde basandome en el mes anterior
            if(inde > 0){//me aseguro de que la posicion no sea cero para poder restarle uno
                if(datesAux[inde - 1]==="Sept"){//si el mes anterior que contiene es igual a septiembre, tengo que llenar con marzo
                    datesAux[inde] = "Mar";
                }else{//si no es septiembre y es marzo
                    datesAux[inde] = "Sept";
                }
                //datesAux[inde] = null;
                coursesAux[inde] = null;
                gradesAux[inde] = null;
                cursandoAux[inde] = null;
            }
        }
    }
    //console.log(coursesAux, gradesAux);
    //indico que el dates es igual a datex aux
    dates = [];
    dates = datesAux;
    courses = [];
    grades = [];
    cursando = [];
    courses = coursesAux;
    grades = gradesAux;
    cursando = cursandoAux;
    if(dates.length < 12){
        let from = dates.length;
        for(let indMissing = from; indMissing < 12; indMissing++){
            if(dates[dates.length - 1] === "Sept"){
                dates.push("Mar");
            }else{
                dates.push("Sept");
            }
            courses.push(null);
            grades.push(null);
            cursando.push(null);
        }
    }
    // // courses[3] = [16, 17];
    // // grades[3] = [80, 70];
    // courses[4] = [16, 17];
    // grades[4] = [80, 70];
    // courses[5] = [18, 19];
    // grades[5] = [80, 50];
    //puedo agregar anios al array si inicia con null entonces son 5 anios en total a mostrar, si inicia sin null entonces son 6 anios en total a msotrar
    let iniciaConNulo = false;
    let years = [];
    //console.log(mapYears);
    // mapYears.set(2008, null);
    // mapYears.set(2009, 2009);
    let index = 0;
    mapYears.forEach(function(value, key, map){
      //console.log(key+" "+value);
      if(value === null){
         // console.log("entrap", key)
          if(index == 0){
            iniciaConNulo = true;
          }else{//si esq es nulo en una posicion q no sea cero, es decir, si no tomo un ciclo entonces ese anio tambn cuenta al momento de brincarse en la grafica no se brinca el anio
              years.push((years[years.length-1])+1);
          }
        
        //years.push(2006);
      }else{
          //console.log(value);
        years.push(value);
        //iniciaConNulo= false;
      }
      index++;
    });
    //console.log(years);
    ///si se salto ciclos debo llenar los anios faltantes
   // years.push(2010);
    let yearsValida = [];
    let ind = 0;
    for( i in years){
        //si es mayor a la primera posicion
      if(i > 0){
        if(years[i] > (years[i-1]+1)){//si el anio actual del for es mayor al anio anterior mas 1 (2009 > 2007 + 1//sinifica que falta el 2008)
          for(let j = (years[i-1]+1); j <= years[i]; j++){//recorro los anios q faltan y los agreog en el array auxiliar (menor o igual para que se agregue el anio de la pos i del array tambn)
            yearsValida.push(j);
          }
        }else{//si no es mayor agrego el de la pos i, osea el actual del recorrido del array
          yearsValida.push(years[i]);
        }
      }else{///si es igual a cero agrego el de la pos cero
        yearsValida.push(years[i]);
      }
    }
   // console.log(yearsValida);
    years = [];//reseteo el array de anios
    years = yearsValida;//indico que el array de anios es igual al arrya validado
    //console.log(years);
    //console.log(years);
    if(iniciaConNulo === true){//si inicia con nulo, significa que son 5 los anios a mostrarse
      //console.log("entrap");
      if(years.length < 5){
        for(var i = years.length; years.length < 5; i++ ){
          years[i] = years[i-1] + 1;
        }
      }
    }else{//si no inicia con nulo, significa que son 6 los anios a mostrarse
      if(years.length < 6){
        for(var i = years.length; years.length < 6; i++ ){
          years[i] = years[i-1] + 1;
        }
      }
    }
    //console.log(years);


    /** Fin proceso de fecha*/

    // courses.push(["19","20","21","22","23"]);
    // courses.push(["24","25","26","27","28"]);
    // // courses.push(["29","30","31","32","33"]);
    // courses.push(null);

    // grades.push([80,90,12,60,40]);
    // grades.push([80,90,12,60,40]);
    // // grades.push([80,90,12,60,40]);
    // grades.push(null);
    
    // // courses.push(["34","35","36","37","38","39"]);
    // courses.push(null);
    // courses.push(null);
    // // courses.push(["40","41","42","43","44"]);
    // courses.push(["45","46","47","48","49"]);

    // //grades.push([80,90,12,60,40]);
    // grades.push(null);
    // grades.push(null);
    // // grades.push([80,90,12,60,40]);
    // grades.push([80,90,12,60,40]);
    
    // courses.push(["50","51","52","53"]);

    // grades.push([80,90,12,60]);
   // console.log(courses);
    const spaceBetweenYears = 20;
    const spaceBetweenTerms = 5;
    const rectSize =20;
    
    //dibujo la lineas del eje x
    ///12 porque son 12 semestres maximo * el numero de rectangulos, osea pro el numero de asignaturas
    ///6 porque son 6 espacios de temrinos (semestre)
    ////4 porque son 6 anios pero en el eje el inicio y el fin no tienen espacio por eso 4, sino seria 6
    let lineaXWidh = 15 * rectSize + (6 * spaceBetweenTerms) + (4 * spaceBetweenYears);
    
    const margin = {top: 15, bottom: 10, right: 15, left: 10};
    const width = lineaXWidh + 20 - margin.right - margin.left;
    const height = 225 - margin.top - margin.bottom;
    

    let svg = d3.select(".chartRect")
        .append("svg")
        .attr('class','svgRectChart')
        // .attr("width", width + margin.right + margin.left)
        // .attr('height', height + margin.top + margin.bottom);
        .attr("preserveAspectRatio", "xMinYMin meet")
          //.attr("viewBox", "0 0 "+(width + margin.left + margin.right)+" "+(height + margin.top + margin.bottom))
          .attr("viewBox", "0 0 540 225")
          //.append('g').attr('class', 'drag');

    let svg2 = svg.append('g').attr('class', 'drag');
        //.attr("viewBox", "0 0 " + (width + margin.right + margin.left) + " " + (height + margin.top + margin.bottom));;
        
    let lineRect  = svg.append("g")
        .attr('transform', `translate(${margin.left}, ${height-25})`);
        lineRect.append('line')
        .attr('x1', 0)
        .attr('y1', 0)
        .attr('x2', lineaXWidh)
        .attr('y2', 0)
        .style('stroke', 'black');
    lineRect.append('rect')//esta recta lo que hace es hacer que cuando se hace drag no se sobremonte en los ejes de meses y anios los cuadros
        .attr('x', 0)
        .attr('y', 0)
        .attr('class','rect-font')
        .attr('height', '50%')
        .attr('width', '100%')
        //.attr('fill', 'white');
        .style('fill', 'oldlace');

    //variable para las fechas en y, es fija
    ///20 porq ese es el espacio desde el pie
    const yDates = height - 15;
    let fechaXAxis = svg.append("g")
        .attr('class', 'axis axis--x font')
        .style('font-size', '10px')
        .attr('transform', `translate(${margin.left}, ${yDates})`)
    //obtengo los cursos repetidos
    //console.log("Cursos repetidos: ");
    //console.log(courses);
    let repetidoss = nRetakeCourses(courses);
    let repetidos = repetidoss[0];
    //recorro cad aciclo
    //let x = ((spaceBetweenTerms + rectSize) * i) + margin.left; 
    let x = margin.left;
    //console.log(fechaIds.length, courses.length);
    let indexFechIds = 0;//index para recorrer las fechas ids, ya que cuando es un curso nulo, no debe sumar el index
    for(var i = 0 ;i < courses.length; i++){
        //recorro cada asignatura
        //primero pregunto si no es nulo, ya que si es nulo no se debe graficar
        if(courses[i] !== null){
        //    console.log("pos "+i);
            let term = courses[i];
            //cursos repetidos en este periodo
            let crep = repetidos[i];
            //notas por periodo
            let notas = grades[i];
            let cursandoState = cursando[i];
            //defino el alto maximo de ese semestre tomando como base al numero de asignaturas
            //tamano del rectangulo * numero deasignaturas + (numero de asignaturas * espacio entre asignaturas) + 50
            //los 50 son el espacio que va a dejar al pie para la grafica de las fechas y anios  
            const alto = rectSize * term.length + (term.length * spaceBetweenTerms) + 40;
            //el orignal es svg.append
            let terms = svg2.append("g")
                .attr("transform", `translate(${margin.left}, ${margin.top})`);
            //let x = ((spaceBetweenTerms + rectSize) * i) + margin.left;     
            let y = height - alto;
            //if((i%2) == 0){
            if(dates[i] === "Mar"){
               // console.log(i);
                if(i!=0){
                    //console.log(i);
                    x = x + rectSize + spaceBetweenYears;
                }
                
            }else if(dates[i] == "Sept"){
                if(i === 0){
                    fechaXAxis.append('text')
                        .attr('x', x)
                        .attr('y', 0)
                        .style("font-weight", "normal")
                        .text("Mar");
                }
                x = x + rectSize + spaceBetweenTerms;
            }
            
            for(var j = 0; j < term.length; j++){
                //aqui debo graficar las rectas de arriba hacia abajo
                //console.log("este es el term: "+term[j]);
                let id = term [j];
                
                let nota = notas [j];
                let state = cursandoState[j];
                //console.log(id);
                terms.append('rect')
                    .attr('x', x)
                    .attr('y', y)
                    .attr('width', rectSize)
                    .attr('height', rectSize)
                    .attr('class', 'rect-course-'+id+' rect-'+fechaIds[indexFechIds].getTime())
                    //.attr('id', 'rect-'+fechaIds[indexFechIds].getTime())
                    .style("fill", function() {
                        if(state === true){
                            return 'orange';
                        }else
                        if(notas[j] < 60 ){
                            return 'lightcoral';
                        }else{
                            return 'lightgreen';
                        }
                    })
                    .style("cursor", "pointer")
                    .on("mouseover", function(){
                        highlightCourse(id, nota, true, state);//id del curso, nota del curso, mouseover
                    })
                    .on("mouseout", function (){
                        highlightCourse(id, nota, false, state);
                    });
                    //grafico donde se va a dibujar, coordenada x, cooordenada y, tamano del cuadrado, valor a dibujar, id de asignatura, nota, si es de workload o no, mostrarlo o no
                addText(terms, x, y, rectSize, crep[j], id, nota, false, "visible", state);
                addComplexity(terms, x, y, rectSize, complejidadPorCurso, id, nota, false, "hidden", state);
                // terms.append('text')
                //     .attr('x', (x - 4 + rectSize / 2))
                //     .attr('y', (y + rectSize - 3))
                //     .text(crep[j])
                //     .on("mouseover", function(){
                //         highlightCourse(id, nota, true);//id del curso, nota del curso, mouseover
                //     })
                //     .on("mouseout", function (){
                //         highlightCourse(id, nota, false);
                //     });
                    //actualizo y
                    y = y + rectSize + spaceBetweenTerms;

            }
           // console.log("este es x", x);
            ///dibujo las fechas por debajo de la grafica
            fechaXAxis.append('text')
                .attr('x', x)
                .attr('y', 0)
                .style("font-weight", "normal")
                .text(dates[i]);
            indexFechIds++;
            
        }else{//al ser nulo, no se debe graficar, esto significa que debe saltarse ese espacio
            //x = x  + rectSize + 2*spaceBetweenTerms;
            if(dates[i] === "Mar"){
                //console.log(i);
                if(i!=0){
                   // console.log(i);
                    x = x + rectSize + spaceBetweenYears;
                }
                
            }else if(dates[i] == "Sept"){
                x = x + rectSize + spaceBetweenTerms;
            }
            //grafico los meses en las posiciones nulas
            fechaXAxis.append('text')
                .attr('x', x)
                .attr('y', 0)
                .style("font-weight", "normal")
                .text(dates[i]);
        }
    }
    //grafico los anios
    //if(years.length === 5){
        if(years.length > 0){//si es mayor a cero se grafica
            let indexX = 0; ///la posicion inicial donde se va a graficar el primer anio
        let yearAxis = svg.append("g")
            .attr('class', 'anios axis axis--x font')
            .style('font-size', '10')
            .attr('transform', `translate(${margin.left+spaceBetweenYears}, ${yDates+10})`)
        if( iniciaConNulo === true){//si inicia con nulo, significa que se debe graficar un anio anterior al del inicio del array, ya que ese valor es nulo
            let yearPosZero = years[0] - 1;
            yearAxis.append('text')
                .attr('class', 'bold-text')
                .attr('x', 0)
                .attr('y', 5)
                .text(yearPosZero);
            indexX= spaceBetweenYears + spaceBetweenTerms + 2*rectSize;//al graficarse un anio anterior al de la pos cero,e ntonces es necesario aumentar el index para el siguiente anio
        }
        
        for (var i in years){//recorro todos los anios en el array y los grafico
            yearAxis.append('text')
            .attr('class', 'bold-text')
            .attr('x', indexX)
            .attr('y', 5)
            .text(years[i]);
            indexX += spaceBetweenYears + spaceBetweenTerms + 2*rectSize; 
        }
    }
    //dibujo la leyenda
    drawLegend(rectSize);

    ///con este codigo, lo que hago es un evento de dragg para la clase drag que es una etiqueta g en el que esta graficado todas las rectas
    //el fin de este es que cuando hayan mas asignaturas de las que se puede visualizar, se pueda arrastrar y asi ver los cuadros que no se pueden ver
    ///para mostrar bien, se debe poner como negativos a los cuadros que no se van a apreciar, asi solamente se arrastra hacia abajo y listo.
    var dragcontainer = d3.drag()
        .on("drag", function(d, i) {
            //console.log('drag');
            //d3.select(this).attr("transform", "translate(" + (d.x = d3.event.x) + "," + (d.y = d3.event.y) + ")");
            let v = d3.event.y;
            //console.log(v, (height-25-spaceBetweenTerms));
            if(v < 0){//cero porque en el g que se grafica los cuadros ya esta hecho el translate a height - 25(es decir, height-25 es la pos cero)
                v = 0;
            }
            //d3.select(this).attr("transform", "translate(" + 0 + "," + (d.y = d3.event.y) + ")");
            d3.select(this).attr("transform", "translate(" + 0 + "," + (d.y = v) + ")");
        });
    var g = d3.select(".drag").datum({x: 0, y: 0}).call(dragcontainer);
    //console.log(g);
}
//cuento cuantas veces a repetido la misma asignatura y guardo en un arrya para poder mostrar
export function nRetakeCourses(coursesId) {
    let cursos = new Map();
    let vecesRepetidas = [];
    for (var i = 0; i < coursesId.length; i++){
        let coursesNMatricula = [];
        let ids = coursesId[i];
        //pregunto si no es nuo, si es nulo, significa que no ha cursado ese semestre, por lo cual no se debe graficar
        if(ids!==null){
        for (var j = 0; j < ids.length; j++){
            //si el mapa ya posee la llave, mando a sumar el valor y agrego en el array
            if(cursos.has(ids[j])){
                cursos.set(ids[j], cursos.get(ids[j])+1);//sumo el valor que ya poseeia
                coursesNMatricula.push(cursos.get(ids[j]));//agrego al array el valor que acabo de sumar
            }else{
                cursos.set(ids[j], 1);
                coursesNMatricula.push(1);
            }
        }
        vecesRepetidas[i] = coursesNMatricula;//agrego las veces repetidas en cada semestre
    }
    }
    //console.log(vecesRepetidas);
    return [vecesRepetidas, cursos];
}
//esta funcion sobresalta a la asignatura que corresponde al pasar el mouse sobre un rect
function highlightCourse(courseId, grade, typemouse, state){
    let color = "green";

    if(state === true){
        color = "orange";
    }else
    if(grade < 60 ){
        color = "coral";
    }

    let id = "#asignatura-"+courseId;

    let border = "1px black solid";

    if(typemouse == true){
        border = "5px "+color+" solid";
    }
    d3.select(id)
        .style("border", border);
}

export function addText(terms, x , y , rectSize, text, id, nota, isFromAutentication, visibility, state){
    terms.append("g")
        .attr("class", function (){
            if(isFromAutentication === true){
                return "nRetake added";
            }else{
                if(nota < 60){
                    return "nRetake rep rep-"+id;//para las asignaturas que va a repetir, cuando selecciono carga de trabajo deben aparecer seleccionadas si el usuari selecciona el cuadro grande la la materia, no se debe ocultar al quitar el mouse. en el archivo utentication quito los eventos del mouse 
                }else
                    return "nRetake";
            }
        })
        .attr('visibility', visibility)
        .append('text')
                .attr('x', (x - 4 + rectSize / 2))
                .attr('y', (y + rectSize - 3))
                .text(text)
                .style("cursor", "pointer")
                .on("mouseover", function(){
                    if(isFromAutentication !== true)
                        highlightCourse(id, nota, true, state);//id del curso, nota del curso, mouseover
                    else
                        d3.select("#asignatura-"+id)
                        .style("box-shadow","2px 2px 2px");
                })
                .on("mouseout", function (){
                    if(isFromAutentication !== true)
                        highlightCourse(id, nota, false, state);
                    else
                        d3.select("#asignatura-"+id)
                        .style("box-shadow","");
                });
}

export function addComplexity(terms, x, y, rectSize, complejidadPorCurso, id, nota, isFromAutentication, visibility, state){//el isfromautentication es true cuando se le llama desde autentication a la funcion
    let complexity = 0;
    if(isFromAutentication === true){
        complexity = complejidadPorCurso;
    }else{
        complexity = complejidadPorCurso.get(id);
    }
    
    let radio = 2;
    let grupo = terms.append("g")
        .attr('class', function(){
            if(isFromAutentication === true){
                return 'complexityRect added';
            }else {
                if(nota < 60){//para las asignaturas que va a repetir, cuando selecciono carga de trabajo deben aparecer seleccionadas si el usuari selecciona el cuadro grande la la materia, no se debe ocultar al quitar el mouse
                    return "complexityRect rep rep-"+id;
                }else
                    return 'complexityRect';
            }
        })
        .style('visibility', visibility)
        .style("cursor", "pointer")
        .on("mouseover", function(){
            if(isFromAutentication !==true)
                highlightCourse(id, nota, true, state);//id del curso, nota del curso, mouseover
            else
            d3.select("#asignatura-"+id)
            .style("box-shadow","2px 2px 2px");  
        })
        .on("mouseout", function (){
            if(isFromAutentication !== true)
                highlightCourse(id, nota, false, state);
            else
                d3.select("#asignatura-"+id)
                .style("box-shadow","");
        });
    if(complexity == 25){
        grupo.append('circle')
            .attr('cx', x + rectSize / 2)
            .attr('cy', y + rectSize / 2 )
            .attr('r', radio)
            // .style('fill', 'mintcream');
            .attr("class", "baja");    
    }else if(complexity == 50){
        grupo.append('circle')
            .attr('cx', x + rectSize / 2 - radio - 1 )
            .attr('cy', y + rectSize / 2 )
            .attr('r', radio)
            // .style('fill', 'mintcream'); 
            .attr("class", "baja");
        grupo.append('circle')
            .attr('cx', x + rectSize / 2 + radio + 1 )
            .attr('cy', y + rectSize / 2 )
            .attr('r', radio)
            // .style('fill', 'darkcyan');       
            .attr("class", "media");
    }else if(complexity == 75){
        grupo.append('circle')
            .attr('cx', x + rectSize / 2 - radio * 2  - 2)
            .attr('cy', y + rectSize / 2 )
            .attr('r', radio)
            // .style('fill', 'mintcream');
            .attr("class", "baja"); 
        grupo.append('circle')
            .attr('cx', x + rectSize / 2)
            .attr('cy', y + rectSize / 2 )
            .attr('r', radio)
            // .style('fill', 'darkcyan');       
            .attr("class", "media");
        grupo.append('circle')
            .attr('cx', x + rectSize / 2 + 2 + radio * 2 )
            .attr('cy', y + rectSize / 2 )
            .attr('r', radio)
            // .style('fill', 'black');
            .attr("class", "alta");       
    }
    
}

function drawLegend(rectSize){
    let svgLegend = d3.select(".legendText")
    .append("svg")
    .attr("class", "svgLegendText")
    .attr("preserveAspectRatio", "xMinYMin meet")
    //.attr("viewBox", "0 0 "+(width + margin.left + margin.right)+" "+(height + margin.top + margin.bottom))
    //.attr("viewBox", "0 0 190 225");;
    .attr("viewBox", "0 0 300 160");;
    //para las asignaturas aprobadas
    let legendApproved = svgLegend.append("g")
        .attr("class", "legendApproved")
        .attr("transform", 'translate(0, 20)');
    legendApproved.append('rect')
        .attr('x', 0)
        .attr('y', 30)
        .attr('width', rectSize)
        .attr('height', rectSize)
        .style('fill', 'lightgreen');
    //dibujo el texto dentro del rectangulo
    legendApproved.append('text')
        .attr('x', rectSize / 2 - 5)
        .attr('y', 30 + rectSize - 3)
        .text('#');
    //dibujo el texto (asignatura aprobada en matricula #)
    legendApproved.append('text')
        .attr('x', rectSize + 6)
        .attr('y', 20 + rectSize - 5)
        .text('Asignatura aprobada en matricula #')
        .attr('class', 'textLegendApprobed');
    ///corto el texto para que aparezca en varias lineas el width maximo es 160
    legendApproved.select('.textLegendApprobed')
        .call(wrap, 160);

    ///para las asignaturas reprobadas
    let legendReproved = svgLegend.append('g')
        .attr('class', 'legendReproved')
        .attr('transform', 'translate(0, 80)');

    legendReproved.append('rect')
        .attr('x', 0)
        .attr('y', 10)
        .attr('width', rectSize)
        .attr('height', rectSize)
        .style('fill', 'lightcoral');
    //dibujo el texto dentro del rectangulo
    legendReproved.append('text')
        .attr('x', rectSize / 2 - 5)
        .attr('y', 10 + rectSize - 5)
        .text('#');
    //dibujo el texto (asignatura reprobada en matricula #)
    legendReproved.append('text')
        .attr('x', rectSize + 6)
        .attr('y', rectSize - 3)
        .text('Asignatura reprobada en matricula #')
        .attr('class', 'textLegendRepprobed');
    ///corto el texto para que aparezca en varias lineas el width maximo es 160
    legendReproved.select('.textLegendRepprobed')
        .call(wrap, 160);

    //al level complejidad le corto las palabras para que de salto de linea y no se extienda
    //d3.select('.labelComplejidad').call(wrap, 200);
}

let lineas =1;
function wrap(text, width) {
    text.each(function() {
      var text = d3.select(this),
          words = text.text().split(/\s+/).reverse(),
          word,
          line = [],
          lineNumber = 0,
          lineHeight = 1.1, // ems
          y = text.attr("y"),
          x = text.attr("x"),
          dy = parseFloat(text.attr("y")),
          tspan = text.text(null).append("tspan").attr("x", x).attr("y", y);
      while (word = words.pop()) {
        line.push(word);
        tspan.text(line.join(" "));
        lineas ++;
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(" "));
          line = [word];
          //console.log(lineNumber+"     "+lineHeight+ "  dy    "+dy);
          tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", (++lineNumber * lineHeight ) + "em").text(word);
        }
      }
    });
  }
 /* 
d3.select(window).on("resize", function() {
    var chart = d3.select(".svgRectChart"),
 // aspect = chart.attr('width') / chart.attr('height');
    aspect = 370 / 225;
  //container = chart.node();
  let container = d3.select(".chartRect");
  let c = document.getElementsByClassName("chartRect")[0];
  
//   let c = chart.select(function(){
//       return this.parentNode
//     });
    //console.log(container.node().getBoundingClientRect().width+"       ");
    //console.log(c.offsetWidth);
    var targetWidth = c.offsetWidth*0.75;//attr('width');
    var targetheight = c.offsetHeight;
    //console.log(targetWidth + "  "+aspect);
    // chart.attr("width", targetWidth);
    // chart.attr("height", Math.round(targetWidth / aspect));
    // //chart.attr("width", targetWidth);
    // //chart.attr("height", targetheight);
    // chart.attr("viewBox", "0 0 "+targetWidth+" "+targetheight);

    //para la grafica de linea
    chart = d3.select("#chartLineComparative");
 // aspect = chart.attr('width') / chart.attr('height');
  //  aspect = 610 / 265;
  //container = chart.node();
    container = d3.select(".chartLine");
    let cc = document.getElementsByClassName("chartLine")[0];
  
//   let c = chart.select(function(){
//       return this.parentNode
//     });
    //console.log(container.node().getBoundingClientRect().width+"       ");
    //console.log(c.offsetWidth);
    //let ttargetWidth = cc.offsetWidth*0.75;//attr('width');

    //targetWidth = 610;
    //let targetHeight = cc.offsetHeight;

    //let targetHeight = 265;
    //console.log(targetWidth + "  "+aspect);
    //let height = Math.round(ttargetWidth / aspect);
    //console.log(height + "  " +targetHeight);
    // if(height  < targetHeight){
    //   console.log("Entraaaaaaaaaaa");
    //   height = targetHeight;
    // }
 
 //////////////////////////////////////IMPORTANTEEEEE///////////////////////////////
 //AL DEFINIR EL VIEWBOX Y EL ASPECT RATIO EN EL MOMENTO DE CREAR EL SVG
 //NO HACE FALTA HACER NADA AQUI EN EL RESIZE, PORQUE AUTOMATICAMENTE YA SE 
 ///HARA RESPONSIVO SIN NECESIDAD DE HACER ESTO
 //HAGO ESO PARA QUE EL RESOPNSIVO NO S EHAGA MUY PEQUENA, PERO NO HACE FALTA
//  aspect = 540 / 225;
//     chart.attr("width", targetWidth);
//     chart.attr("height", Math.round(targetWidth / aspect));
//    //chart.attr("height", targetHeight);
//     chart.attr("viewBox", "0 0 "+targetWidth+" " +targetheight);
});*/
