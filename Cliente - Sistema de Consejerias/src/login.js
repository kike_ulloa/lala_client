import React, { Component } from 'react';
import ReactDOM from "react-dom";

import "./css/login.css";
import App from './App';


class Login extends Component{
    constructor(){
        super();
        this.state = {
            user: "",
            password: "",
            redirect: false
        };
    }
    onHandleChange = (e) => {
        
        this.setState({user: e.target.value});
        
    }
    onHandleChangePassword = (e) => {
        this.setState({password: e.target.value});
    }
    onSubmit(e){
        e.preventDefault();
        let user = this.state.user;
        let password = this.state.password;
        //alert(user+" "+ password);
        //this.setState({redirect: true});
        if(user==="admin@ucuenca.edu.ec" && password === "admin"){
            ReactDOM.render(<App />, document.getElementById('root'));
        }else if(user==="LALA_Ucuenca" && password === "lalaucuenca"){
            ReactDOM.render(<App />, document.getElementById('root'));
        }else{
            alert("Correo o contraseña incorrecta!");
        }
        
    }

    render(){
        return(
            
            <div className="container centroLogin">

                <div className="user-container bothCenter">
                    <label className="label-user">Correo:</label>
                    <input type="text" className="form-user form-control size"  onChange={e => this.onHandleChange(e)} name="user" placeholder="correo@ucuenca.edu.ec"/>
                </div>
                <div className="password-container bothCenter">
                    <label className="label-password">Contraseña:</label>
                    <input type="password" className="form-password form-control size"  onChange={e => this.onHandleChangePassword(e)} name="password" placeholder="contraseña"/>
                </div>
                <button type="button" onClick={e => this.onSubmit(e)} className="btn btn-primary btn-primary-login">Login</button>
                
            </div>
        );
    }
}
export default Login;