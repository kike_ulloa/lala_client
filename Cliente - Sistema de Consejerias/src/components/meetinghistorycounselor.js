import React from 'react';
import MUIDataTable from 'mui-datatables';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
const request = require('request-promise');

global.tamano=0;
let ENDPOINTSESSIONSHISTORY = 'http://localhost:3000/api/v1/getStudentHistorySessions/?studentid=';

// const columns = [{name: "id", options:{display: 'false'}},"Name", "Title", "Location", "Age", "Salary"];
const columns = [{name: "id", options:{display: 'false'}},{name:"Observaciones", width:'100px'}, "Consejero", "Fecha"];
    
        /*const data = [
          [1,"Gabby George", "Business Analyst", "Minneapolis", 30, "$100,000"],
          [2,"Aiden Lloyd", "Business Consultant", "Dallas", 55, "$200,000"],
          [3,"Jaden Collins", "Attorney", "Santa Ana", 27, "$500,000"],
          [4,"Franky Rees", "Business Analyst", "St. Petersburg", 22, "$50,000"],
          [5,"Aaren Rose", "Business Consultant", "Toledo", 28, "$75,000"],
          [6,
            "Blake Duncan",
            "Business Management Analyst",
            "San Diego",
            65,
            "$94,000"
          ],
          [7,"Frankie Parry", "Agency Legal Counsel", "Jacksonville", 71, "$210,000"],
          [8,"Lane Wilson", "Commercial Specialist", "Omaha", 19, "$65,000"],
          [9,"Robin Duncan", "Business Analyst", "Los Angeles", 20, "$77,000"],
          [10,"Mel Brooks", "Business Consultant", "Oklahoma City", 37, "$135,000"],
          [11,"Harper White", "Attorney", "Pittsburgh", 52, "$420,000"],
          [12,"Kris Humphrey", "Agency Legal Counsel", "Laredo", 30, "$150,000"],
          [13,"Frankie Long", "Industrial Analyst", "Austin", 31, "$170,000"],
          [14,"Brynn Robbins", "Business Analyst", "Norfolk", 22, "$90,000"],
          [15,"Justice Mann", "Business Consultant", "Chicago", 24, "$133,000"],
          [16,
            "Addison Navarro",
            "Business Management Analyst",
            "New York",
            50,
            "$295,000"
          ],
          [17,"Jesse Welch", "Agency Legal Counsel", "Seattle", 28, "$200,000"],
          [18,"Eli Mejia", "Commercial Specialist", "Long Beach", 65, "$400,000"],
          [19,"Gene Leblanc", "Industrial Analyst", "Hartford", 34, "$110,000"],
          [20,"Danny Leon", "Computer Scientist", "Newark", 60, "$220,000"],
          [21,"Lane Lee", "Corporate Counselor", "Cincinnati", 52, "$180,000"],
          [22,"Jesse Hall", "Business Analyst", "Baltimore", 44, "$99,000"],
          [23,"Danni Hudson", "Agency Legal Counsel", "Tampa", 37, "$90,000"],
          [24,"Terry Macdonald", "Commercial Specialist", "Miami", 39, "$140,000"],
          [25,"Justice Mccarthy", "Attorney", "Tucson", 26, "$330,000"],
          [26,"Silver Carey", "Computer Scientist", "Memphis", 47, "$250,000"],
          [27,"Franky Miles", "Industrial Analyst", "Buffalo", 49, "$190,000"],
          [28,"Glen Nixon", "Corporate Counselor", "Arlington", 44, "$80,000"],
          [29,
            "Gabby Strickland",
            "Business Process Consultant",
            "Scottsdale",
            26,
            "$45,000"
          ],
          [30,"Mason Ray", "Computer Scientist", "San Francisco", 39, "$142,000"]
        ];*/
        let options={};
        let numero = 0;
        let filasSeleccionadas = new Map();
class List extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data: []
        }
        this.getSessionsHistory();
         options = {
            filterType: "dropdown",
            responsive: "scroll",
            pagination: false,
            onRowClick: e=>this.rowClick(e),
            onRowsDelete: e=>this.deleted(e),
            onRowsSelect: e=>this.selectedRows(e),
            sort: false, ///con true se puede ordena rpor columan al hacer clic en cad aoclumna sea ascendente o decendente
            search: false,
            print: false,
            download: false,
            viewColumns: false
          };
          numero = 0;
          filasSeleccionadas = new Map();
         // console.log("entraaaaaaa", this.props.student_id);
    }
    async getSessionsHistory(){
        ENDPOINTSESSIONSHISTORY+=this.props.student_id;
        let history = await request({
            uri: ENDPOINTSESSIONSHISTORY,
            json: true,
            rejectUnauthorized: false,
        });
        //data = history;
        //console.log(history, ENDPOINTSESSIONSHISTORY);

        let data = [];
        for(let i in history){
            let date = new Date(Date.parse(history[i].date));
            let d = String(date.getDate()).padStart(2, '0')+"/"+String(date.getMonth()+1).padStart(2, '0')+"/"+date.getFullYear();
            data[i] = [history[i].id, history[i].observations, history[i].counselor.name, d];
        }
        //data.push([2, 'esta es una nueva observaicon', 'COnsejero', '23/02/2017']);
         //console.log(data);
        // console.log("entraaa");
        global.tamano=data.length;
        ENDPOINTSESSIONSHISTORY = 'http://localhost:3000/api/v1/getStudentHistorySessions/?studentid=';
        this.setState({data: data});
    }
    rowClick(e){
        //console.log(e);
        let vari =document.getElementsByClassName("table-data")[0];
        let v = vari.getElementsByTagName("input")[this.state.data.length];
        v.checked = true;
        //console.log(vari, v);
        // document.getElementById("MUIDataTableBodyRow-"+(e[0]-1)).style.background="green";
      }
      deleted(e){

          console.log(e);
          //alert(e);
      }
      ///cuando selecciona alguna fila o varias
      selectedRows(e){
        
        let h6=document.getElementsByClassName("MuiTypography-subtitle1-64")[0];//obtengo el tag h6 que aparece cuando selecciona alguna o varias filas
       // console.log("entraaaa", h6, e);
        if( e !== undefined){
          for(let i in e){//el e es un vector que contiene la fila seleccionada o, en caso de seleccionar todas desde la columna, contendra todas las seleciconadas
            if(filasSeleccionadas.has(e[i].dataIndex)){///si el mapa ya tiene a la fila seleccionado, significa que esta deseleccionando
              filasSeleccionadas.delete(e[i].dataIndex);//elimino la fila que anteriormente estaba seleciconada
            }else{//en caso de que el mapa no tenga la fila seleccionada..la agrego, ya que esta seleccionando pro primera vez
              filasSeleccionadas.set(e[i].dataIndex, e[i].dataIndex);
            }
          }
        }else{
          filasSeleccionadas.clear();
        }
        if(h6 !== undefined && h6 !== null){//cuando se quita al seleccion de todas las filas retorna undefined
          // if(filasSeleccionadas.length === 1)
          //  /// numero = parseInt(h6.innerHTML.replace(/[^\d]/g, ''));//obtengo el numero de filas seleccionadas (1 row(s) selected) solo obtengo el numero
          // else numero ++;
          let appear = "";
          if(filasSeleccionadas.size > 1){
            appear = filasSeleccionadas.size + " filas seleccionadas";
          }else if(filasSeleccionadas.size === 1){
            appear = filasSeleccionadas.size + " fila seleccionada";
          }
          let seleccionadas = appear;
          //console.log(seleccionadas);
          h6.innerHTML = seleccionadas;
        }else{
          filasSeleccionadas = new Map();
        }
        
      }
      getMuiTheme = () => createMuiTheme({
        overrides: {
          MUIDataTableBodyCell: {
            root: {
                '&:nth-child(2)': {
                    width: '200px'
                  }
              //backgroundColor: "green"
              //width: "100px"
            }
          }
        }
      });

      componentWillReceiveProps(){
        if(global.tamano > this.state.data.length){
          this.getSessionsHistory();
          //console.log("agrega data");
        }
      }
      render() {
        
        
        
        
        return (
             //<MuiThemeProvider theme={this.getMuiTheme()}>
                <MUIDataTable
                    title={"Historial de Sesiones"}
                    data={this.state.data}
                    columns={columns}
                    options={options}
                />
             //</MuiThemeProvider>
        );
      }
}

export default List;