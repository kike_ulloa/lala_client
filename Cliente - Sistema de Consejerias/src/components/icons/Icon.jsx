import React from 'react';
const {PropTypes} = React;

const Icon = props => {
  const styles = {
    svg: {
      display: 'inline-block',
      verticalAlign: 'middle',
    },
    path: {
      fill: props.color,
    },
  };

  return (
    <svg
      style={styles.svg}
      width={`${props.size}%`}
      height={`${props.size}%`}
      viewBox={`0 0 ${props.viewBoxX} ${props.viewBoxY}`}
    >
      <path
        style={styles.path}
        d={props.icon}
      ></path>
    </svg>
  );
};

// Icon.propTypes = {
//   icon: PropTypes.string.isRequired,
//   size: PropTypes.number,
//   color: PropTypes.string,
// };

Icon.defaultProps = {
  size: 100,
  viewBoxX: 24,
  viewBoxY: 24
};

export default Icon;