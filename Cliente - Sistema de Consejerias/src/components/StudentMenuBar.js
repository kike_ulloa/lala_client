import React from 'react';
/**Importo los iconos */
import Icon from './icons/Icon';
import {ICONS} from '../icons/icons';

/**Importo el popUp para mostrar la informacion del estudiante */
import PopUpStudentInfo from './PopUps/PopUpStudentInfo';

// Importo el PopUp para mostrar la configuración
import Configuration from './PopUps/PopUpConfiguration';

//importo el popup para llenar la sobservaciones al final de la sesion
import Observations from './PopUps/SessionObservations';
//Dos formas de crear variables globales que se pueden acceder y modificar desde cualquier archivo js.
//necesito para cuando abra la ventana configuracion y haga un check, ese check se quede guardado a pesar de que cierre esa ventana
global.checkGlobal = false;
// window.checked = false;
global.showComplexityText = "Mostrar Complejidad";

class StudentMenuBar extends React.Component{
    constructor(){
        super();
        this.state = {
            style: {},
            showPopUpStudentInfo: false,
            showPopUpCOnfiguration: false,
            showObservations: false
        }
    }

    muestraclick(e){
        e.preventDefault();
        var rect = e.target.getBoundingClientRect();
       // console.log(this.props.email);
       // alert(rect.top+" "+rect.left+" "+e.target.offsetWidth+" "+e.screenX + " "+e.screenY);
        this.setState({style: {top: 60, left: e.screenX}});
        this.setState({showPopUpStudentInfo: true});
    }

    closePopUpStudentInfo(){
        this.setState({showPopUpStudentInfo:false});
    }
    //funcion para mostrar el popup configuración
    showCOnfiguration(e){
        e.preventDefault();
        this.setState({showPopUpCOnfiguration: true});
    }
    //funcion que cierra el popup configuración
    hideCOnfiguration(e){
        
        this.setState({showPopUpCOnfiguration: false});
    }
    //funcion que abre el popup observaciones
    showPopUpObservations(e){
        e.preventDefault();
        this.setState({showObservations: true});
    }
    //funcion que cierra el popup observaciones
    hidePopUpObservations(e){
        e.preventDefault();
        this.setState({showObservations: false});
    }
    render () {
        return(
        <div className="contenedor">
            <div className="menuStudent">
                <label for="btnface" className="labelButtonStudentIcon"><Icon icon={ICONS.face} color='mediumslateblue'></Icon></label>
                <button id="btnface" className="buttonStudentIcon" onClick={this.muestraclick.bind(this)}></button>
                <label for="btnInforIcon" className="labelButtonStudentIcon iconHistory"><Icon  className="iconHistoy" icon={ICONS.message} color='mediumslateblue'></Icon></label>
                <button id="btnInforIcon" className="buttonStudentMessageIcon" onClick={this.showPopUpObservations.bind(this)}></button>
                <h3 className="name">{this.props.nombre}</h3>
            </div>
          <h3 className="titleCurriculum">Carrera de {this.props.carrera}</h3>
          <div className="inforStudent">
                <label for="btnSettingsIcon" className="labelButtonStudentIcon"><Icon icon={ICONS.settings} color='mediumslateblue'></Icon></label>
                <button id="btnSettingsIcon" className="buttonStudentSettingsIcon" onClick={this.showCOnfiguration.bind(this)}></button>
          </div>
          {this.state.showPopUpStudentInfo ? 
                <PopUpStudentInfo style={this.state.style} estudiante={this.props.nombre}
                email={this.props.email}
                closePopUpStudentInfo={this.closePopUpStudentInfo.bind(this)}
                carrera={this.props.carrera} promediosStudent={this.props.promediosStudent}
                totalAsignaturas={this.props.totalAsignaturas}
                totalAsignaturasAprobadas={this.props.totalAsignaturasAprobadas}
                totalAsignaturasCursadasPerdidas={this.props.totalAsignaturasCursadasPerdidas}
                ></PopUpStudentInfo>
                : null
          }
            {this.state.showObservations ? 
                <Observations closePopUpObservations={this.hidePopUpObservations.bind(this)} student={this.props.nombre} counselor={"Consejero"} student_id={this.props.student_id} /> : null
            }
          {this.state.showPopUpCOnfiguration ?
                <Configuration closePopUpConfiguration={this.hideCOnfiguration.bind(this)}/> : null
            }
        </div>
        );
    }
}
export default StudentMenuBar;