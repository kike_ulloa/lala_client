import React from 'react';
import PopUpNotasComparadasGrow from '../charts/BarChartPopUpNotasComparadasCompaneros';

class PopUpAdignaturaGrow extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            style: {
                top: 0,
                left: 0
            }
        }
        
        
        
    }
    componentDidMount(){

        this.setState({style: {top:0, left:0}});
        //console.log(this.state.style+"  "+document.getElementsByClassName("popup_inner_grow")[0]);
        var popup = document.getElementsByClassName('popup_inner_grow')[0];
        var data=popup.getBoundingClientRect();
        
        let position = this.props.popUpPos;
        //console.log("Este es el famoso top "+data.top+" "+popup.offsetHeight+" "+position.centroX);
        if(position.centroX!=undefined){
            //console.log("holap");
            //encuentro el centro del popup
            var mitadX = popup.offsetWidth / 2;//el ancho del popup para dos
            var mitadY = popup.offsetHeight / 2; //el alto del popup para dos

            //para el aldo izquierdo 
            var posX = position.centroX - mitadX; //resto..el centro del div de la materia menos la mitad del ancho del popup, me da la posicion en x en la que se debe ubicar
            //para el lado de arriba (top)
            var posY = position.centroY - mitadY; //resto..el centro del div de la materia menos la mitad del largo del popup, me da la posicion en y en la que se debe ubicar
            //para el aldo derecho
            var posXr = position.centroX + mitadX;
            //para el lado de abajo(bottom)
            var posYb = position.centroY + mitadY;
            
            //ahora comparo que no se salga del div el popup, para ello comparo los lados izquierdo, arriba, derecho y abajo que no se exeda
            if(posX<position.MinimoX){
                posX = position.MinimoX; //si es menor al minimo en x, entonces toma el valor del minimo
            }
            if(posXr > position.MaximoX){
                posX = position.MaximoX - popup.offsetWidth; //si es mayor al maximo en x, entonces toma el valor del maximo menos el ancho del componente popup
            }
            if(posY < position.MinimoY){
                posY = position.MinimoY; //si es menor al minimo en y, entonces toma el valor del minimo
            }
            if(posYb > position.MaximoY){
                posY = position.MaximoY - popup.offsetHeight; //si es mayor al maximo en y, entonces toma el valor del maximo menos el largo del componente popup
            }
            //console.log(position.centroY+" centroX: "+position.centroX+" mitadY: "+mitadY+" mitadX: "+mitadX+" posXr: "+posXr+" posyb: "+posYb);
            this.update(posX, posY);
            
        }
    }
    update(posX, posY){
        //console.log("Estas son las coordenadas: x: "+posX+"  y: "+posY);
        if(this.state.style.top !== posY){
            this.setState({style: {top: posY, left: posX}});
        }
    }
    
    render(){
        return (
            <div className='popup'>
                <div className='popup_inner_grow' style={this.state.style}>
                <span className="close glyphicon glyphicon-remove close-alert" onClick={this.props.closePopUpGrow}></span>
                <div className="popUpGrowTitulo"><h6>NOTA DEL ALUMNO EN ÚLTIMA MATRÍCULA comparado con notas compañeros de aula </h6></div>
                <div className="secciones">
                    <div className="grafica">
                        <div className="descripcion">
                            <h5>{this.props.asignatura}; {this.props.nStudentsGroup-1} compañeros de aula. </h5>
                        </div>
                        <div className="graficaCompleta">
                            <PopUpNotasComparadasGrow data={this.props.datagraph} size={[100,100]} position={this.props.position} notaF={this.props.notaF}/>
                        </div>
                    </div>
                    <div className="detalle">
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Aprovechamiento 1: </strong>{this.props.aprovechamientoOne}</td>
                                </tr>
                                <tr>
                                    <td><strong>Interciclo: </strong>{this.props.interciclo}</td>
                                </tr>
                                <tr>
                                    <td><strong>Aprovechamiento 2: </strong>{this.props.aprovechamientoTwo}</td>
                                </tr>
                                <tr>
                                    <td><strong>Final: </strong>{this.props.final}</td>
                                </tr>
                                <tr>
                                    <td><strong>Suspensión: </strong>{this.props.suspension}</td>
                                </tr>
                                <tr>
                                    <td><strong>NOTA FINAL: </strong>{this.props.notaF}</td>
                                </tr>
                                <tr>
                                    <td><br/></td>
                                </tr>
                                <tr>
                                    <td><strong>Faltas: </strong>{this.props.faltas}</td>
                                </tr>
                                <tr>
                                    <td><strong>Forma De Aprobación: </strong>{this.props.formaAprovacion}</td>
                                </tr>
                                <tr>
                                    <td><br/></td>
                                </tr>
                                <tr>
                                    <td><strong>Horas Teóricas: </strong>{this.props.hteoricas}</td>
                                </tr>
                                <tr>
                                    <td><strong>Horas Prácticas: </strong>{this.props.hpracticas}</td>
                                </tr>
                                <tr>
                                    <td><strong>Horas T. Autónomo: </strong>{this.props.htrabajoautonomo}</td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
               {/* <button onClick={this.props.closePopUpGrow}>close me</button>*/}
                
                </div>
            </div>
        );
    }
}
export default PopUpAdignaturaGrow;