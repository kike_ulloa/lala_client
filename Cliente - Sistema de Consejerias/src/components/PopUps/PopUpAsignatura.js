import React from 'react';
import BarChartPopUp from '../charts/BarChartPopUP1';
import * as d3 from 'd3';
import {draw} from '../charts/BarChartPopUpAsignatura';
class PopUpAsignatura extends React.Component {
    constructor(props){
        super(props);
        this.state  = {
            style: {
                top: 0,
                left: 0
            }
        }
        
        
    }
   
    componentDidUpdate(){
        var popup = document.getElementsByClassName('popup_inner')[0];
        console.log(popup+ "   "+document.getElementsByClassName('popup_inner').length);
        if(popup!=undefined){
        var data=popup.getBoundingClientRect();
        
        let position = this.props.popUpPos;
        console.log("Este es el famoso top "+data.top+" "+popup.offsetHeight+" "+position.centroX);
        if(position.centroX!=undefined){
            //console.log("holap");
            //encuentro el centro del popup
            var mitadX = popup.offsetWidth / 2;//el ancho del popup para dos
            var mitadY = popup.offsetHeight / 2; //el alto del popup para dos

            //para el aldo izquierdo 
            var posX = position.centroX - mitadX; //resto..el centro del div de la materia menos la mitad del ancho del popup, me da la posicion en x en la que se debe ubicar
            //para el lado de arriba (top)
            var posY = position.centroY - mitadY; //resto..el centro del div de la materia menos la mitad del largo del popup, me da la posicion en y en la que se debe ubicar
            //para el aldo derecho
            var posXr = position.centroX + mitadX;
            //para el lado de abajo(bottom)
            var posYb = position.centroY + mitadY;
            
            //ahora comparo que no se salga del div el popup, para ello comparo los lados izquierdo, arriba, derecho y abajo que no se exeda
            if(posX<position.MinimoX){
                posX = position.MinimoX; //si es menor al minimo en x, entonces toma el valor del minimo
            }
            if(posXr > position.MaximoX){
                posX = position.MaximoX - popup.offsetWidth; //si es mayor al maximo en x, entonces toma el valor del maximo menos el ancho del componente popup
            }
            if(posY < position.MinimoY){
                posY = position.MinimoY; //si es menor al minimo en y, entonces toma el valor del minimo
            }
            if(posYb > position.MaximoY){
                posY = position.MaximoY - popup.offsetHeight; //si es mayor al maximo en y, entonces toma el valor del maximo menos el largo del componente popup
            }
           // console.log(position.centroY+" centroX: "+position.centroX+" mitadY: "+mitadY+" mitadX: "+mitadX+" posXr: "+posXr+" posyb: "+posYb);
            this.update(posX, posY);
            draw(this.props.datagraph, this.props.position);
        }
    }
    }

    //creo este metodo para que despues que se haya creado este elemnto, es decir el popup, se ejecute este
    //metodo y pueda actualizar la posicion del pop up, eso solo sucede cuando uso bootstrap, pues
    ///si no uso este metodo no se muestra el popup
    componentWillMount(){
        var popup = document.getElementsByClassName('popup_inner')[0];
       // console.log(popup+ "   "+document.getElementsByClassName('popup_inner').length);
        if(popup!=undefined){
        var data=popup.getBoundingClientRect();
        
        let position = this.props.popUpPos;
       // console.log("Este es el famoso top "+data.top+" "+popup.offsetHeight+" "+position.centroX);
        if(position.centroX!=undefined){
            //console.log("holap");
            //encuentro el centro del popup
            var mitadX = popup.offsetWidth / 2;//el ancho del popup para dos
            var mitadY = popup.offsetHeight / 2; //el alto del popup para dos

            //para el aldo izquierdo 
            var posX = position.centroX - mitadX; //resto..el centro del div de la materia menos la mitad del ancho del popup, me da la posicion en x en la que se debe ubicar
            //para el lado de arriba (top)
            var posY = position.centroY - mitadY; //resto..el centro del div de la materia menos la mitad del largo del popup, me da la posicion en y en la que se debe ubicar
            //para el aldo derecho
            var posXr = position.centroX + mitadX;
            //para el lado de abajo(bottom)
            var posYb = position.centroY + mitadY;
            
            //ahora comparo que no se salga del div el popup, para ello comparo los lados izquierdo, arriba, derecho y abajo que no se exeda
            if(posX<position.MinimoX){
                posX = position.MinimoX; //si es menor al minimo en x, entonces toma el valor del minimo
            }
            if(posXr > position.MaximoX){
                posX = position.MaximoX - popup.offsetWidth; //si es mayor al maximo en x, entonces toma el valor del maximo menos el ancho del componente popup
            }
            if(posY < position.MinimoY){
                posY = position.MinimoY; //si es menor al minimo en y, entonces toma el valor del minimo
            }
            if(posYb > position.MaximoY){
                posY = position.MaximoY - popup.offsetHeight; //si es mayor al maximo en y, entonces toma el valor del maximo menos el largo del componente popup
            }
           // console.log(position.centroY+" centroX: "+position.centroX+" mitadY: "+mitadY+" mitadX: "+mitadX+" posXr: "+posXr+" posyb: "+posYb);
            this.update(posX, posY);
            
        }
    }
    }

    update(posX, posY){
        //console.log("Estas son las coordenadas: x: "+posX+"  y: "+posY+" "+this.state.style.top);
        //console.log(this.props.position);
        d3.select(".popup_inner")
            .style('top', posY+'px')
            .style('left', posX+'px');
        // let coors = posY + 5;
        // let coorsm = posY - 5;
        // if(this.state.style.top !== posY || this.state.style.top >= coors || this.state.style.top <= coorsm){
        //     this.setState({style: {top: posY, left: posX}});
        // }
    }
    
    render(){
        return (
            <div className='popup'>
                <div className='popup_inner' style={this.state.style} >
                <span className="close glyphicon glyphicon-remove close-alert" onClick={this.props.closePopup}></span>
                <div className="contentPopLittle" onClick={this.props.closePopupOpenGrow}>
                <div className="asignaturaTitulo"><h3 className="h5Descripcion">{this.props.asignatura}</h3></div>
                {/*<button onClick={this.props.closePopup}>close me</button>
                <h1>holaaaaa</h1>*/}
                <div className="containersvg"></div>
                <div className={this.props.aprobacion+" base-little"}>{this.props.grades.length ? this.props.grades : ''}</div>
                </div>
                
              }
                </div>
            </div>
        );
    }
}
export default PopUpAsignatura;