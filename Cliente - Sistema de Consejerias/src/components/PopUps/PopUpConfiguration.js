import React from "react";
import '../../css/checkboxstyle.css';
import * as d3 from "d3";

//console.log(global.checkGlobal, window.checked);
class Configuration extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            check: global.checkGlobal
        }
    }
    checkChanged(e){
       // e.preventDefault();
        if(this.state.check===false){
            //alert("checked");
            d3.selectAll(".complexity").style('display', 'flex');
            d3.select('.legendComplexity').style('display', 'block');
            global.checkGlobal = true;
            global.showComplexity = "Ocultar Complejidad";
            this.setState({check: global.checkGlobal});
            //console.log(global.checkGlobal, window.checked);
        }else{
            d3.selectAll(".complexity").style('display', 'none');
            d3.select('.legendComplexity').style('display', 'none');
            global.checkGlobal = false;
            global.showComplexity = "Mostrar Complejidad";
            this.setState({check: global.checkGlobal});
        }
    }

    render(){
        return (
            <div className="popup z-index config">
                {/*<div className="popup_configuration" style={{backgroundImage: 'url('+process.env.PUBLIC_URL + '/images/background.jpg)'}} >*/}
                <div className="popup_configuration">
                    <span className="close glyphicon glyphicon-remove close-alert" onClick={this.props.closePopUpConfiguration}></span>
                    <div className="title-config">
                        <h1 className="title-config-text">Configuración</h1>
                    </div>
                    <div className="config-body">
                            <form className="form-show-complexity">
                            <label className="check-container" >
                                <input id="check-complexity"
                                    type="checkbox"
                                    defaultChecked={this.state.check}
                                    onChange={this.checkChanged.bind(this)}
                                >
                                </input>
                                <span className="checkmark"></span>
                                <label className="txt-show-complexity" htmlFor="check-complexity">
                                    {global.showComplexity !== undefined ? global.showComplexity : "Mostrar Complejidad"}
                                </label>
                            </label>
                            </form>
                        
                    </div>
                </div>
            </div>
        );
    }
}
export default Configuration;