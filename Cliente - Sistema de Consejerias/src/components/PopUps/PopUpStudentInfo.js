import React from 'react';

import {returnGeneralAverage} from '../../js/validations';
import {retakenCoursesTimes} from '../../js/validations';
let promedioGeneral = 0;
class PopUpStudentInfo extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="popup z-index">
                <div className="popup_student_info" style={this.props.style}>
                    <span className="close glyphicon glyphicon-remove close-alert" onClick={this.props.closePopUpStudentInfo}></span>
                    <div className="description">
                        <div className="popupStudentInfo">
                            <div className="popupStudentInfoPhoto" >Foto</div>
                            <div className="popUpStudentInfoData" >
                                <h1>{this.props.estudiante}</h1>
                                <h1 className="paddingBottomMain">{this.props.email}</h1>
                                
                                <h4>Carrera: {this.props.carrera}</h4>
                                <h4>Itinerario: </h4>
                            </div>
                        </div>
                        <div className="popupStudentInfoDetail">
                            <div className="popupStudentInfoDetailDescription">
                                <h5 className="paddingBottom">Promedio general : {Math.round(returnGeneralAverage(this.props.promediosStudent))}</h5>
                                
                                <h5>Total de asignaturas del programa: {this.props.totalAsignaturas}</h5>
                                <h5>Total de asignaturas cursadas: {this.props.totalAsignaturasCursadasPerdidas.size}</h5>
                                <h5>Total de asignaturas aprobadas: {this.props.totalAsignaturasAprobadas}</h5>
                                <h5>Veces que repitió asignaturas: {retakenCoursesTimes(this.props.totalAsignaturasCursadasPerdidas)}</h5>
                            </div>
                            <div className="popupStudentInfoDetailChart">
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PopUpStudentInfo;