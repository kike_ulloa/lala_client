import React from 'react';

import List from '../meetinghistorycounselor';
// import { request } from 'https';
const request = require('request-promise');
let fecha = "";
let tod ="";

class Observations extends React.Component{
    constructor(props){
        super(props);
        let defaultData =  [];
        defaultData.push(<tr>
        <td>Se ha acordado con el estudiante que va a cursar un numero menor de asignaturas para aumentar su promedio</td>
        <td>Juan Rodriguez</td>
        <td>08/07/2007</td>
        </tr>);
    defaultData.push(<tr>
       <td>Se ha acordado con el estudiante que su mejor opcion es cursar las asignaturas aun no aprobadas</td>
       <td>Juan Rodriguez</td>
       <td>16/07/2008</td>
       </tr>);
        this.state = {
            hour: this.getHour(),
            observations: "",
            enabled: "disabled",
            dataTable: defaultData,
            chan: true,
            size: defaultData.length,
            TextButton: "Agregar Observación"
        }
        
        fecha = this.getDate();
       
    }
    
    getDate(){
        
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var day = today.getDay();
        //var hour = today.getHours()+":"+String(today.getMinutes()).padStart(2 , 0) + ":" + String(today.getSeconds()).padStart(2, 0);
        today = dd + '/' + mm + '/' + yyyy;
        let meses = ["Enero", "Febrero", "Marzo", "Abril","Mayo","Junio","Julio","Agosto","Septimbre","Octubre","Noviembre","Diciembre"]
        let dias = ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"];
        //fecha = today;
        let fecha = dias[day] + ' ' + dd + ' de ' + meses[parseInt(mm) - 1] + ' del ' + yyyy;// + ", "+hour;
        tod = today;
        return fecha;
    }
    getHour(){
        var today = new Date();
        var hour = today.getHours()+":"+String(today.getMinutes()).padStart(2 , 0) + ":" + String(today.getSeconds()).padStart(2, 0);
        return hour;

    }
    componentDidMount() {
        this.interval = setInterval(() => this.setState({ hour: this.getHour() }), 1000);
      }
      componentWillUnmount() {
        clearInterval(this.interval);
      }
      

    onTextAreaChange(e){
        let val = e.target.value;
        if(val.length > 0){
            //console.log(val);
            this.setState({observations: val});
            this.setState({enabled: ""});
        }else{
            this.setState({enabled: "disabled"});
        }
        
    }
    onSave(e){
        e.preventDefault();
        
        let dat = this.state.dataTable;
        dat.push(<tr>
            <td>{this.state.observations}</td>
            <td>Consejero</td>
            <td>{tod}</td>
            </tr>);
            //console.log(dat);
            this.setState({dataTable: dat});
            this.setState({chan: false});
            let observationsDiv = document.getElementsByClassName("observations")[0];
            let observationstext = document.getElementsByClassName("observations-area")[0];
            observationsDiv.style.display = "none";
            observationstext.value = "";
            
            //guardo los datos llamando al webservice
            this.saveData();
            // global.tamano = global.tamano + 1;
            // this.setState({TextButton: "Agregar Observación"});
            
            // this.forceUpdate();
            // this.setState(this.state);
            //this.setState({chan: true});
    }
     async saveData(){
        ///aqui guardo los datos
            //primero obtengo la fecha
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            //doy formato a la fecha
            let fechaFormat = mm+"/"+dd+"/"+yyyy;
            let idStudent = this.props.student_id;
            //////el id de counselor debo tomarlo cuando inicia sesion, para esto necesita el correo o nombre de usuario
            let idCounselor = 1;
            let observaciones = this.state.observations;
            let update = ()=>{
                global.tamano = global.tamano + 1;
                this.setState({TextButton: "Agregar Observación"});
            }
            const ENDPOINTSAVE = 'http://localhost:3000/api/v1/saveNewMeetingObservations/';
            let resp =  await request({
                uri: ENDPOINTSAVE,
                json: true,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                rejectUnauthorized: false,
                form: {
                    studentid: idStudent,
                    counselorid: idCounselor,
                    observations: observaciones,
                    date: fechaFormat
                }
            }).then(result =>{
                //console.log(result);
                update();
                // global.tamano = global.tamano + 1;
                // this.setState({TextButton: "Agregar Observación"});
            });
            
            

            
            
        return resp;
    }
    
    onAddObservation(e){
        e.preventDefault();
        let observationsDiv = document.getElementsByClassName("observations")[0];
        if(this.state.TextButton.toLocaleLowerCase() !== "cancelar"){
            //console.log("entraaaaaa");
            observationsDiv.style.display = "flex";
            this.setState({TextButton: "Cancelar"});
        }else{
            observationsDiv.style.display = "none";
            this.setState({TextButton: "Agregar Observación"});
        }
    }
    render(){
        return (
            <div className="popup z-index observation">
                {/*<div className="popup_observations" style={{backgroundImage: 'url('+process.env.PUBLIC_URL + '/images/background.jpg)'}} >*/}
                <div className="popup_observations">
                    <span className="close glyphicon glyphicon-remove close-alert" onClick={this.props.closePopUpObservations}></span>
                    <div className="title-config">
                        <h1 className="title-config-text">Observaciones de la Sesión</h1>
                    </div>
                    <div className="observations-body">
                            <form className="form-observations">
                            <div className="student-date">
                                <label className="student-name-label">Estudiente: </label>
                                <label className="student-name">{this.props.student}</label>
                                <label className="fecha">{fecha}</label>
                            </div>
                            <div className="counselor">
                                <label className="counselor-name-label">Consejero: </label>
                                <label className="counselor-name">{this.props.counselor}</label>
                                <label className="hour-label" >{this.state.hour} </label>
                            </div>
                            <button type="button" onClick={e => this.onAddObservation(e)} className="btn btn-primary btn-guardar">{this.state.TextButton}</button>
                            <div className="observations">
                                <label className="label-observations">Observaciones:</label>
                                <textarea className="observations-area" placeholder="Ingrese información importante sobre la sesión" rows="5" onChange={e => this.onTextAreaChange(e)}></textarea>
                            
                            <button type="button" onClick={e => this.onSave(e)} className="btn btn-primary btn-guardar" disabled={this.state.enabled}>Guardar</button>
                            </div>
                            <div className="table-data">
                                
                                    <List student_id={this.props.student_id}/>
                            </div>
                            {/* <button type="button" onClick={e => this.onSubmit(e)} className="btn btn-primary btn-guardar">Mostrar Sesiones Anteriores</button> */}
                            {/* <div className="container-history">
                                    <label className="label-history"> Historial de sesiones</label>
                                    <div className="table-container" >
                                        <table className="table-history" >
                                            <tbody>
                                                <tr>
                                                    <th>Observaciones</th>
                                                    <th>Consejero</th>
                                                    <th>Fecha</th>
                                                </tr>
                                                
                                                {/* <tr>
                                                    <td>Se ha acordado con el estudiante que va a cursar un numero menor de asignaturas para aumentar su promedio</td>
                                                    <td>Juan Rodriguez</td>
                                                    <td>08/07/2007</td>
                                                </tr>
                                                <tr>
                                                    <td>Se ha acordado con el estudiante que su mejor opcion es cursar las asignaturas aun no aprobadas</td>
                                                    <td>Juan Rodriguez</td>
                                                    <td>16/07/2008</td>
                                                </tr> * aqui se cierra el comentaris en caso de usar este
                                                { this.state.dataTable.length === this.state.size ? this.state.dataTable : this.setState({size: this.state.dataTable.length})}
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    
                                </div> */}
                                
                            </form>
                        
                    </div>
                </div>
            </div>

        );
    }
}

export default Observations;