import React from 'react';
import {scaleLinear} from 'd3-scale';
import {max} from 'd3-array';
import {select} from 'd3-selection';
class BarChartPopUp1 extends React.Component {
    constructor(props){
        super(props);
        this.createBarChart = this.createBarChart.bind(this);
    }
    componentDidMount(){
        this.createBarChart();
    }
    componentDidUpdate(){
        this.createBarChart();
    }
    createBarChart() {
        const node = this.node
        //const dataMax = max(this.props.data)//extrae el maximo del data
        const dataMax = 100
        console.log(this.props.data);
        console.log(this.props.position);
        const yScale = scaleLinear()
           .domain([0, dataMax])
           .range([0, this.props.size[1]])//rango maximo es 100
  
      select(node)
         .selectAll('rect')
         .data(this.props.data)
         .enter()
         .append('rect')
     
     select(node)
        .selectAll('rect')
        .data(this.props.data)
        .exit()
        .remove()
        
     select(node)
        .selectAll('rect')
        .data(this.props.data)
    .style('fill', (d,i) => {if(i===10){return 'lightcoral';}else if(i===this.props.position){if(this.props.position < 6)return 'lightcoral'; else return 'lightgreen';}else if(i===12){if(this.props.position < 6)return 'lightcoral'; else return 'lightgreen';}else if(i===11){return 'lightgreen';}else{return 'darkgray';/*return '#fe9922';*/}})
    .attr('x', (d,i) => {if(i===10){return 0;}else if(i===12){return (this.props.position*25)}else if(i===11){return (6*25)-5;/*return (this.props.position*25)+20*/}else{return i * 25}})
        .attr('y', (d,i) => {if(i===10 || i===11 || i===12){return 103;}else{return this.props.size[1] - yScale(d)}})
        .attr('height', (d,i) => {if(i===10 || i===11 || i===12){return 10;}else{return yScale(d)}})
        .attr('width', (d,i) => {if(i===10){return this.props.position*25}else if(i===12){return 20}else if(i===11){return (this.props.data.length-this.props.position)*25}else{return 20;}})

     }
     
     
    render(){
        return (
           
            <svg className="svg" ref={node => this.node = node}
      width={this.props.data.length ? (this.props.data.length-3)*25 : 250} height={115}>
      {/*<rect x="0" y="103" height="10" width="150" ></rect>*/}
      </svg>
      
      
        );
    }
}
export default BarChartPopUp1;