import * as d3 from 'd3';
import {maxLevelsToGraph} from '../../js/validations';

export function draw(data, position){
    //defino los margenes
    d3.select(".svgCourseMiniChart").remove();
    const margin = {top: 10, bottom: 10, left: 10, right: 10};
    //maximo del eje y (100 nota maxima)
    const dataMaxValida = d3.max(data)//extrae el maximo del data
    if(dataMaxValida > 0){
        const labelGrids= maxLevelsToGraph(dataMaxValida);
        
        const dataMax = d3.max(labelGrids);
        //const dataMax = 100;

        const yScale =  d3.scaleLinear()
            .domain([dataMax, 0])
            .range([dataMax,0]);
        //creo el svg en el div
        let svg = d3.select('.containersvg')
            .append('svg')
            .attr('class', 'svgCourseMiniChart')
            .attr('preserveAspectRatio', 'xMinYMin')
            .attr('viewBox', '0 0 275 83')
            .style("min-width","100%")
            .style("min-height", "100%")
            .append('g')
            .attr('transform', 'translate(10, 0)');//creo el g y lo traslado para que no se muestre al inicio en la pos cero de x sino desde la 10 todas las barras.

        svg.selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .style('fill', (d, i) => {
                if(i===10){
                    return 'lightcoral';
                }else if(i===position){
                    if(position < 6) return 'lightcoral';
                    else return 'lightgreen';
                }else if(i === 12){
                    if(position < 6) return 'lightcoral';
                    else return 'lightgreen';
                }else if(i === 11){
                    return 'lightgreen';
                }else return 'darkgray';
            })
            .attr('x', (d, i) => {
                if(i === 10) return 0;//si es la primera barra hrizontal, se va a hubicar en la primera posicion, osea desde la cero.
                else if(i === 12) return position * 25; //si es la barra horizontal que representa donde se encuentra el estudiante, entonces s eva a hubicar en la posicion que seencuentra, por eso la operacion de posicion por 25
                else if(i === 11) return (6 * 25) - 5;//si es la ultima barra horzontal, se va a hubicar desde la posicion que finaliza la primera barra hasta el final
                else return i*25 ///si no son las barras horizontales, entonces indico de que se posicionen cada 25 (20 el ancho de cada barra y 5 los espacios entre las barras).
            })
            .attr('y', (d, i) => {
                if(i === 10 || i === 11 || i === 12) return 103;//si son las barras horizontales al pie, entonces indico qe se van a hubicar desde la pos 103, ya que 100 es el maximo de las verticales.
                else {//caso contrario resto el tamano maximo menos la escala (tambien para no hacer la resta, se puede definir en el rango que no vaya de 0 a datamax, sino de datamax a cero)
                    // if(yScale(d) === dataMax){
                    //     return (100)
                    // }
                    return dataMax - yScale(d);
                }
            })
            .attr('height', (d, i) => {
                if(i === 10 || i === 11 || i === 12) return 10;//en caso de que sean las barrar horzontales, van a tener un alto de 20
                else{//cuando no son la sbarras horizontales, sino verticales
                    let max = (dataMax - yScale(d));
                    //console.log(max);
                    if(max === dataMax){//sognifica que es cero, por lo tanto, el tamano o el alto debe ser cero
                        return yScale(d);
                    }
                    return 100 - max;
                } 
                //return yScale(d);//si no son las barras horizontales al pie, entonces retorna el alto de la barra
            })
            .attr('width', (d, i) => {
                if(i === 10) return 6*25;//6 porq el 6 representa 60 (59, de 60 a 69 etc), va asi: 0-9, 10-19, 20-29, 30-39, 40-49, 50-59, en total son 6 intervalos, y es 25 porque cuenta los espacios de 5 tambien
                else if(i === 12) return 20;//esta barra es la del pie que representa donde se encuentra el estudiante (esta por demas, pero servira en caso de cambiar los colores)
                else if(i === 11) return 4 * 25;//4 por25 porq va asi: 60-69, 70-79, 80-89, 90-100, en total son 4 intervalos
                else return 20; //si no es ninguna barra al pie, entonces solo devuelvo el ancho de la barra vertical que seria de 20
            });
        }else{
            //creo el svg en el div
            let svg = d3.select('.containersvg')
            .append('svg')
            .attr('class', 'svgCourseMiniChart')
            .attr('preserveAspectRatio', 'xMinYMin')
            .attr('viewBox', '0 0 275 83')
            .style("min-width","100%")
            .style("min-height", "100%")
            .append('g')
            .attr('transform', 'translate(65, 50)')
            .attr('class', 'g-cursando')
            .append('text').attr('x', 0).attr('y', 0).attr('class', 'txt-cursando').text("CURSANDO...");
        }
}