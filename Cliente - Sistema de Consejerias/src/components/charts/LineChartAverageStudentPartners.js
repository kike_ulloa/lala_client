import React from 'react';
import {scaleLinear} from 'd3-scale';
import {max} from 'd3-array';
import {extent} from 'd3-array';
import {select} from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import {line} from 'd3-shape';
import {scaleTime} from 'd3-scale';
import {scalePoint} from 'd3-scale';

class AverageStudentPartners extends React.Component{
    constructor(props){
        super(props);
        this.createLineChart = this.createLineChart.bind(this);
    }
    componentDidMount(){
        this.createLineChart();
    }
    componentDidUpdate(){
        this.createLineChart();
    }
    
    createLineChart(){        
        var datos = this.props.data;
        console.log(datos);
        //defino el tamano de la grafica
        const margin = {top: 10, right: 10, bottom: 10, left: 10};
        const width = 325 - margin.left - margin.right;
        const height = 250 - margin.top - margin.bottom;
        var r = [datos];
        
        
        // Define the scales and tell D3 how to draw the line
        //const xn = scalePoint().domain(this.props.dates).rangeRound([0, width]); 
        //para los meses
        const fechas= this.props.dates;

        var formatXLabel = function(d,i) {
           // console.log("este es d: "+(d%3)+"  este es i: "+i);
            return fechas[d % fechas.length]+"";      
        }
        //console.log(formatDay+"     "+datos.student.data.length );
        const xn = scalePoint().domain(this.props.dates).rangeRound([0, width]); 
        const x = scaleLinear().domain([0, datos.student.data.length]).range([0, width]);     //el tamano de los promedios del estudiant multiplicado por 20(distancia entree puntos)
        const y = scaleLinear().domain([0, 100]).range([height, 0]); // el alto maximo es 100 porque 100 es la nota maxima
        //const line = line().x(d => x(d.month)).y(d => y(d.averages));
        const lines = line().x((d,i) => x(i)).y((d,i) => y(d));
        //para los anios
        let anios = this.props.anios;
        //anios.push("2008");
        let tam = anios.length ;
        console.log("Este es el tamano de anios "+anios[2]);
        let co = -1;
        var formatYears = function(d,i){
            console.log("estos son los anios : "+d+"   "+(co));
            co++;
            if(co==anios.length){
                return "";
            }else{
                return anios[co]+"";
            }
            
        }
        const xAnios = scaleLinear().domain([0, anios.length]).range([0, width]);
        const axisYear = axisBottom(xAnios).tickFormat(formatYears).ticks(tam);
        // Add the axes and a title
        const xAxis = axisBottom(x).tickFormat(formatXLabel).ticks(fechas.length);
        const yAxis = axisLeft(y).ticks(2);
        const node =  this.node;
        const node2 = this.node2;
        if(fechas.length>0){
            select(node2)
            .selectAll('g')
            .data([])
            .enter()
            .append('g');
        select(node2)
            .selectAll('g')
            .data([])
            .exit()
            .remove();
        select(node)
            .append('g').attr('transform', 'translate(35, 10)').call(yAxis); 
        select(node)
            .append('g').attr('transform', 'translate(35,' + height + ')').call(xAxis);
        select(node2)
            .append('g').attr('transform', 'translate(35, '+(height+15)+')').call(axisYear);
        // select(node)
        //     .append('text').html('State Population Over Time').attr('x', 200);
            let states;
            
            let da = [];
            da.push(datos.partners);
            da.push(datos.student);
            console.log(da[1].name);

            da.map((d, i)=>{
                console.log(d.name+ " " +i+"  "+parseInt(d.data[1]));
                //console.log(d.partners.name);

                states = d;
                console.log(states);
                select(node)
                    .append('path')
                    .attr('fill', 'none')
                    .attr('stroke', function(){if(i===0){return 'black';}else{console.log("este es i "+d);return 'blue'}})
                    .attr('stroke-width', 2)
                    .attr('transform', 'translate(35, 15)')
                    //.datum(d => d.data)
                    .attr('d', lines(d.data, i));
                
                select(node)
                    .append('text')
                    .html(d.name)
                    .attr('fill', function(){
                        if(i===0){
                            return "black";
                        }else{
                            return "blue";
                        }
                    })
                    .attr('alignment-baseline', 'middle')
                    .attr('x', width)
                    .attr('dx', '.5em')
                    .attr('y', y(d.data[d.data.length-1])); 
                
                // tipBox = chart.append('rect')
                //     .attr('width', width)
                //     .attr('height', height)
                //     .attr('opacity', 0)
                //     .on('mousemove', drawTooltip)
                //     .on('mouseout', removeTooltip);
            });
            
        }

    }
    createLineChartM(){
        /* implementation heavily influenced by http://bl.ocks.org/1166403 */
		
		// define dimensions of graph
		var m = [20, 20, 20, 20]; // margins
		var w = 500 - m[1] - m[3]; // width
		var h = 400 - m[0] - m[2]; // height
		
		// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
		var data = [3, 6, 2, 7, 5, 2, 0, 3, 8, 9, 2, 5, 9, 3, 6, 3, 6, 2, 7, 5, 2, 1, 3, 8, 9, 2, 5, 9, 2, 7];

		// X scale will fit all values from data[] within pixels 0-w
		var x = scaleLinear().domain([0, data.length]).range([0, w]);
		// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
		var y = scaleLinear().domain([0, 10]).range([h, 0]);
			// automatically determining max range can work something like this
			// var y = d3.scale.linear().domain([0, d3.max(data)]).range([h, 0]);

		// create a line function that can convert data[] into x and y points
		var linea = line()
			// assign the X function to plot our line as we wish
			.x(function(d,i) { 
				// verbose logging to show what's actually being done
				console.log('Plotting X value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
				// return the X coordinate where we want to plot this datapoint
				return x(i); 
			})
			.y(function(d) { 
				// verbose logging to show what's actually being done
				console.log('Plotting Y value for data point: ' + d + ' to be at: ' + y(d) + " using our yScale.");
				// return the Y coordinate where we want to plot this datapoint
				return y(d); 
			})

            // Add an SVG element with the desired dimensions and margin.
			// var graph = d3.select("#graph").append("svg:svg")
			//       .attr("width", w + m[1] + m[3])
			//       .attr("height", h + m[0] + m[2])
			//     .append("svg:g")
			//       .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

			// create yAxis
			var xAxis = axisBottom().scale(x).tickSize(-h);
            // Add the x-axis.
            const node = this.node;
            select(node)
			    .append("g")
			      .attr("class", "x axis")
			      .attr("transform", "translate(0," + h + ")")
			      .call(xAxis);


			// create left yAxis
			var yAxisLeft = axisLeft().scale(y).ticks(4);
            // Add the y-axis to the left
            select(node)
			    .append("g")
			      .attr("class", "y axis")
			      .attr("transform", "translate(-25,0)")
			      .call(yAxisLeft);
			
  			// Add the line by appending an svg:path element with the data line we created above
			// do this AFTER the axes above so that the line is above the tick-lines
  			select(node).append("path").attr("d", linea(data)).attr("fill", "none").attr("stroke","blue").attr("stroke-width","2");
    }
    createLineChart3(){
        console.log(this.props.data);
        //console.log(this.props.data.student.data.length+"   "+this.props.data.student.data);
        var data = this.props.data.student.data.map(function(d, i) {
            return {
               date: i,
               close: d
            };
            
        });
        const node = this.node;
        let tickY = [60];
        let yScale = scaleLinear()
                        .domain([0, 100]) ///el dominio es de 0 a 100 osea, 
                        .range([0, 200]); //200 es e alto de svg, ese lo defino yo
        let xscale = scaleLinear()
                        .domain([0, this.props.data.student.data.length])
        //para el eje y
        let y = scaleLinear()
                .domain([0, 100]) ///el dominio es de 0 a 100 osea, 
                .range([200, 0]); //200 es e alto de svg, ese lo defino yo, se le pone alreves para que tome como 200 al pixel 0 y 0 al pixel 200

        var yAxis = axisLeft(y)
        //.scale(y)
        .tickValues(tickY)
        .ticks(tickY.length)
        .tickFormat("")
        .tickSize(-200,0,0);

        var xScale = scaleLinear()
                .domain([0, 100]) // input
                .range([0, 200]); // output
        var lin = line()
        .x(function(d, i) { return d.date*100; }) // set the x values for the line generator
        .y(function(d) { return d.close; }) // set the y values for the line generator 

        select(node)
            .append("g")
            .attr("class", "y axis")
            .attr('transform', 'translate(15, 150)')
            .call(yAxis);

        select(node)
            .append("path")
            .datum(data) // 10. Binds data to the line 
            .attr("class", "line") // Assign a class for styling 
            .attr("d", lin); // 11. Calls the line generator
    }
    createLineChart1(){
        var arrData = [
            [1,200],
            [2, 300], 
            [3, 150]];



       // var parseDate = time.format("%Y-%m-%d").parse;time


        var x = scaleTime()
            .range([0, 300])

        var y = scaleLinear()
            .range([400, 0]);

        var xAxis = axisBottom()
            .scale(x);
            

        var yAxis = axisLeft()
            .scale(y)
            

        var linea = line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.close); });

        var data = arrData.map(function(d) {
            return {
                date: d[0],
                close: d[1]
            };
            
        });

        console.log(data);


        x.domain(extent(data, function(d) { return d.date; }));
        y.domain(extent(data, function(d) { return d.close; }));
        const node = this.node;
        select(node)
            .selectAll('g')
            .data([])
            .enter()
            .append('g');
        select(node)
            .selectAll('g')
            .data([])
            .exit()
            .remove();
            select(node)
        .selectAll('path')
            .datum([])
            .enter()
            .append('path');
        select(node)
            .selectAll('path')
            .datum([])
            .exit()
            .remove();
        
        select(node)
            .append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + 300 + ")")
            .call(xAxis);
        select(node)
            .append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("Price ($)");
        select(node)
            .append("path")
            .datum(data)
            .attr("class", "line")
            .attr("d", linea)
            .attr("fill", "none")
            .attr("stroke", "black");
        
    }
    render(){
        return(
            <svg className="svgIm" ref={node => this.node = node}
                width="350" height="350">
                <g className="g" ref={node2 => this.node2 = node2}></g>
            </svg>
        );
    }
}
export default AverageStudentPartners;