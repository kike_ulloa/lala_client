import React from 'react';
import {scaleLinear} from 'd3-scale';
import {max} from 'd3-array';
import {select} from 'd3-selection';
import { axisBottom, axisLeft } from 'd3-axis';
import {symbol} from 'd3-shape';
//importo funciones de validaciones
import {maxLevelsToGraph} from '../../js/validations.js'
import {grades} from '../../js/validations';
///detecto si el navegador es firefox
let isFirefox = typeof InstallTrigger !== 'undefined';
//let responsive = "0 0 475 283";///para cuando sea en ves de todos los 45 el numero 40
let responsive = "0 0 500 283";
class PopUpNotasComparadasGrow extends React.Component{
    constructor(){
        super();
        this.createBarChar = this.createBarChar.bind(this);
    }
    componentDidMount(){
        this.createBarChar();
       
       
    }
    
    componentDidUpdate(){
        this.createBarChar();
    }
    createBarChar(){
        if(isFirefox){
            responsive = "-4 -2 495 283";
        }
        var triangulo = symbol().size(1000).type(symbol.triangle);
       // width = 35 + this.props.data.length ? +(this.props.data.length-3)*40 : 424 ;
       ///esto va en el render en el svg pero como uso responsivo con el viewbox ya no hace falta
       /* width={this.props.data.length ? 'calc(7% + '+(this.props.data.length-3)*40+')' : 424 } height='100%'*/
        const node = this.node
        const dataMaxValida = max(this.props.data)//extrae el maximo del data
        if(dataMaxValida > 0){
            const labelGrids= maxLevelsToGraph(dataMaxValida);
            const dataMax = max(labelGrids);
            const node2 = this.node2;//se hace un segundo nodo para el eje y, pues al poner las ticks(grids) con el node estas se sobremontan y se ponen por encima de las barras, al crear otro nodo se soluciona este inconveniene, 
            console.log("este es el maximo: "+dataMaxValida+" este es el max porcentaje "+dataMax + " los label girds: "+labelGrids);
            //const dataMax = 100
            const yScale = scaleLinear()
            .domain([0, dataMax])
            .range([0, 200])//rango maximo es 100(en este caso seria el valor que le defina yo, siendo este el alto toal del svg)
    
            
            ///defino el axis de y
            const y = scaleLinear().domain([0,dataMax]).range([200, 0]);//rango maximo (de 0 a 100)
            var yAxis = axisLeft(y)
            //.scale(y)
            .tickValues(labelGrids)
            .ticks(labelGrids.length)
            .tickFormat(d => d + "%")
            .tickSize(-(this.props.data.length-3)*45,0,0);
            ///esto es para cuando son varios datos, es decir, un array, con esto se indica que entre al array, lo rrecorra y elimine los que se repiten
            ///en este caso se envia un array de cero asi no se crearan <g> vacios o repetidos, como en el caso de los ticks que seesta creando 2 veces en 
            ///vez de crearse solo una
            select(node2)
                .selectAll('g')
                .data([])
                .enter()
                .append('g');
            select(node2)
                .selectAll('g')
                .data([])
                .exit()
                .remove();
            select(node2).
                append("g")
                .attr("class", "y axis")
                .attr('transform', 'translate(15, 150)')
                //.attr('x',200)
                .call(yAxis);
                //.attr('x', 0)
                //.attr('y', 50);
            //para el eje xES DIFERENCI
            let gradess = grades();
            const x = scaleLinear().domain([0,max(gradess)]).range([0, (this.props.data.length-3)*45]);
            var xAxis = axisBottom()
                .scale(x)
                .tickValues(gradess)
                .ticks(gradess.length)
                .tickSize(0);
            select(node)
                .append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(25, 220)")
                .call(xAxis);
            
                /**Dibujo la felcha en la posicion que se encuentra el estudiante con
                 * su respectivo color, o bien perdio o paso.
                 */
            var posi = this.props.position;
            /**Defino en x la posicion y tambn en y */
            var xPos = (this.props.position + 1) * 45 + 10, yPos = 230;
            select(node)
                .append('path')
                .attr('d', function(d) { 
                    /**Esta linea crea la flecha que apunta hacia arriba
                     * el 15 es cuan ancho es, el 20 cuan largo es y el - 3o
                     * es el doble de 15 siempre, con ello se forma un triangulo
                     */
                return 'M ' + xPos +' '+ yPos + ' l 15 20 l -30 0 z';
                })
                .style('fill', (d) => {
                    if(posi < 6){//en caso de que la posicion sea enor a 6 (60) significa que perdio
                        return 'lightcoral';
                    }else{
                        return 'lightgreen';
                    }
                });    
                /**Dibujo la nota debajo de la flecha que acabo de dibujar. */
                select(node)
                    .append('text')
                    .attr('x', xPos - 10)
                    .attr('y', (yPos + 35))
                    .text(Math.round(this.props.notaF));
                select(node)
                .selectAll('rect')
                .data(this.props.data)
                .enter()
                .append('rect')
            
            select(node)
                .selectAll('rect')
                .data(this.props.data)
                .exit()
                .remove()
            select(node)
                .selectAll('rect')
                .data(this.props.data)
                .style('fill', (d,i) => {console.log("este es el i "+i);if(i===10){return 'lightcoral';}else if(i===this.props.position){if(this.props.position < 6)return 'lightcoral'; else return 'lightgreen';}else if(i===12){if(this.props.position < 6)return 'lightcoral'; else return 'lightgreen';}else if(i===11){return 'lightgreen';}else{/*return '#0f52ba';*/return 'darkgray';/*return '#fe9922';*/}})
                .attr('x', (d,i) => {if(i===10){return '7%';}else if(i===12){return 'calc(7% + '+(this.props.position*45)+')'}else if(i===11){return 'calc(7% + '+((6*45)-5)+')'/*return 'calc(7% + '+((this.props.position*40)+35)+')'*/}else{return 'calc(7% + '+(i * 45)+')'}})
                .attr('y', (d,i) => {if(i===10 || i===11 || i===12){return "calc(100% - 75)";}else{return "calc(100% - "+((80+yScale(d)))+")"}}) //(yScale(d)+yScale(d)-(this.props.size[1]-yScale(d)))
                .attr('height', (d,i) => {if(i===10 || i===11 || i===12){return 10;}else{return yScale(d)}})
                .attr('width', (d,i) => {if(i===10){if(isFirefox){return ((6*45)-5);}else return 'calc(0% + '+((6*45)-5)+')'}else if(i===12){return 35}else if(i===11){return (((this.props.data.length - 3) * 45)-((6*45)-5));}else{return 35;}})
                .style("transform", (d,i) => {
                    if(isFirefox){
                        if(i===10){
                            return 'translate(0%, calc(100% - 75px))';
                        }else if(i===12){
                            return 'translate(calc(7% + '+(this.props.position*40)+'px), calc(100% - 75px))';
                        }else if(i===11){
                            return 'translate(calc(7% + '+((6*45)-5)+'px), calc(100% - 75px))';
                        }else{
                            return 'translate(calc(7% + '+(i * 45)+'px), calc(100% - '+((80+yScale(d)))+'px))';
                        }
                    }else
                        return 'none';
                })
        }else{
            if(select(node).select('.g-cursando').node() === null){//este if evita que se graficque dos veces ya que eso sucede porque se actualiza al llmarlo desde componentDidUpdate
            select(node)
                .append('g')
                .attr("class", "g-cursando")
                .attr('transform', 'translate(200, 130)')
                .append('text')
                    .attr('x', 0)
                    .attr('y', 0)
                    .text('CURSANDO...');
            }
        }
            // select(node)
            // .selectAll('rect')
            // .data(this.props.data)
            // .style('fill', (d,i) => {console.log("este es el i "+i);if(i===10){return 'lightcoral';}else if(i===this.props.position){return 'orange';}else if(i===12){return 'orange';}else if(i===11){return 'lightgreen';}else{return '#0f52ba';/*return 'darkgray';return '#fe9922';*/}})
            // .attr('x', (d,i) => {if(i===10){return 'calc(7% - 0px)';}else if(i===12){return 'calc(7% + '+(this.props.position*40)+'px)'}else if(i===11){return 'calc(7% + '+((6*40)-5)+'px)'/*return 'calc(7% + '+((this.props.position*40)+35)+')'*/}else{return 'calc(7% + '+(i * 40)+')'}})
            // .attr('y', (d,i) => {if(i===10 || i===11 || i===12){return "calc(100% - 75px)";}else{return "calc(100% - "+((80+yScale(d)))+"px)"}}) //(yScale(d)+yScale(d)-(this.props.size[1]-yScale(d)))
            // .style('height', (d,i) => {if(i===10 || i===11 || i===12){return 10+'px';}else{return yScale(d)}})
            // .style('width', (d,i) => {if(i===10){return 'calc(7% + '+this.props.position*40+'px)'}else if(i===12){return 35+'px';}else if(i===11){return (((this.props.data.length - 3) * 40)-((6*40)-5))+'px';}else{return 35+'px';}})
        
    }
    //en el tamano, es decir, en width, sumo el 7% ya que ese porcentaje es lo que esta ocupando los labels de y con porcentajes
    render(){
        return (
            <svg className="svg" ref={node => this.node = node}
                preserveAspectRatio="xMidYMid meet" 
                viewBox={responsive}
               >
                {/*<rect x="0" y="103" height="10" width="150" ></rect>*/}
                <g ref={node2 => this.node2 = node2}></g>
            </svg>
        );
    }
}
export default PopUpNotasComparadasGrow;