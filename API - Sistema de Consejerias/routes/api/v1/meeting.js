const { check, validationResult, oneOf } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');
module.exports = function(API , crudOperations , db , oauth2Authentication){

    const errorsTypes = { INVALID_PARAMETER : 'InvalidParameter',MISSING_PARAMETER : 'MissingParameter'};
  
    const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
      return {type:msg.type,msg:`${location}[${param}]: ${msg.msg}`};
    };

    API.get('/getStudentHistorySessions/', oauth2Authentication, [
        check('studentid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists()
    ], function(req, res, next){
        const errors = validationResult(req).formatWith(errorFormatter);
        if (!errors.isEmpty()){
            res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
        }
        else{
            const processRequest = async()=>{

                try {
      
                  // Get the curriculum and the program object
                  const data = await crudOperations.getMeetingHistory(parseInt(req.query.studentid));
                  // If the data is not found
                  if(!data)
                    res.status(400).json({errors:[{status:404,error:"StudentNotFound",detail:"There is not history for given student."}]});
      
                  else{
      
                    console.log(data.length);
                    //res.json(terms);
                    res.json(data);
                    //res.json(createStudentAcademicResponse(data,historyAcademics,loadingDate));
                  }
                }
                catch (e) {
                  console.log(e);
                  res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
                }
              }
      
              processRequest();
        }
    });
    //Servicio rest para guardar la snuevas consegerias, este servicio es de tipo post ya que guarda en la base de datos
    API.post('/saveNewMeetingObservations/', oauth2Authentication, function(req, res, next){
      const errors = validationResult(req).formatWith(errorFormatter);
        if (!errors.isEmpty()){
            res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
        }
        else{
          try{
            //console.log(req);
              console.log(req.body.studentid, req.body.counselorid, req.body.observations, req.body.date);
              
            const resp = crudOperations.addMeeting(req.body.studentid, req.body.counselorid, req.body.observations, req.body.date)
            res.json(resp);
          }catch (e){
            console.log(e);
            res.json('error');
          }
        }
    });

}
