var express = require('express');
var API = express.Router();
const OktaJwtVerifier = require('@okta/jwt-verifier')

var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json').v1[env];

module.exports =  function(crudOperations,db){

  const oktaJwtVerifier = new OktaJwtVerifier({
    issuer: config.oauth2Server,
    clientId: config.clientId, //ClientId API so that it can get the pub key from the oauth2Authentication sever
  });

  const oauth2Authentication = async (req,res,next) => {
    next();
    /*try {
      const { authorization } = req.headers
      if (!authorization) throw new Error('You must send an Authorization header')

      const [authType, token] = authorization.split(' ')
      if (authType !== 'Bearer') throw new Error('Expected a Bearer token')

      if(!token) throw new Error('An access token must be provided')

      await oktaJwtVerifier.verifyAccessToken(token)
      next();

    }
    catch (error) {

      //console.log(error,error.name);

      switch (error.name) {
        case 'Error':
              res.status(403).json({errors:[{status:403,error:error.message}]});
              break;
        case 'JwtParseError':
              error.userMessage === 'Jwt is expired' ? res.status(403).json({errors:[{status:403,error:"The access token has expired"}]}) : res.status(403).json({errors:[{status:403,error:"The access token is invalid"}]});
              break;
        default:
              res.status(403).json({errors:[{status:403,error:"Error"}]});
      }
    }*/
  }

  require('./programs')(API , crudOperations , db , oauth2Authentication);

  require('./students')(API , crudOperations , db , oauth2Authentication);

  require('./courses')(API , crudOperations , db , oauth2Authentication);

  require('./meeting')(API , crudOperations , db , oauth2Authentication);

  return API;
};
