const { check, validationResult, oneOf } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');


module.exports = function(API , crudOperations , db , oauth2Authentication){

  const errorsTypes = { INVALID_PARAMETER : 'InvalidParameter',MISSING_PARAMETER : 'MissingParameter'};

  const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
    return {type:msg.type,msg:`${location}[${param}]: ${msg.msg}`};
  };

  /**
   * @api {get} /coursesAcademics/:code Request Statistics information from an specific course.
   * @apiVersion 1.0.0
   * @apiName GetCourseAcademics
   * @apiGroup Course
   *
   * @apiParam {String} code Course code.
   *
   * @apiHeader {String} access-token Unique access-token.
   *
   * @apiExample {curl} Example usage:
   *     curl -i https://localhost:3000/api/v1/coursesAcademics/EYAG1012
   *
   * @apiSuccess {[ACADEMICS_BY_TERM]} academicsByTerm   An array of ACADEMICS_BY_TERM of the Course .
   * @apiSuccess {Integer} year  The year of the term.
   * @apiSuccess {String} term  The semester of the Term.
   * @apiSuccess {StatisticByTerm} academics  Statistic of the Course in a specific Term.
   *
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   {
       "academicsByTerm": [
           {
               "year": 2018,
               "semester": "1S",
               "academics": {
                   "distribution": [
                       {
                           "label": "label1",
                           "value": 2
                       },
                       {
                           "label": "label2",
                           "value": 1
                       },
                       {
                           "label": "label3",
                           "value": 2
                       }
                   ],
                   "dist_range": {
                       "max": 3,
                       "min": 2
                   },
                   "student_count": null,
                   "fail_rate": null,
                   "drop_rate": null,
                   "count_by_delay": null
               }
           },
           {
               "year": 2015,
               "semester": "1S",
               "academics": {
                   "distribution": [
                       {
                           "label": "label1",
                           "value": 2
                       },
                       {
                           "label": "label2",
                           "value": 1
                       },
                       {
                           "label": "label3",
                           "value": 2
                       }
                   ],
                   "dist_range": {
                       "max": 3,
                       "min": 2
                   },
                   "student_count": null,
                   "fail_rate": null,
                   "drop_rate": null,
                   "count_by_delay": null
               }
           },
           {
               "year": 2015,
               "semester": "2S",
               "academics": {
                   "distribution": [
                       {
                           "label": "label1",
                           "value": 2
                       },
                       {
                           "label": "label2",
                           "value": 1
                       },
                       {
                           "label": "label3",
                           "value": 2
                       }
                   ],
                   "dist_range": {
                       "max": 3,
                       "min": 2
                   },
                   "student_count": null,
                   "fail_rate": null,
                   "drop_rate": null,
                   "count_by_delay": null
               }
           }
       ]
   }
   *
   * @apiError ProgramNotFound The <code>id</code> or the <code>year</code> of the Program was not found.
   * @apiError NoAccessRight The <code>id</code> of the User was not found.
   *
   *
   * @apiErrorExample {json} Error-courseNotFound-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *        "errors": [
   *           {
   *                "status": 404,
   *                "error": "coursesAcademicsNotFound",
   *                "detail": "Wasn't found data for that cours."
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-NoAccessRight-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "error": "NoAccessRight"
   *     }
   */

    API.get('/coursesAcademics/:id',oauth2Authentication,[

      sanitize('id').trim().escape(),
      //check('id',{type:errorsTypes.INVALID_PARAMETER,msg:'Must be an Integer'}).isInt(),

    ],function(req,res,next){

      const errors = validationResult(req).formatWith(errorFormatter);

      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{

        const processRequest = async()=>{

          try {

            // Get the curriculum and the program object
            console.log("entra al catch");
            const data = await crudOperations.getCourseAcademics(req.params.id);
            console.log("linea 1");
            // If the data is not found
            if(!data.length)
              res.status(400).json({errors:[{status:404,error:"coursesAcademicsNotFound",detail:"Wasn't found data for that course"}]});

            else{
              //res.json(data);
              res.json(createResponseProgram(data));
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequest();

      };
    });

    /**
    * Summary. Create a response JSON.
    *
    * Description. Create a response JSON with data of a student, Program,
    * ProgramTerms and Courses
    *
    * @param {Object}   program       An Object with information about the Program and the Curriculum.
    * @param {Object}   terms         An Array with information about the ProgramTerms and the Courses.
    *
    * @return {Object} The response object.
    */

    const createResponseProgram = (academics) =>{

      const returnValue = {

        id:              academics[0].course_id,
        academicsByTerm: academics.map(academic => {
                                return {
                                  year:     academic.term.year,
                                  semester: academic.term.semester,
                                  academics: {
                                      distribution:   academic.distribution,
                                      dist_range:     academic.dist_range,
                                      student_count:  academic.student_count,
                                      fail_rate:      academic.fail_rate,
                                      drop_rate:      academic.drop_rate,
                                      count_by_delay: academic.count_by_delay
                                  }
                                }
                            })
      };
    /*  if( terms.length!==0 ){
        const returnTerms = terms.map(term => {
          return {
            year:           term.year,
            semester:       term.semester,
            termAvg:        '-------------------------------',
            accAvg:         '-------------------------------',
            //coursesTaken:    term.history_academic_courses,
            coursesTaken:    term.history_academic_courses.map( history_academic_courses =>{
              return {
                courseId:     history_academic_courses.course.code,
                name:         history_academic_courses.course.name,
                grade:        history_academic_courses.grade,
                registration: history_academic_courses.registration,
                state:        history_academic_courses.state,
                group:        history_academic_courses.course.statistic,
                cohort:       history_academic_courses.course.statisticCohorts.length !== 0 ? history_academic_courses.course.statisticCohorts[0] : []
              }
            })
          };
        });

        terms = returnTerms;
      }*/
      //returnValue.terms = terms;

      return returnValue;
    }

}
