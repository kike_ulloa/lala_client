const { check, validationResult, oneOf } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');


module.exports = function(API , crudOperations , db , oauth2Authentication){

  const errorsTypes = { INVALID_PARAMETER : 'InvalidParameter',MISSING_PARAMETER : 'MissingParameter'};

  const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
    return {type:msg.type,msg:`${location}[${param}]: ${msg.msg}`};
  };

  /**
   * @api {get} /programs/:id Request Program information from a specific year
   * @apiVersion 1.0.0
   * @apiName GetProgram
   * @apiGroup Program
   *
   * @apiParam {Integer} id Program unique ID.
   * @apiParam {Integer} year  Year of the program.
   *
   * @apiHeader {String} access-token Unique access-token.
   *
   * @apiExample {curl} Example usage:
   *     curl -i https://localhost:3000/api/v1/programs/4711?year=2016
   *
   * @apiSuccess {String} lastDataUpdate   The last date when the data were loaded.
   * @apiSuccess {Integer} id   The id of the Program.
   * @apiSuccess {String} name  The name of the Program.
   * @apiSuccess {String} desc  The description of the Program.
   * @apiSuccess {Integer} plan  The id of the Curriculum.
   * @apiSuccess {Integer} year  The year of the Curriculum.
   * @apiSuccess {[Term]} terms  An array of Terms
   * @apiSuccess {Integer} term[position]  The position of the Term.
   * @apiSuccess {String} term[name]  The name of the Term.
   * @apiSuccess {[String]} term[tags]  An Array of tags for the Term.
   * @apiSuccess {[Course]} term[courses]  An array of Courses.
   * @apiSuccess {String} course[code]  The code of the Course.
   * @apiSuccess {String} course[name]  The name of the Course.
   * @apiSuccess {String} course[desc]  The description of the Course.
   * @apiSuccess {String} course[area]  The area of the Course.
   * @apiSuccess {[String]} course[tags]  An Array of tags for the Course.
   * @apiSuccess {[String]} course[requisites]  An Array of requisites for the Course.
   * @apiSuccess {Integer} course[credits]  The credits of the Course.
   *
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *        "lastDataUpdate": "2018-01-12T05:00:00.000Z",
   *        "id": "4711",
   *        "name": "Ingeniería Civil en Informática",
   *        "desc": "Description",
   *        "plan": "3",
   *        "year": "2016",
   *        "tags": [
   *            "Tag1",
   *            "Tag2"
   *        ],
   *        "terms": [
   *            {
   *                "position": "1",
   *                "name": "1 Término",
   *                "tags": [
   *                    "Tag1",
   *                    "Tag2"
   *                ],
   *                "courses": [
   *                    {
   *                        "code": "BAIN037",
   *                        "name": "Cálculo en una Variable",
   *                        "desc": "Descr",
   *                        "tags": [
   *                            "Tag1",
   *                            "Tag2"
   *                        ],
   *                        "requisites": [
   *                            "Álgebra para Ingeniería"
   *                        ],
   *                        "credits": 6
   *                    },
   *                    {
   *                        "code": "BAIN019",
   *                        "name": "Química para Ingeniería",
   *                        "desc": "Descr",
   *                        "tags": [
   *                            "Tag1",
   *                            "Tag2"
   *                        ],
   *                        "requisites": [],
   *                        "credits": 2
   *                    }
   *                ]
   *            },
   *            {
   *                "position": "2",
   *                "name": "2  Término",
   *                "tags": [
   *                    "Tag1",
   *                    "Tag2"
   *                ],
   *                "courses": [
   *                    {
   *                        "code": "DYRE060",
   *                        "name": "Taller de Ingeniería: Programación Aplicada",
   *                        "desc": "Descr",
   *                        "tags": [
   *                            "Tag1",
   *                            "Tag2"
   *                        ],
   *                        "requisites": [
   *                            "Álgebra para Ingeniería",
   *                            "Geometría para Ingeniería",
   *                            "Taller de Ingeniería I"
   *                        ],
   *                        "credits": 3
   *                    },
   *                    {
   *                        "code": "BAIN039",
   *                        "name": "Comunicación Idioma Inglés",
   *                        "desc": "Descr",
   *                        "tags": [
   *                            "Tag1",
   *                            "Tag2"
   *                        ],
   *                        "requisites": [
   *                            "AA"
   *                        ],
   *                        "credits": 5
   *                    }
   *                ]
   *            },
   *            {
   *                "position": "3",
   *                "name": "3 Término",
   *                "tags": [],
   *                "courses": []
   *            }
   *        ]
   *    }
   *
   * @apiError InvalidParameter The <code>id</code> or <code>year</code> are not Number.
   * @apiError MissingParameter The <code>year</code> is missing.
   * @apiError ProgramNotFound The <code>id</code> or the <code>year</code> of the Program was not found.
   * @apiError NoAccessRight The <code>id</code> of the User was not found.
   *
   * @apiErrorExample {json} Error-InvalidParameter-Response:
   *     HTTP/1.1 400 Bad Request
   *     {
   *        "errors": [
   *           {
   *                "status": 422,
   *                "error": "InvalidParameter",
   *                "detail": "params[id]: Must be an Integer"
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-MissingParameter-Response:
   *     HTTP/1.1 400 Bad Request
   *     {
   *        "errors": [
   *           {
   *                "status": 422,
   *                "error": "MissingParameter",
   *                "detail": "body[year]: Must be provided"
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-ProgramNotFound-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *        "errors": [
   *           {
   *                "status": 404,
   *                "error": "ProgramNotFound",
   *                "detail": "The Program wasn't found."
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-NoAccessRight-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "error": "NoAccessRight"
   *     }
   */

    API.get('/programs/:id', oauth2Authentication ,[

      check('id',{type:errorsTypes.INVALID_PARAMETER,msg:'Must be an Integer'}).isInt(),

      check('year',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),

      check('year',{type:errorsTypes.INVALID_PARAMETER,msg:'Must be an Integer'}).isInt()

    ],function(req,res,next){

      const errors = validationResult(req).formatWith(errorFormatter);

      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{

        const processRequest = async()=>{

          try {

            // Get the curriculum and the program object
            const data = await crudOperations.getProgram(req.params.id,req.query.year);

            // If the data is not found
            if(!data)
              res.status(400).json({errors:[{status:404,error:"ProgramNotFound",detail:"The Program wasn't found."}]});

            else{

              // Get the terms along with the courses
              const terms = await crudOperations.getProgramTerms(data.id);
              const loadingDate = await crudOperations.getLoadingDate();
              //res.json(terms);
              res.json(createResponseProgram(data,terms,loadingDate));
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequest();

      };
    });

    /**
    * Summary. Create a response JSON.
    *
    * Description. Create a response JSON with data of a Curriculum, Program,
    * ProgramTerms and Courses
    *
    * @param {Object}   program       An Object with information about the Program and the Curriculum.
    * @param {Object}   terms         An Array with information about the ProgramTerms and the Courses.
    *
    * @return {Object} The response object.
    */

    const createResponseProgram = (program,terms,loadingDate) =>{
      const returnValue = {
        lastDataUpdate: loadingDate,
        id:   program.program.id,
        name: program.program.name,
        desc: program.program.desc,
        plan: program.id,
        year: program.year,
        tags: program.tags ? program.tags.split(',') : [],
      };
      if( terms.length!==0 ){
        const returnTerms = terms.map(term => {
          return {
            position:   term.position,
            name:       term.name,
            tags:       term.tags ? term.tags.split(',') : [],
            courses:    term.program_courses.map( programCourse =>{
              return {
                code:       programCourse.course.code,
                name:       programCourse.course.name,
                desc:       programCourse.course.desc,
                area:       programCourse.area,
                tags:       programCourse.course.tags ? programCourse.course.tags.split(',') : [],
                requisites: programCourse.requisites ? programCourse.requisites.split('\n') : [],
                credits:    programCourse.course.credits
              }
            })
          };
        });

        terms = returnTerms;
      }
      returnValue.terms = terms;

      return returnValue;
    }

}
