const { check, validationResult, oneOf } = require('express-validator/check');
const { matchedData, sanitize } = require('express-validator/filter');
const d3 = require("d3");

module.exports = function(API , crudOperations , db , oauth2Authentication){

  const errorsTypes = { INVALID_PARAMETER : 'InvalidParameter',MISSING_PARAMETER : 'MissingParameter'};

  const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
    return {type:msg.type,msg:`${location}[${param}]: ${msg.msg}`};
  };

  /**
   * @api {get} /students/:id Request Student academic information from a specific program
   * @apiVersion 1.0.0
   * @apiName GetStudent
   * @apiGroup Student
   *
   * @apiParam {String} id Student unique ID.
   * @apiParam {Integer} program  Program unique ID
   *
   * @apiHeader {String} access-token Unique access-token.
   *
   * @apiExample {curl} Example usage:
   *     curl -i https://localhost:3000/api/v1/students/00918df7-5843-c53e-eec9-1595af6fdcd9?program=223
   *
   * @apiSuccess {String} lastDataUpdate   The last date when the data were loaded.
   * @apiSuccess {String} id   The id of the Student.
   * @apiSuccess {String} program  The name of the Program.
   * @apiSuccess {Integer} cohortYear  The cohort year of the student
   * @apiSuccess {Integer} plan  The id of the last Curriculum of the Student in a Program.
   * @apiSuccess {Integer} planYear  The year version of the last Curriculum of the Student in a Program.
   * @apiSuccess {[Curriculum]} previousPlans  An array with information (id and year) about previous curriculums in a program of the student.
   * @apiSuccess {Double} bachCompletedCourses Data related with the student's dropout
   * @apiSuccess {Double} bachTotalCourses  Data related with the student's dropout
   * @apiSuccess {Double} estimatedTermsToCompleteBach Data related with the student's dropout
   * @apiSuccess {Double} estimatedProbability Data related with the student's dropout
   * @apiSuccess {[Term]} terms  An array of Term academics for a student.
   * @apiSuccess {Integer} term[year]  The year of the Term.
   * @apiSuccess {String} term[semester]  The semester of the Term.
   * @apiSuccess {Double} term[termAvg]  An statistic of the Term.
   * @apiSuccess {Double} term[accAvg]  An statistic of the Term.
   * @apiSuccess {[Course]} term[courses]  An Array of Courses taken by the Student in the Term.
   * @apiSuccess {String} course[code]  The code of the Course.
   * @apiSuccess {Double} course[grade]  The grade get it by the Student.
   * @apiSuccess {String} course[registration]  The registration of the Course. (cursada, homologada, convalidada, anulada)
   * @apiSuccess {String} course[status]  The status of the Course. (Aprobada, Reprobada)
   * @apiSuccess {StatisticByGroup} course[historicGroup]  Some statistics about a course in general.
   * @apiSuccess {StatisticByCohort} course[cohortGroup]  Some statistics about a course in the specific cohort of the student.
   * @apiSuccess {StatisticByClass} course[classGroup]  Some statistics about a course in a specific term.
   *
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *     {
    "lastDataUpdate": "2018-01-12T05:00:00.000Z"
    "id": "00918df7-5843-c53e-eec9-1595af6fdcd9",
    "program": "Ingeniería Civil",
    "cohortYear": 2016,
    "plan": "2864",
    "planYear": 2015,
    "previousPlans": [],
    "bachCompletedCourses": 0.0,
    "bachTotalCourses": 0.0,
    "estimatedTermsToCompleteBach": 0.0,
    "estimatedProbability": 0.0,
    "terms": [
        {
            "year": 2015,
            "semester": "1S",
            "termAvg": null,
            "accAvg": null,
            "coursesTaken": [
                {
                    "code": "ICF01107",
                    "name": "LABORATORIO DE FÍSICA A",
                    "grade": 7.8,
                    "registration": null,
                    "state": "AP",
                    "historicGroup": {
                        "course_code": "ICF01107",
                        "distribution": [
                            {
                                "label": "label1",
                                "value": 2
                            },
                            {
                                "label": "label2",
                                "value": 1
                            },
                            {
                                "label": "label3",
                                "value": 2
                            }
                        ],
                        "dist_range": {
                            "max": 3,
                            "min": 2
                        },
                        "student_count": 2,
                        "fail_rate": 23,
                        "drop_rate": null,
                        "count_by_delay": null
                    },
                    "cohortGroup": {
                        "course_id": "368914",
                        "year": 2015,
                        "distribution": [
                            {
                                "label": "label1",
                                "value": 5
                            },
                            {
                                "label": "label2",
                                "value": 7
                            },
                            {
                                "label": "label3",
                                "value": 3
                            }
                        ],
                        "dist_range": {
                            "min": 2,
                            "max": 3
                        },
                        "student_count": 3,
                        "fail_rate": 18,
                        "drop_rate": 20,
                        "count_by_delay": null
                    },
                    "classGroup": {
                        "distribution": [
                            {
                                "label": "label1",
                                "value": 2
                            },
                            {
                                "label": "label2",
                                "value": 3
                            },
                            {
                                "label": "label3",
                                "value": 5
                            },
                            {
                                "label": "label3",
                                "value": 40
                            },
                            {
                                "label": "label4",
                                "value": 0
                            },
                            {
                                "label": "label5",
                                "value": 0
                            }
                        ],
                        "dist_range": {
                            "min": 1,
                            "max": 7
                        },
                        "student_count": 50,
                        "fail_rate": 0.2,
                        "drop_rate": 0.24,
                        "count_by_delay": {},
                        "course_code": "ICF01107",
                        "year": 2011,
                        "semester": "1S"
                    }
                }
            ]
        },
        {
            "year": 2015,
            "semester": "2S",
            "termAvg": null,
            "accAvg": null,
            "coursesTaken": [
                {
                    "code": "ICF01123",
                    "name": "LABORATORIO DE FÍSICA B",
                    "grade": 8.7,
                    "registration": null,
                    "state": "AP",
                    "historicGroup": {
                        "course_code": "ICF01123",
                        "distribution": null,
                        "dist_range": null,
                        "student_count": null,
                        "fail_rate": null,
                        "drop_rate": null,
                        "count_by_delay": null
                    },
                    "cohortGroup": {
                        "course_id": "361992",
                        "year": 2015,
                        "distribution": null,
                        "dist_range": {
                            "min": 2,
                            "max": 3
                        },
                        "student_count": 4,
                        "fail_rate": 44,
                        "drop_rate": 22,
                        "count_by_delay": null
                    },
                    "classGroup":null
                },
                {
                    "code": "ICF01115",
                    "name": "FÍSICA B",
                    "grade": 7.45,
                    "registration": null,
                    "state": "AP",
                    "group": null,
                    "historicGroup": {
                        "course_id": "361995",
                        "year": 2015,
                        "distribution": null,
                        "dist_range": null,
                        "student_count": null,
                        "fail_rate": null,
                        "drop_rate": null,
                        "count_by_delay": null
                    },
                    "classGroup":null
                }
            ]
        }
    ]
}
   *
   * @apiError InvalidParameter The <code>program</code> is not Number.
   * @apiError MissingParameter The <code>program</code> is missing.
   * @apiError ProgramNotFound The <code>id</code> of the Student was not found.
   * @apiError NoAccessRight The <code>id</code> of the User was not found.
   *
   * @apiErrorExample {json} Error-InvalidParameter-Response:
   *     HTTP/1.1 400 Bad Request
   *     {
   *        "errors": [
   *           {
   *                "status": 422,
   *                "error": "InvalidParameter",
   *                "detail": "params[program]: Must be an Integer"
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-MissingParameter-Response:
   *     HTTP/1.1 400 Bad Request
   *     {
   *        "errors": [
   *           {
   *                "status": 422,
   *                "error": "MissingParameter",
   *                "detail": "body[program]: Must be provided"
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-StudentNotFound-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *        "errors": [
   *           {
   *                "status": 404,
   *                "error": "StudentNotFound",
   *                "detail": "The Student wasn't found."
   *            }
   *        ]
   *     }
   *
   * @apiErrorExample {json} Error-NoAccessRight-Response:
   *     HTTP/1.1 404 Not Found
   *     {
   *       "error": "NoAccessRight"
   *     }
   */
   


    API.get('/students/:id', oauth2Authentication,[

      sanitize('id').trim().escape(),

      check('program',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),

      check('program',{type:errorsTypes.INVALID_PARAMETER,msg:'Must be an Integer'}).isInt()

    ],function(req,res,next){

      const errors = validationResult(req).formatWith(errorFormatter);

      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{

        const processRequest = async()=>{

          try {

            // Get the curriculum and the program object
            const data = await crudOperations.getStudentsAcademics(req.params.id,req.query.program);
            // If the data is not found
            if(!data)
              res.status(400).json({errors:[{status:404,error:"StudentNotFound",detail:"The Student wasn't found."}]});

            else{

              const curriculum_id = data.student_curriculums.length !== 0 ? data.student_curriculums[0].curriculum.id : -1;
              const cohortYear = data.student_curriculums.length !== 0 ? data.student_curriculums[0].year : -1;
              const historyAcademics = curriculum_id !== -1 && cohortYear !== -1 ? await crudOperations.getHistoryAcademicStudentByCurriculum(curriculum_id,req.params.id,cohortYear) : [] ;
              const loadingDate = await crudOperations.getLoadingDate();
              //res.json(terms);
              res.json(createStudentAcademicResponse(data,historyAcademics,loadingDate));
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequest();

      };
    });

    /**
    * Summary. Create a response JSON.
    *
    * Description. Create a response JSON with data of a student, Program,
    * ProgramTerms and Courses
    *
    * @param {Object}   program          An Object with information about the Program and the Curriculum.
    * @param {Object}   historyAcademics An Array with information about the history academic of the student.
    *
    * @return {Object} The response object.
    */

    const createStudentAcademicResponse = (student,historyAcademics,loadingDate) =>{

      const returnValue = {
        lastDataUpdate: loadingDate,
        id:             student.id,
        program:        student.student_curriculums.length !== 0 ? student.student_curriculums[0].curriculum.program.name : null,
        cohortYear:     student.student_curriculums.length !== 0 ? student.student_curriculums[0].year : null,
        plan:           student.student_curriculums.length !== 0 ? student.student_curriculums[0].curriculum.id : null,
        planYear:       student.student_curriculums.length !== 0 ? student.student_curriculums[0].curriculum.year : null,
        previousPlans : student.student_curriculums && student.student_curriculums.length === 0 ? [] :
                        // Because the array is sort, just return the previous Curricuums
                        student.student_curriculums.slice(1)// [2016,2015,2014] => [2015,2014]
                        // Just return the id and the year the previous Curriculums
                        .map(studentPlan => {
                              return {
                                id:   studentPlan.curriculum.id,
                                year: studentPlan.curriculum.year
                              }
                          }),
         bachCompletedCourses :         student.student_dropout ? student.student_dropout.bach_completed_courses :null,
         bachTotalCourses :             student.student_dropout ? student.student_dropout.bach_total_courses : null,
         estimatedTermsToCompleteBach : student.student_dropout ? student.student_dropout.estimated_terms_to_complete_bach : null,
         estimatedProbability   :       student.student_dropout ? student.student_dropout.estimated_probability: null,

      };

      // Process to nest the history academic data in terms
      let termsParsed = [];
      let nestedTerms = d3.nest().key(function(d) { return d.year; }).key(function(d) { return d.semester; }).entries(historyAcademics);

      Object.keys(nestedTerms).forEach(function(k){
        let entries = nestedTerms[k].values;
        Object.keys(entries).forEach(function(k){
          termsParsed.push(entries[k].values);
        });
      });

      let terms = termsParsed.map(function(history_academic_courses){
        return {
          year:         history_academic_courses[0].year,
          semester:     history_academic_courses[0].semester,
          termAvg:      history_academic_courses[0].statistic_student_term ? history_academic_courses[0].statistic_student_term.termAvg : null,
          accAvg:       history_academic_courses[0].statistic_student_term ? history_academic_courses[0].statistic_student_term.accAvg : null,
          coursesTaken: history_academic_courses.map(history_academic_course => {
            return {
              code:           history_academic_course.course.code,
              name:           history_academic_course.course.name,
              grade:          history_academic_course.grade,
              registration:   history_academic_course.registration,
              state:          history_academic_course.state,
              cohortGroup:    history_academic_course.course.statisticCohorts.length !== 0 ? history_academic_course.course.statisticCohorts[0] : null,
              classGroup:     history_academic_course.course.statisticTerms.length !== 0 ? history_academic_course.course.statisticTerms[0] : null,
              historicGroup:  history_academic_course.course.statistic
            }
          })
        };
      });

      returnValue.terms = terms;

      return returnValue;
    }





    /**
    * WebService para obtener el listado de todos los estudiantes y devolver el primero en formato json.
    */

    API.get('/students/', oauth2Authentication,function(req, res, next){
    const errors = validationResult(req).formatWith(errorFormatter);

      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{

        const processRequest2 = async()=>{

          try {

            // Get the curriculum and the program object
            const data = await crudOperations.getAllStudents();
            // If the data is not found
            if(!data)
              res.status(400).json({errors:[{status:404,error:"StudentNotFound",detail:"No hay estudiantes."}]});

            else{
              console.log(data[0].id);
              const student_id = data.length !== 0 ? data[0].id : -1;
              const name = data.length !== 0 ? data[0].name : -1;
              const state = data.length !==0 ? data[0].state : -1;
              
              //res.json(terms);
              res.json(createAllStudentsResponse(data[0]));
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequest2();
      };

   });



    const createAllStudentsResponse = (student) =>{

      const returnValue = {
        id:             student.id,
        name:           student.name,
        state:          student.state,
      };

      return returnValue;
    }

    /*const obtiene= async(id,res)=>{
      try{
        const dataDos = await crudOperations.getCorsesAndProgramCourse(id);
        // If the data is not found
        if(!dataDos){console.log("errores en data dos "+dataDos); return null;}
        //  res.status(400).json({errors:[{status:404,error:"StudentNotFound",detail:"No hay estudiantes."}]});

        else{
          //dat=[];
          dataDos.map(program_course=>{
            console.log("+++++++++++++++++++++++++++++++++++ "+program_course.id+" "+program_course.position);
            
          });
          return dataDos;
        }
      }catch (e) {
        console.log(e);
        //res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
        return null;
      }
    }*/

    /**
     * Web Service para obtener el historial academico del estudiante
     */
    API.get('/getStudentAcademics/', oauth2Authentication, [
      check('studentid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),

      check('curriculumid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists()

    ],function(req, res, next){

      const errors = validationResult(req).formatWith(errorFormatter);
      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{

        const processRequest = async()=>{

          try {

            // Get the curriculum and the program object
            const data = await crudOperations.getHistoryAcademicStudentByCurriculumId(parseInt(req.query.studentid),parseInt(req.query.curriculumid));
            // If the data is not found
            if(!data)
              res.status(400).json({errors:[{status:404,error:"StudentNotFound",detail:"The Student wasn't found."}]});

            else{

              data.map(history => {
                let date = new Date(Date.parse(history.term.start_Date));
                const month = date.toLocaleString('es-EC', { month: 'long' });
                console.log(history.registration+" "+history.grade+" "+history.group+" "+history.state+" "+date.getMonth()+" "+month+" "+date.getFullYear());
              });
              //res.json(terms);
              res.json(data);
              //res.json(createStudentAcademicResponse(data,historyAcademics,loadingDate));
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequest();

      };

    });
    /**
    * WebService para obtener el listado de todos los estudiantes y devolver el primero en formato json.
    */

   API.get('/getStudentCurriculum/', oauth2Authentication,[
    check('studentid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),
   ],function(req, res, next){
    const errors = validationResult(req).formatWith(errorFormatter);

      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{
        
          
        
        const processRequestTest = async()=>{

          try {

            // Get the curriculum and the program object
            const data = await crudOperations.getCurriculum(req.query.studentid);
            
           /* const dataDos = await crudOperations.getCorsesAndProgramCourse(1);
            if(!dataDos){
              console.log("este es el data dos undefined "+dataDos);
            }else{
              dataDos.map(d=>{
                console.log("ESTOS SON LOS DATOS NECESARIOS: "+d.id+" "+d.position+" "+d.dependents);
              });
            }*/
            // If the data is not found
            if(!data)
              res.status(400).json({errors:[{status:404,error:"StudentNotFound",detail:"No hay estudiantes."}]});

            else{
              console.log(data.length);
              let tam=data.length;
              const student_id = data.length !== 0 ? data[0].id : -1;
              const name = data.length !== 0 ? data[0].name : -1;
              const state = data.length !==0 ? data[0].state : -1;
              let i=0;
              let student=[];
              //mapeo 
              /*data.map(studen=>{
                studen.student_curriculums.map(es=>{
                  es.curriculum.program_terms.map(async(te)=>{
                    try{
                    const dataDos = await crudOperations.getCorsesAndProgramCourse(te.id);
                    dataDos.map(da=>{
                      console.log("Este es el data mapeado: "+da.id+" "+da.position);
                    });
                  }catch(e){
                    console.log(e);
                    res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
                  }
                  });
                });
              });*/
              for(i=0;i<tam;i++){
                console.log("este es el correo "+data[i].email);
                console.log(data[i].anonid+" "+data[i].name+" "+data[i].state+" "+data[i].student_curriculums.length);
                let curriculums = data[i].student_curriculums;
                 student[i] = {//se le pone como vector [i] para q en cad aposicion guarde un registro, asi no se pierden registros
                  id: data[i].anonid,
                  name: data[i].name,
                  state: data[i].state,
                  email: data[i].email 
                }
                //for que imprime los curriculums
                let student_curriculum=[];
                let j=0;
                for(j=0;j<curriculums.length;j++){
                  console.log("*********** "+curriculums[j].id+" "+curriculums[j].semester+" "+curriculums[j]+" "+curriculums[j].curriculum);
                  student_curriculum[j] = {//en cad aposicion j se va a guardar un student curriculum junto con los arrays que le pertenezcan.
                    id: curriculums[j].id,
                    year: curriculums[j].year,
                    semester: curriculums[j].semester,
                    n_courses_in_program: curriculums[j].n_courses_in_program,
                    n_taken_courses: curriculums[j].n_taken_courses,
                    n_approved_courses: curriculums[j].n_approved_courses,
                    n_failed_courses: curriculums[j].n_failed_courses,
                    n_retaken_courses: curriculums[j].n_retaken_courses,
                    average_grade: curriculums[j].average_grade,
                    last_workload: curriculums[j].last_workload,
                    last_success_rate: curriculums[j].last_success_rate,
                    n_current_courses: curriculums[j].n_current_courses,
                    state: curriculums[j].state
                  }

                  //curriculum
                  //aqui seria otro for para recorrer los curriculums aunque de hecho solo puede tener un solo curriculum un student curriculum
                  //es decir, la relacion es que un curriculum puede tener varios student_curriculum y no alreves, asi que seria un solo curriculum para 
                  //uno o mas student curriculum.
                  console.log("----------- "+curriculums[j].curriculum.id+" "+curriculums[j].curriculum.year+" "+curriculums[j].curriculum.semester);
                  let curriculum = {
                    id: curriculums[j].curriculum.id,
                    state: curriculums[j].curriculum.state,
                    year: curriculums[j].curriculum.year,
                    semester: curriculums[j].curriculum.semester
                  }
                  //student_curriculum.curriculum = curriculum;
                  //program
                  //un solo programa porque un curriculum debe pertenecer tan solo a un programa, mientras que un programa puede tener uno o varios curriculums
                  console.log("=========== "+curriculums[j].curriculum.program.name);
                  let program = {
                    id: curriculums[j].curriculum.program.id,
                    name: curriculums[j].curriculum.program.name
                  }
                  curriculum.program = program;
                  // if(!curriculum.program){
                  //   curriculum.program = program;
                  // }else{
                  //   curriculum.program.push(program);
                  // }
                  //curriculum.program += program;
                  console.log(JSON.stringify(curriculum));
                  //program terms
                  let program_terms=curriculums[j].curriculum.program_terms;
                  let program_term = [];
                  
                  program_term.program_course = Object.create(null);
                  let h=0;
                  for(h=0;h<program_terms.length;h++){
                    console.log("______________ "+program_terms[h].name+" "+program_terms[h].year+" "+program_terms[h].position+" "+program_terms[h].curriculum_id+" "+program_terms[h].position+" "+program_terms[h].tags+" "+program_terms[h].program_courses.length+" "+program_terms[h].program_courses[0].id+" "+program_terms[h].program_courses[0].position);
                    //para el json

                    program_term [h]= {
                      id: program_terms[h].id,
                      year: program_terms[h].year,
                      name: program_terms[h].name,
                      position: program_terms[h].position
                    }
                    const dataDos = await crudOperations.getCorsesAndProgramCourse(program_terms[h].id);
                    if(!dataDos){
                      console.log("este es el data dos "+dataDos);
                    }else{
                      const program_course=dataDos.map(d=>{
                        console.log("ESTOS SON LOS DATOS NECESARIOS: "+d.id+" "+d.course.name+" "+d.position+" "+d.dependents);
                        const retorna = {
                          course_id: d.course.id,
                          course_nombre: d.course.name,
                          course_codigo: d.course.code,
                          course_estado: d.course.state,
                          course_creditos: d.course.credits,
                          program_course_id: d.id,
                          program_term_id: d.program_term_id,
                          area: d.area,
                          position: d.position,
                          mention: d.mention,
                          requisites: d.requisites,
                          dependents: d.dependents,
                          theoretical_hours: d.theoretical_hours,
                          practical_hours: d.practical_hours,
                          autonomous_hours: d.autonomous_hours,
                          complexity: d.complexity,
                          avg_last_semester: d.avg_last_semester,
                          success_rate_last_semester: d.success_rate_last_semester,
                          blocked: d.blocked,
                          program_course_state: d.state
                        }
                        
                        return retorna;
                        //creo el json
                      });
                      program_term[h].program_course = program_course;
                      // if(!program_term.program_course){
                      //   console.log("entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+program_term.program_course);
                      //   program_term.program_course = program_course;
                      // }else{
                        
                      //   program_term.program_course.push(program_course);
                      //   console.log("entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa2 "+program_term.program_course.length);
                      // }
                      //program_term.program_course += program_course;
                    }
                  }
                  curriculum.program_term = program_term;
                  // if(!curriculum.program_term){
                  //   curriculum.program_term = program_term;
                  // }else{
                  //   curriculum.program_term.push(program_term);
                  // }
                  //curriculum.program_term += program_term;
                  student_curriculum[j].curriculum = curriculum;
                  // if(!student_curriculum.curriculum){
                  //   student_curriculum.curriculum = curriculum;
                  // }else{
                  //   student_curriculum.curriculum.push(curriculum);
                  // }
                  
                  // test=curriculum;
                }
                student[i].student_curriculum = student_curriculum;
                //console.log("Este es el student curriculum "+JSON.stringify(student_curriculum));
                // if(!student.student_curriculum){
                //   student.student_curriculum = student_curriculum;  
                // }else{
                //   student.student_curriculum.push(student_curriculum);
                // }
                
                /*let dataDos = await crudOperations.getCorsesAndProgramCourse(1);
                console.log(dataDos.length);
                dataDos = await crudOperations.getCorsesAndProgramCourse(2);
                console.log(dataDos.length);
                dataDos = await crudOperations.getCorsesAndProgramCourse(5);
                console.log(dataDos.length);*/
              }
              console.log(student.length);
              let a=0;
              for(a=0;a<student.length;a++){
                console.log(student[a].name);
              }
             /* data.map(stuiante=>{
                  //console.log(stuiante.id+" "+stuiante.name+" "+stuiante.state+" "+stuiante.history_academic_courses.length);
                  stuiante.student_curriculums.map(student_cur=>{
                      //console.log("*********** "+student_cur.id+" "+student_cur.semester+" "+student_cur.state+" "+student_cur.curriculum);

                      //curriculum
                      //cuando se hace un select desde el detalle al padre ya no se aumenta la "s" al final del nombre de la tabla como ene l casd del detalle
                      //ejm: student_curriculumS, en este caso desde student_curriculum hacia su padre va sin s, pues es solo un registro dienio del detalle

                      //console.log("----------- "+student_cur.curriculum.id+" "+student_cur.curriculum.year+" "+student_cur.curriculum.semester);

                      //program
                      //console.log("=========== "+student_cur.curriculum.program.name);

                      //program_term
                      student_cur.curriculum.program_terms.map(pt=>{
                          //console.log("______________ "+pt.name+" "+pt.year+" "+pt.position+" "+pt.curriculum_id+" "+pt.position+" "+pt.tags+" "+pt.program_courses.length+" "+pt.program_courses[0].id+" "+pt.program_courses[0].position);
                          // Get the curriculum and the program object
                          const dataDos=obtiene(pt.id,res);
                          console.log("Este es el tamano del data dos: "+dataDos.length);
                          
                          //program course
                        //   pt.program_courses.map(pc=>{
                        //       console.log("xxxxxxxxxxxxxx "+pc.id+" "+pc.autonomous_hours+" "+pc.requisites+" "+pc.dependents+" ");
                        //   });
                      });
                    //   student_cur.curriculum.map(cur=>{
                    //       console.log(cur.id+" "+cur.year+" "+cur.semester);
                    //   });
                  });
                //   stuiante.history_academic_courses.map(academic=>{
                //       console.log("------------ "+academic.id+" "+academic.registration+" "+academic.grade+" "+academic.code_valida);
                //   });
              });
              console.log("este es el dattaaaaaaa: "+data[0].id+" "+data[0].year+" "+data[0].name+" "+data.length);*/
              //res.json(terms);
              //res.json(createAllTestStudentsResponse(data[0]));
              //res.json("retornando respuesta");
             // student.test = test;
              //console.log(JSON.parse(JSON.parse(jsons)[0].student_curriculum));
              //console.log(JSON.parse(json)[0].student_curriculum[0].curriculum.program_term[0].program_course);
             //console.log("Esta es el respuesta \n "+JSON.stringify(student));
             // res.json(JSON.stringify(student));
             res.json(student);
             //trabajo futuro no se necesita hacer el proceso anterior,s olo mandar directo a convertir data en json (el inconveniente eds q en progrma_course no sale las palabras completas de los campos, es decir, el nombre de las columnas)
             //res.json(data);
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequestTest();
      };

   });



    const createAlltestStudentsResponse = (student) =>{

      const returnValue = {
        id:             student.id,
        name:           student.name,
        state:          student.state,

      };
      if( student.length!==0 ){
        const returnTerms = terms.map(term => {
          return {
            position:   term.position,
            name:       term.name,
            tags:       term.tags ? term.tags.split(',') : [],
            courses:    term.program_courses.map( programCourse =>{
              return {
                code:       programCourse.course.code,
                name:       programCourse.course.name,
                desc:       programCourse.course.desc,
                area:       programCourse.area,
                tags:       programCourse.course.tags ? programCourse.course.tags.split(',') : [],
                requisites: programCourse.requisites ? programCourse.requisites.split('\n') : [],
                credits:    programCourse.course.credits
              }
            })
          };
        });

        terms = returnTerms;
      }

      return returnValue;
    }

    /**
     * Web Service para obtener todos los companeros de aula de un estudiante dado en el mismo ciclo lectivo
     */
    API.get('/getAveragePartners/', oauth2Authentication, [
      check('studentid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),
      check('curriculumid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),
      check('courseid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),
      check('termid',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists(),
      check('group',{type:errorsTypes.MISSING_PARAMETER,msg:'Must be provided'}).exists()

      

    ],function(req, res, next){

      const errors = validationResult(req).formatWith(errorFormatter);
      if (!errors.isEmpty()){
        res.status(400).json({errors:errors.array().map(function(error){return {status:422,error:error.type,detail:error.msg}})});
      }

      else{

        const processRequest = async()=>{

          try {
            let concatenado = [];
            // Get the students and history_academics of they.
            const data = await crudOperations.getPartnersOfCourse(parseInt(req.query.curriculumid),parseInt(req.query.courseid),parseInt(req.query.termid),req.query.group);
            // If the data is not found
            if(!data){
              //res.status(400).json({errors:[{status:404,error:"StudentsNotFound",detail:"The Students weren't found in Aprobed courses.."}]});
              //llamo a la fincion que estrae los estudiantes companeros de la misma aula pero que reprobaron el curso
              //mando un array vacio en caso de que no haya datos de estudientes que aprobaron
              const dataLost = await crudOperations.getPartnersOfCourseReprobados(parseInt(req.query.curriculumid),parseInt(req.query.courseid),parseInt(req.query.termid),req.query.group, []);
              if(!dataLost){
                res.status(400).json({errors:[{status:404,error:"StudentsNotFound",detail:"The Students weren't found in Reprobed or Aprobed courses."}]});
                
              }else{
                console.log("==============================STUDENTS REPROBED==============================");
                //anado el array de los perdidos al array de los aprobados, es solo para tener un solo array de todos los estudiantes que estubieron en el mismo 
                //cuso que el estudiante dado
                
                dataLost.map(student => {
                  console.log(student.name+" "+student.history_academic_courses.length+" "+student.history_academic_courses[0].course_id+" "+student.history_academic_courses[0].group+" "+student.history_academic_courses[0].state);  
                });
                concatenado=dataLost;

                res.json(returnStudentPercentagesByGrades(concatenado, parseInt(req.query.studentid)));
              }
            }
            else{
              let studentsIds = [];
              
              data.map(student => {
                console.log(student.name+" "+student.history_academic_courses.length+" "+student.history_academic_courses[0].course_id+" "+student.history_academic_courses[0].group+" "+student.history_academic_courses[0].state);
                studentsIds.push(parseInt(student.anonid));
              });
              console.log(studentsIds.length+" " +studentsIds);
              //llamo a la fincion que estrae los estudiantes companeros de la misma aula pero que reprobaron el curso
              const dataLost = await crudOperations.getPartnersOfCourseReprobados(parseInt(req.query.curriculumid),parseInt(req.query.courseid),parseInt(req.query.termid),req.query.group, studentsIds);
              if(!dataLost){
                //res.status(400).json({errors:[{status:404,error:"StudentsNotFound",detail:"The Students weren't found in Reprobed courses."}]});
                concatenado=data;
                //res.json(concatenado);
              }else{
                console.log("==============================STUDENTS REPROBED==============================");
                //anado el array de los perdidos al array de los aprobados, es solo para tener un solo array de todos los estudiantes que estubieron en el mismo 
                //cuso que el estudiante dado
                
                dataLost.map(student => {
                  console.log(student.name+" "+student.history_academic_courses.length+" "+student.history_academic_courses[0].course_id+" "+student.history_academic_courses[0].group+" "+student.history_academic_courses[0].state);  
                });
                concatenado = data.concat(dataLost);
              }
              //res.json(terms);
              console.log("Lo que busco : ");
              console.log(returnStudentPercentagesByGrades(concatenado, parseInt(req.query.studentid)));
              
              res.json(returnStudentPercentagesByGrades(concatenado, parseInt(req.query.studentid)));
              //res.json(createStudentAcademicResponse(data,historyAcademics,loadingDate));
            }
          }
          catch (e) {
            console.log(e);
            res.status(500).json({errors:[{status:500,error:"InternalServerError"}]});
          }
        }

        processRequest();

      };

    });

    const returnStudentPercentagesByGrades = (data, student_id) =>{
      let studentsids = [0,0,0,0,0,0,0,0,0,0];
      const numEstudiantes = data.length;
      let posStudent=0; //posicion en la que se encuentra el estudiante
      data.map(student => {
        let studentGrade = Math.round(parseFloat(student.history_academic_courses[0].grade));
        let studentid=student.anonid;
        console.log(studentid+" "+student_id);
        if(studentGrade >= 0 && studentGrade < 10){
          studentsids[0]+=1;
          if(student_id==studentid)
            posStudent=0;
        }else if(studentGrade >= 10 && studentGrade < 20){
          studentsids[1]+=1;
          if(student_id==studentid)
            posStudent=1;
        }else if(studentGrade >= 20 && studentGrade < 30){
          studentsids[2]+=1;
          if(student_id==studentid)
            posStudent=2;
        }else if(studentGrade >= 30 && studentGrade < 40){
          studentsids[3]+=1;
          if(student_id==studentid)
            posStudent=3;
        }else if(studentGrade >= 40 && studentGrade < 50){
          studentsids[4]+=1;
          if(student_id==studentid)
            posStudent=4;
        }else if(studentGrade >= 50 && studentGrade < 60){
          studentsids[5]+=1;
          if(student_id==studentid)
            posStudent=5;
        }else if(studentGrade >= 60 && studentGrade < 70){
          studentsids[6]+=1;
          if(student_id==studentid)
            posStudent=6;
        }else if(studentGrade >= 70 && studentGrade < 80){
          studentsids[7]+=1;
          if(student_id==studentid)
            posStudent=7;
        }else if(studentGrade >= 80 && studentGrade < 90){
          studentsids[8]+=1;
          if(student_id==studentid)
            posStudent=8;
        }else if(studentGrade >= 90){
          studentsids[9]+=1;
          if(student_id==studentid)
            posStudent=9;
        }
      });
      //convierto a porcentajes el numero de estudiantes
      let porcentajes = [];
      for(var i=0;i<studentsids.length;i++){
        let procentaje = (studentsids[i]*100)/numEstudiantes;//regla de tres, si numestudiantes es 100, cuanto sera el numero de la posicion i
        porcentajes.push(parseFloat(procentaje.toFixed(2)));//convierto a string con dos decimales con la funcion toFixed(2) y de ese string lo convierto a float para mantener el formato numero y no string.
      }
      const retorna = {
        data: porcentajes,
        position: posStudent
      }
      return retorna;
    }
}
