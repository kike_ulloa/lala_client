var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/api/doc/index-doc.html');
});

router.get('/doc', function(req, res, next) {
  res.redirect('/api/doc/index-doc.html');
});
module.exports = router;
