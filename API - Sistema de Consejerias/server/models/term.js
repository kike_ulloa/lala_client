

module.exports = function(sequelize, DataTypes) {
    var Term = sequelize.define('term', {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoincrement: true
      },
      year: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      semester: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      tags: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      start_Date: {
          type: DataTypes.DATE,
          allowNull: true
      },
      end_Date: {
          type: DataTypes.DATE,
          allowNull: true
      }
    }, {
      tableName: 'term',
      timestamps: false
    });
  
    Term.associate = function (db){
      Term.hasMany(db.history_academic_course,{foreignKey:'term_id'});
    }
  
    return Term;
  };
  