

module.exports = function(sequelize, DataTypes) {
    const Course = sequelize.define('course', {
      name: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      code: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      desc: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      credits: {
        type: DataTypes.DOUBLE,
        allowNull: true
      },
      tags: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      state: {
        type: DataTypes.TEXT,
        allowNull: true,
        defaultValue: 'state'
      },
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      }
    }, {
      tableName: 'course',
      timestamps: false
    });
  
    Course.associate = function(db){
      Course.hasOne(db.program_course,{foreignKey:{name:'course_id'}});
      Course.hasMany(db.history_academic_course,{foreignKey:'course_id'});
    };
  
  
    return Course;
  };
  