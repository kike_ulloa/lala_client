module.exports = function(sequelize, DataTypes) {
    const Program =  sequelize.define('program', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      desc: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      state: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'program',
      timestamps: false
    });
  
    Program.associate = function(db){
      Program.hasMany(db.curriculum,{foreignKey:{name:'program_id'}});
    };
  
    return Program;
  };