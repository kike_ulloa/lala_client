

module.exports = function(sequelize, DataTypes) {
    var Counselor = sequelize.define('counselor', {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'counselor',
      timestamps: false
    });
  
    Counselor.associate = function (db){
      Counselor.hasMany(db.meeting,{foreignKey:'counselor_id'});
    }
  
    return Counselor;
  };
  