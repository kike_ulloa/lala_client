

module.exports = function(sequelize, DataTypes) {
    const CourseAvailable = sequelize.define('course_available', {
      id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true
      },
      student_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
              model: 'student',
              key: 'anonid'
          },
          unique: false
      },
      curriculum_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'curriculum',
          key: 'id'
        },
        unique: true
      },
      course_code: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      course_name: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      course_area: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      course_level: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'course_available',
      timestamps: false
    });
  
    CourseAvailable.associate = function(db){
      CourseAvailable.belongsTo(db.student,{foreignKey:{name:'student_id'}});
      CourseAvailable.belongsTo(db.curriculum,{foreignKey:{name:'curriculum_id'}});
    }
  
  
    return CourseAvailable;
  };
  