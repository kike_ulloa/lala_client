

module.exports = function(sequelize, DataTypes) {
    var Student = sequelize.define('student', {
      anonid: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      state: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      gender: {
          type: DataTypes.TEXT,
          allowNull: true
      },
      email: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'student',
      timestamps: false
    });
  
    Student.associate = function (db){
      Student.hasMany(db.student_curriculum,{foreignKey:'student_id'});
      Student.hasMany(db.history_academic_course,{foreignKey:'student_id'});
      Student.hasMany(db.course_available,{foreignKey:'student_id'});
      Student.hasMany(db.meeting,{foreignKey:'student_id'});
    }
  
    return Student;
  };
  