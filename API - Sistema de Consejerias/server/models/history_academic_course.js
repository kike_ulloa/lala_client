
module.exports = function(sequelize, DataTypes) {
    const History = sequelize.define('history_academic_course', {
      student_id: {
        type: DataTypes.TEXT,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'student',
          key: 'anonid'
        }
      },
      curriculum_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
              model: 'curriculum',
              key: 'id'
          }
      },
      term_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
            model: 'term',
            key: 'id'
        }
      },
      course_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: false,
        references: {
          model: 'course',
          key: 'id'
        }
      },
      registration: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      teacher_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
              model: 'teacher',
              key: 'anonid'
          }
      },
      grade: {
        type: DataTypes.DOUBLE,
        allowNull: true
      },
      workshop1_grade: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      midterm_grade: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      workshop2_grade: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      final_exam_grade: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      retake_grade: {
          type: DataTypes.DOUBLE,
          allowNull:true
      },
      n_missed_classes: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      group: {
          type: DataTypes.TEXT,
          allowNull: true
      },
      n_students_in_group: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      group_average_grade: {
          type: DataTypes.DOUBLE,
          allowNull:true
      },
      state: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      approval_method: {
        type: DataTypes.TEXT,
        allowNull:true
      },
      code_valida: {
        type: "ARRAY",
        allowNull: true
      },
      id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          primaryKey: true
      }
    }, {
      tableName: 'history_academic_course',
      timestamps: false
    });
  
    History.associate = function(db){
        History.belongsTo(db.student,{foreignKey:'student_id'});
        History.belongsTo(db.curriculum,{foreignKey:'curriculum_id'});
        History.belongsTo(db.term,{foreignKey:'term_id'});
        History.belongsTo(db.course,{foreignKey:'course_id'});
        History.belongsTo(db.teacher,{foreignKey:'teacher_id'});
        
        // En realidad la clave foranea es una composicion de student_id,year y semestre
        // no solo student_id, pero debido a que sequelize no perimite la composicion de claves
        // el filtro por los 3 parametros esta implementado en el query
        //{
        //  model:db.statistic_student_term,
        //  on:{
        //    col1: Sequelize.where(Sequelize.col("history_academic_course.year"), "=", Sequelize.col("statistic_student_terms.year")),
        //    col2: Sequelize.where(Sequelize.col("history_academic_course.semester"), "=", Sequelize.col("statistic_student_terms.semester")),
        //    col3: Sequelize.where(Sequelize.col("statistic_student_terms.student_id"), "=", studentId)
        //  }
        //}
        //History.hasOne(db.statistic_student_term,{foreignKey:'student_id',sourceKey:'student_id'});
  
        // Esta es una segunda forma de hacerlo, pero debe removerse el "on" en el query
       /* History.hasOne(db.statistic_student_term, { foreignKey: 'student_id', scope: { year: sequelize.where(sequelize.col('history_academic_course.year'), '=', sequelize.col('statistic_student_term.year')),
                                                                                        semester: sequelize.where(sequelize.col('history_academic_course.semester'), '=', sequelize.col('statistic_student_term.semester'))
                                                                                      }})*/
    }
  
    return History;
  
  };
  