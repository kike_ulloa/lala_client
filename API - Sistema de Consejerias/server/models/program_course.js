

module.exports = function(sequelize, DataTypes) {
  const ProgramCourse = sequelize.define('program_course', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    program_term_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'program_term',
        key: 'id'
      }
    },
    course_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'course',
        key: 'id'
      }
    },
    area: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    mention: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    requisites: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    dependents: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    theoretical_hours: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    practical_hours: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    autonomous_hours: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    complexity: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    avg_last_semester: {
        type: DataTypes.DOUBLE,
        allowNull: true
    },
    success_rate_last_semester: {
        type: DataTypes.DOUBLE,
        allowNull: true
    },
    blocked: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    state: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    type: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'program_course',
    timestamps: false
  });

  ProgramCourse.associate = function(db){
    ProgramCourse.belongsTo(db.program_term,{foreignKey:{name:'program_term_id'}});
    ProgramCourse.belongsTo(db.course,{foreignKey:{name:'course_id'}});
  }

  return ProgramCourse;
};
