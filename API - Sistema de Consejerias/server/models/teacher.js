

module.exports = function(sequelize, DataTypes) {
    var Teacher = sequelize.define('teacher', {
      anonid: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      state: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'teacher',
      timestamps: false
    });
  
    Teacher.associate = function (db){
      Teacher.hasMany(db.history_academic_course,{foreignKey:'teacher_id'});
    }
  
    return Teacher;
  };
  