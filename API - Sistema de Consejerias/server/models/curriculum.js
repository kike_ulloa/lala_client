

module.exports = function(sequelize, DataTypes) {
    const Curriculum = sequelize.define('curriculum', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      program_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'program',
          key: 'id'
        },
        unique: true
      },
      tags: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      state: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      year: {
        type: DataTypes.INTEGER,
        allowNull: true,
        primaryKey: false
      },
      semester: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'curriculum',
      timestamps: false
    });
  
    Curriculum.associate = function(db){
        Curriculum.belongsTo(db.program,{foreignKey:{name:'program_id'}});
        Curriculum.hasMany(db.program_term,{foreignKey:{name:'curriculum_id'}});
        Curriculum.hasMany(db.student_curriculum,{foreignKey:{name:'curriculum_id'}});
        Curriculum.hasMany(db.course_available,{foreignKey:{name:'curriculum_id'}});
        Curriculum.hasMany(db.history_academic_course,{foreignKey:{name:'curriculum_id'}});
    }
  
    return Curriculum;
  
  };
  