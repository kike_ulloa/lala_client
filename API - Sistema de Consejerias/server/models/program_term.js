/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const ProgramTerm = sequelize.define('program_term', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    curriculum_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'curriculum',
        key: 'id'
      },
      unique: true
    },
    year: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: true,
      primaryKey: false
    },
    tags: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'program_term',
    timestamps: false
  });

  ProgramTerm.associate = function(db){
    ProgramTerm.belongsTo(db.curriculum,{foreignKey:{name:'curriculum_id'}});
    ProgramTerm.hasMany(db.program_course,{foreignKey:{name:'program_term_id'}});
  }


  return ProgramTerm;
};
