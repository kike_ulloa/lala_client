

module.exports = function(sequelize, DataTypes) {
    const Meeting = sequelize.define('meeting', {
      student_id: {
          type: DataTypes.BIGINT,
          allowNull: false,
          references: {
              model: 'student',
              key: 'anonid'
          },
          unique: false
      },
      counselor_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'curriculum',
          key: 'id'
        },
        unique: true
      },
      date: {
        type: DataTypes.DATE,
        allowNull: true
      },
      observations: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'meeting',
      timestamps: false
    });
  
    Meeting.associate = function(db){
      Meeting.belongsTo(db.student,{foreignKey:{name:'student_id'}});
      Meeting.belongsTo(db.counselor,{foreignKey:{name:'counselor_id'}});
    }
  
  
    return Meeting;
  };
  