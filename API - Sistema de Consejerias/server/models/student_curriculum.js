

module.exports = function(sequelize, DataTypes) {
    const StudentCurriculum = sequelize.define('student_curriculum', {
        id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoincrement: true
        },
      student_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'student',
          key: 'anonid'
        }
      },
      year: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      curriculum_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'curriculum',
          key: 'id'
        }
      },
      semester: {
          type: DataTypes.TEXT,
          allowNull: true
      },
      n_courses_in_program: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      n_taken_courses: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      n_approved_courses: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      n_failed_courses: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      n_retaken_courses: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      average_grade: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      last_workload: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      last_success_rate: {
          type: DataTypes.DOUBLE,
          allowNull: true
      },
      n_current_courses: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      state: {
          type: DataTypes.TEXT,
          allowNull: true
      }
    }, {
      tableName: 'student_curriculum',
      timestamps: false
    });
  
    StudentCurriculum.associate = function(db){
      StudentCurriculum.belongsTo(db.student,{foreignKey:'student_id'});
      StudentCurriculum.belongsTo(db.curriculum,{foreignKey:'curriculum_id'});
    }
  
  
    return StudentCurriculum;
  };
  