const request = require('request-promise')
const btoa = require('btoa')


const ISSUER = 'https://dev-878998.oktapreview.com/oauth2/ausj08qk5vYaB8HWc0h7';
const TEST_CLIENT_ID = '0oaj19qfa1c2F66WD0h7';
const TEST_CLIENT_SECRET = 'LxhIkvex-1Z1JAx7vhW9LL4VhSYb-yuSlvUGUPlp';
const DEFAULT_SCOPE = '';
//const ENDPOINT = 'http://localhost:3000/api/v1/students/00918df7-5843-c53e-eec9-1595af6fdcd9?program=223';
//const ENDPOINT = 'http://localhost:3000/api/v1/students/E26?program=1';
//const ENDPOINT = 'http://localhost:3000/api/v1/students2/'; //toda la malla 
//const ENDPOINT = 'http://localhost:3000/api/v1/studentacademicsbycurriculum/?studentid=1&curriculumid=1'; //el historico academico del estudiante
const ENDPOINT = 'http://localhost:3000/api/v1/studentpartners/?studentid=1&curriculumid=1&courseid=14&termid=3&group=A'; //los companeros de aula que aprobaron

const test = async () => {
  const token = btoa(`${TEST_CLIENT_ID}:${TEST_CLIENT_SECRET}`)
  try {
      const { token_type, access_token } = await request({
      uri: `${ISSUER}/v1/token`,
      json: true,
      method: 'POST',
      headers: {
        authorization: `Basic ${token}`,
      },
      form: {
        grant_type: 'client_credentials',
        scope: DEFAULT_SCOPE,
      },
    })

    const response = await request({
      uri: ENDPOINT,
      json: true,
      rejectUnauthorized: false,
      headers: {
        authorization: [token_type, access_token].join(' '),
      },
    })
    //console.log(response[0].student_curriculums[0].curriculum.program_terms[0].program_courses[0].me);
    console.log(response);
    //console.log(JSON.parse(response).student_curriculum.curriculum.program_term.program_course,access_token);
    //console.log(response[0].student_curriculum[0].curriculum.program_term[0].program_course);
    //console.log(JSON.parse(JSON.parse(response).student_curriculum));
    //console.log(JSON.parse(response).student_curriculum);
    //console.log(JSON.parse(response)[0].student_curriculum[0].curriculum.program_term[0].program_course);
  } catch (error) {
    console.log(`Error: ${error.message}`)
  }
}

test()
