define({
  "name": "LALA API",
  "version": "0.1.0",
  "description": "The LALA API REST documentation.",
  "title": "LALA API",
  "url": "https://localhost:3000/api/v1",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-01-11T15:52:25.258Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
