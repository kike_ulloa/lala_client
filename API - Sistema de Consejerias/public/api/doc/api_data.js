define({ "api": [
  {
    "type": "get",
    "url": "/coursesAcademics/:code",
    "title": "Request Statistics information from an specific course.",
    "version": "1.0.0",
    "name": "GetCourseAcademics",
    "group": "Course",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Course code.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Unique access-token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://localhost:3000/api/v1/coursesAcademics/EYAG1012",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "[ACADEMICS_BY_TERM]",
            "optional": false,
            "field": "academicsByTerm",
            "description": "<p>An array of ACADEMICS_BY_TERM of the Course .</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "year",
            "description": "<p>The year of the term.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "term",
            "description": "<p>The semester of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "StatisticByTerm",
            "optional": false,
            "field": "academics",
            "description": "<p>Statistic of the Course in a specific Term.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n    \"academicsByTerm\": [\n        {\n            \"year\": 2018,\n            \"semester\": \"1S\",\n            \"academics\": {\n                \"distribution\": [\n                    {\n                        \"label\": \"label1\",\n                        \"value\": 2\n                    },\n                    {\n                        \"label\": \"label2\",\n                        \"value\": 1\n                    },\n                    {\n                        \"label\": \"label3\",\n                        \"value\": 2\n                    }\n                ],\n                \"dist_range\": {\n                    \"max\": 3,\n                    \"min\": 2\n                },\n                \"student_count\": null,\n                \"fail_rate\": null,\n                \"drop_rate\": null,\n                \"count_by_delay\": null\n            }\n        },\n        {\n            \"year\": 2015,\n            \"semester\": \"1S\",\n            \"academics\": {\n                \"distribution\": [\n                    {\n                        \"label\": \"label1\",\n                        \"value\": 2\n                    },\n                    {\n                        \"label\": \"label2\",\n                        \"value\": 1\n                    },\n                    {\n                        \"label\": \"label3\",\n                        \"value\": 2\n                    }\n                ],\n                \"dist_range\": {\n                    \"max\": 3,\n                    \"min\": 2\n                },\n                \"student_count\": null,\n                \"fail_rate\": null,\n                \"drop_rate\": null,\n                \"count_by_delay\": null\n            }\n        },\n        {\n            \"year\": 2015,\n            \"semester\": \"2S\",\n            \"academics\": {\n                \"distribution\": [\n                    {\n                        \"label\": \"label1\",\n                        \"value\": 2\n                    },\n                    {\n                        \"label\": \"label2\",\n                        \"value\": 1\n                    },\n                    {\n                        \"label\": \"label3\",\n                        \"value\": 2\n                    }\n                ],\n                \"dist_range\": {\n                    \"max\": 3,\n                    \"min\": 2\n                },\n                \"student_count\": null,\n                \"fail_rate\": null,\n                \"drop_rate\": null,\n                \"count_by_delay\": null\n            }\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProgramNotFound",
            "description": "<p>The <code>id</code> or the <code>year</code> of the Program was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-courseNotFound-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n   \"errors\": [\n      {\n           \"status\": 404,\n           \"error\": \"coursesAcademicsNotFound\",\n           \"detail\": \"Wasn't found data for that cours.\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-NoAccessRight-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/api/v1/courses.js",
    "groupTitle": "Course"
  },
  {
    "type": "get",
    "url": "/programs/:id",
    "title": "Request Program information from a specific year",
    "version": "1.0.0",
    "name": "GetProgram",
    "group": "Program",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Program unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "year",
            "description": "<p>Year of the program.</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Unique access-token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://localhost:3000/api/v1/programs/4711?year=2016",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastDataUpdate",
            "description": "<p>The last date when the data were loaded.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the Program.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The name of the Program.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>The description of the Program.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "plan",
            "description": "<p>The id of the Curriculum.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "year",
            "description": "<p>The year of the Curriculum.</p>"
          },
          {
            "group": "Success 200",
            "type": "[Term]",
            "optional": false,
            "field": "terms",
            "description": "<p>An array of Terms</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "term[position]",
            "description": "<p>The position of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "term[name]",
            "description": "<p>The name of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "[String]",
            "optional": false,
            "field": "term[tags]",
            "description": "<p>An Array of tags for the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "[Course]",
            "optional": false,
            "field": "term[courses]",
            "description": "<p>An array of Courses.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[code]",
            "description": "<p>The code of the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[name]",
            "description": "<p>The name of the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[desc]",
            "description": "<p>The description of the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[area]",
            "description": "<p>The area of the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "[String]",
            "optional": false,
            "field": "course[tags]",
            "description": "<p>An Array of tags for the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "[String]",
            "optional": false,
            "field": "course[requisites]",
            "description": "<p>An Array of requisites for the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "course[credits]",
            "description": "<p>The credits of the Course.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n {\n    \"lastDataUpdate\": \"2018-01-12T05:00:00.000Z\",\n    \"id\": \"4711\",\n    \"name\": \"Ingeniería Civil en Informática\",\n    \"desc\": \"Description\",\n    \"plan\": \"3\",\n    \"year\": \"2016\",\n    \"tags\": [\n        \"Tag1\",\n        \"Tag2\"\n    ],\n    \"terms\": [\n        {\n            \"position\": \"1\",\n            \"name\": \"1 Término\",\n            \"tags\": [\n                \"Tag1\",\n                \"Tag2\"\n            ],\n            \"courses\": [\n                {\n                    \"code\": \"BAIN037\",\n                    \"name\": \"Cálculo en una Variable\",\n                    \"desc\": \"Descr\",\n                    \"tags\": [\n                        \"Tag1\",\n                        \"Tag2\"\n                    ],\n                    \"requisites\": [\n                        \"Álgebra para Ingeniería\"\n                    ],\n                    \"credits\": 6\n                },\n                {\n                    \"code\": \"BAIN019\",\n                    \"name\": \"Química para Ingeniería\",\n                    \"desc\": \"Descr\",\n                    \"tags\": [\n                        \"Tag1\",\n                        \"Tag2\"\n                    ],\n                    \"requisites\": [],\n                    \"credits\": 2\n                }\n            ]\n        },\n        {\n            \"position\": \"2\",\n            \"name\": \"2  Término\",\n            \"tags\": [\n                \"Tag1\",\n                \"Tag2\"\n            ],\n            \"courses\": [\n                {\n                    \"code\": \"DYRE060\",\n                    \"name\": \"Taller de Ingeniería: Programación Aplicada\",\n                    \"desc\": \"Descr\",\n                    \"tags\": [\n                        \"Tag1\",\n                        \"Tag2\"\n                    ],\n                    \"requisites\": [\n                        \"Álgebra para Ingeniería\",\n                        \"Geometría para Ingeniería\",\n                        \"Taller de Ingeniería I\"\n                    ],\n                    \"credits\": 3\n                },\n                {\n                    \"code\": \"BAIN039\",\n                    \"name\": \"Comunicación Idioma Inglés\",\n                    \"desc\": \"Descr\",\n                    \"tags\": [\n                        \"Tag1\",\n                        \"Tag2\"\n                    ],\n                    \"requisites\": [\n                        \"AA\"\n                    ],\n                    \"credits\": 5\n                }\n            ]\n        },\n        {\n            \"position\": \"3\",\n            \"name\": \"3 Término\",\n            \"tags\": [],\n            \"courses\": []\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidParameter",
            "description": "<p>The <code>id</code> or <code>year</code> are not Number.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingParameter",
            "description": "<p>The <code>year</code> is missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProgramNotFound",
            "description": "<p>The <code>id</code> or the <code>year</code> of the Program was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-InvalidParameter-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"errors\": [\n      {\n           \"status\": 422,\n           \"error\": \"InvalidParameter\",\n           \"detail\": \"params[id]: Must be an Integer\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-MissingParameter-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"errors\": [\n      {\n           \"status\": 422,\n           \"error\": \"MissingParameter\",\n           \"detail\": \"body[year]: Must be provided\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-ProgramNotFound-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n   \"errors\": [\n      {\n           \"status\": 404,\n           \"error\": \"ProgramNotFound\",\n           \"detail\": \"The Program wasn't found.\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-NoAccessRight-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/api/v1/programs.js",
    "groupTitle": "Program"
  },
  {
    "type": "get",
    "url": "/students/:id",
    "title": "Request Student academic information from a specific program",
    "version": "1.0.0",
    "name": "GetStudent",
    "group": "Student",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Student unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "program",
            "description": "<p>Program unique ID</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Unique access-token.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://localhost:3000/api/v1/students/00918df7-5843-c53e-eec9-1595af6fdcd9?program=223",
        "type": "curl"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastDataUpdate",
            "description": "<p>The last date when the data were loaded.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the Student.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "program",
            "description": "<p>The name of the Program.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "cohortYear",
            "description": "<p>The cohort year of the student</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "plan",
            "description": "<p>The id of the last Curriculum of the Student in a Program.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "planYear",
            "description": "<p>The year version of the last Curriculum of the Student in a Program.</p>"
          },
          {
            "group": "Success 200",
            "type": "[Curriculum]",
            "optional": false,
            "field": "previousPlans",
            "description": "<p>An array with information (id and year) about previous curriculums in a program of the student.</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "bachCompletedCourses",
            "description": "<p>Data related with the student's dropout</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "bachTotalCourses",
            "description": "<p>Data related with the student's dropout</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "estimatedTermsToCompleteBach",
            "description": "<p>Data related with the student's dropout</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "estimatedProbability",
            "description": "<p>Data related with the student's dropout</p>"
          },
          {
            "group": "Success 200",
            "type": "[Term]",
            "optional": false,
            "field": "terms",
            "description": "<p>An array of Term academics for a student.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "term[year]",
            "description": "<p>The year of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "term[semester]",
            "description": "<p>The semester of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "term[termAvg]",
            "description": "<p>An statistic of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "term[accAvg]",
            "description": "<p>An statistic of the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "[Course]",
            "optional": false,
            "field": "term[courses]",
            "description": "<p>An Array of Courses taken by the Student in the Term.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[code]",
            "description": "<p>The code of the Course.</p>"
          },
          {
            "group": "Success 200",
            "type": "Double",
            "optional": false,
            "field": "course[grade]",
            "description": "<p>The grade get it by the Student.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[registration]",
            "description": "<p>The registration of the Course. (cursada, homologada, convalidada, anulada)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "course[status]",
            "description": "<p>The status of the Course. (Aprobada, Reprobada)</p>"
          },
          {
            "group": "Success 200",
            "type": "StatisticByGroup",
            "optional": false,
            "field": "course[historicGroup]",
            "description": "<p>Some statistics about a course in general.</p>"
          },
          {
            "group": "Success 200",
            "type": "StatisticByCohort",
            "optional": false,
            "field": "course[cohortGroup]",
            "description": "<p>Some statistics about a course in the specific cohort of the student.</p>"
          },
          {
            "group": "Success 200",
            "type": "StatisticByClass",
            "optional": false,
            "field": "course[classGroup]",
            "description": "<p>Some statistics about a course in a specific term.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n    \"lastDataUpdate\": \"2018-01-12T05:00:00.000Z\"\n    \"id\": \"00918df7-5843-c53e-eec9-1595af6fdcd9\",\n    \"program\": \"Ingeniería Civil\",\n    \"cohortYear\": 2016,\n    \"plan\": \"2864\",\n    \"planYear\": 2015,\n    \"previousPlans\": [],\n    \"bachCompletedCourses\": 0.0,\n    \"bachTotalCourses\": 0.0,\n    \"estimatedTermsToCompleteBach\": 0.0,\n    \"estimatedProbability\": 0.0,\n    \"terms\": [\n        {\n            \"year\": 2015,\n            \"semester\": \"1S\",\n            \"termAvg\": null,\n            \"accAvg\": null,\n            \"coursesTaken\": [\n                {\n                    \"code\": \"ICF01107\",\n                    \"name\": \"LABORATORIO DE FÍSICA A\",\n                    \"grade\": 7.8,\n                    \"registration\": null,\n                    \"state\": \"AP\",\n                    \"historicGroup\": {\n                        \"course_code\": \"ICF01107\",\n                        \"distribution\": [\n                            {\n                                \"label\": \"label1\",\n                                \"value\": 2\n                            },\n                            {\n                                \"label\": \"label2\",\n                                \"value\": 1\n                            },\n                            {\n                                \"label\": \"label3\",\n                                \"value\": 2\n                            }\n                        ],\n                        \"dist_range\": {\n                            \"max\": 3,\n                            \"min\": 2\n                        },\n                        \"student_count\": 2,\n                        \"fail_rate\": 23,\n                        \"drop_rate\": null,\n                        \"count_by_delay\": null\n                    },\n                    \"cohortGroup\": {\n                        \"course_id\": \"368914\",\n                        \"year\": 2015,\n                        \"distribution\": [\n                            {\n                                \"label\": \"label1\",\n                                \"value\": 5\n                            },\n                            {\n                                \"label\": \"label2\",\n                                \"value\": 7\n                            },\n                            {\n                                \"label\": \"label3\",\n                                \"value\": 3\n                            }\n                        ],\n                        \"dist_range\": {\n                            \"min\": 2,\n                            \"max\": 3\n                        },\n                        \"student_count\": 3,\n                        \"fail_rate\": 18,\n                        \"drop_rate\": 20,\n                        \"count_by_delay\": null\n                    },\n                    \"classGroup\": {\n                        \"distribution\": [\n                            {\n                                \"label\": \"label1\",\n                                \"value\": 2\n                            },\n                            {\n                                \"label\": \"label2\",\n                                \"value\": 3\n                            },\n                            {\n                                \"label\": \"label3\",\n                                \"value\": 5\n                            },\n                            {\n                                \"label\": \"label3\",\n                                \"value\": 40\n                            },\n                            {\n                                \"label\": \"label4\",\n                                \"value\": 0\n                            },\n                            {\n                                \"label\": \"label5\",\n                                \"value\": 0\n                            }\n                        ],\n                        \"dist_range\": {\n                            \"min\": 1,\n                            \"max\": 7\n                        },\n                        \"student_count\": 50,\n                        \"fail_rate\": 0.2,\n                        \"drop_rate\": 0.24,\n                        \"count_by_delay\": {},\n                        \"course_code\": \"ICF01107\",\n                        \"year\": 2011,\n                        \"semester\": \"1S\"\n                    }\n                }\n            ]\n        },\n        {\n            \"year\": 2015,\n            \"semester\": \"2S\",\n            \"termAvg\": null,\n            \"accAvg\": null,\n            \"coursesTaken\": [\n                {\n                    \"code\": \"ICF01123\",\n                    \"name\": \"LABORATORIO DE FÍSICA B\",\n                    \"grade\": 8.7,\n                    \"registration\": null,\n                    \"state\": \"AP\",\n                    \"historicGroup\": {\n                        \"course_code\": \"ICF01123\",\n                        \"distribution\": null,\n                        \"dist_range\": null,\n                        \"student_count\": null,\n                        \"fail_rate\": null,\n                        \"drop_rate\": null,\n                        \"count_by_delay\": null\n                    },\n                    \"cohortGroup\": {\n                        \"course_id\": \"361992\",\n                        \"year\": 2015,\n                        \"distribution\": null,\n                        \"dist_range\": {\n                            \"min\": 2,\n                            \"max\": 3\n                        },\n                        \"student_count\": 4,\n                        \"fail_rate\": 44,\n                        \"drop_rate\": 22,\n                        \"count_by_delay\": null\n                    },\n                    \"classGroup\":null\n                },\n                {\n                    \"code\": \"ICF01115\",\n                    \"name\": \"FÍSICA B\",\n                    \"grade\": 7.45,\n                    \"registration\": null,\n                    \"state\": \"AP\",\n                    \"group\": null,\n                    \"historicGroup\": {\n                        \"course_id\": \"361995\",\n                        \"year\": 2015,\n                        \"distribution\": null,\n                        \"dist_range\": null,\n                        \"student_count\": null,\n                        \"fail_rate\": null,\n                        \"drop_rate\": null,\n                        \"count_by_delay\": null\n                    },\n                    \"classGroup\":null\n                }\n            ]\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidParameter",
            "description": "<p>The <code>program</code> is not Number.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingParameter",
            "description": "<p>The <code>program</code> is missing.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProgramNotFound",
            "description": "<p>The <code>id</code> of the Student was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-InvalidParameter-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"errors\": [\n      {\n           \"status\": 422,\n           \"error\": \"InvalidParameter\",\n           \"detail\": \"params[program]: Must be an Integer\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-MissingParameter-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n   \"errors\": [\n      {\n           \"status\": 422,\n           \"error\": \"MissingParameter\",\n           \"detail\": \"body[program]: Must be provided\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-StudentNotFound-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n   \"errors\": [\n      {\n           \"status\": 404,\n           \"error\": \"StudentNotFound\",\n           \"detail\": \"The Student wasn't found.\"\n       }\n   ]\n}",
          "type": "json"
        },
        {
          "title": "Error-NoAccessRight-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/api/v1/students.js",
    "groupTitle": "Student"
  }
] });
