var Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = (db) => {

    return {

      /**
      * Summary. Get a Program with a specific Curriculum.
      *
      * Description. Get a Program with a specific Curriculum.
      *
      * @param {int}   id       The id of the Program.
      * @param {int}   year     The year of the Curriculum.
      *
      * @return {Object} The response object.
      */
      getProgram : async(id,year)=>{
        const data = await db.curriculum.findOne({
          where:{
            "$program.id$":id,
            "$program.state$":'active',
            year:year,
            state:'active'
          },
          include:[{model:db.program}]
        });
        return data;
      },

      /**
      * Summary. Get an Array of ProgramTerms with their Courses from a specific Curriculum.
      *
      * Description. Get an Array of ProgramTerms with their Courses from a specific Curriculum.
      *
      * @param {int}   curriculum_id       The id of the Curriculum.
      *
      * @return {Object} The response object.
      */
      getProgramTerms: async(curriculum_id) =>{
        const terms = await db.program_term.findAll({
          order:[
            ['position','ASC']
          ],
          where:{
            curriculum_id:curriculum_id,
            [Op.or]: [{"$program_courses.state$":'active'},{"$program_courses.state$":null}]
          },
          include:[
                    {
                      model:db.program_course,
                      include:[
                        {
                          model:db.course,
                          where:{
                              state:'active'
                            }
                        }
                      ]
                    }
                  ]
        });
        return terms;
      },

      /**
      * Summary. Get a Student academic from a program.
      *
      * Description. Get a Student academic from a program.
      *
      * @param {string}  studentId       The id of the Student.
      * @param {int}     programId     The id of the Program.
      *
      * @return {Object} The response object.
      */
      getStudentsAcademics: async(studentId,programId) => {

        const student = await db.student.findOne({
              order:[
                // If I want to order for attributes belonging in other tables
                [db.student_curriculum,db.curriculum,'year','DESC']
              ],
              where:{
                id:studentId,
                state:'active'
              },
              include:[
                {
                  model:db.student_curriculum,
                  include:[
                    {
                      model:db.curriculum,
                      where:{
                        state:'active'
                      },
                      include:{
                        model:db.program,
                        where:{
                          id:programId,
                          state:'active'
                        }
                      }
                    }
                  ]
                },
                {
                  model:db.student_dropout
                }
              ]
            });
          return student;
      },

      /**
      * Summary. Get the history academic of a student from an specific Curriculum.
      *
      * Description. Get the history academic of a student from an specific Curriculum.
      *
      * @param {string}  studentId       The id of the Student.
      * @param {int}     curriculumId    The id of the Curriculum.
      * @param {int}     cohortYear      The cohort year of the student. This parameter for the statistics cohort
      *
      * @return {Object} The response object.
      */
      getHistoryAcademicStudentByCurriculum: async(curriculumId,studentId,cohortYear) => {//Cohort year for the statistics cohort
          const historyAcademics = await db.history_academic_course.findAll({
            order:[
              ['year','ASC'],
              ['semester','ASC']
            ],
            where:{
                student_id:studentId
            },
            include:[
              {
                model:db.statistic_student_term,
                /*on:{
                  col1: Sequelize.where(Sequelize.col("history_academic_course.year"), "=", Sequelize.col("statistic_student_term.year")),
                  col2: Sequelize.where(Sequelize.col("history_academic_course.semester"), "=", Sequelize.col("statistic_student_term.semester")),
                  col3: Sequelize.where(Sequelize.col("statistic_student_term.student_id"), "=", studentId)
                }*/
              },
              {
                model:db.course,
                where:{
                  curriculum_id:curriculumId,
                  state:'active'
                },
                include:[
                  {
                    model:db.statisticTerm,
                    on: {
                      col1: Sequelize.where(Sequelize.col("course.code"), "=", Sequelize.col("course->statisticTerms.course_code")),
                      col2: Sequelize.where(Sequelize.col("history_academic_course.year"), "=", Sequelize.col("course->statisticTerms.year")),
                      col3: Sequelize.where(Sequelize.col("history_academic_course.semester"), "=", Sequelize.col("course->statisticTerms.semester"))
                    }
                  },
                  {
                    model:db.statistic
                  },
                  {
                    model:db.statisticCohort,
                    // La restriccion del año la coloco aqui debido a que
                    // where me produciria un inner join
                    // lefth join on a.course_id = b.course_id and a.year = cohorte
                    on: {
                      col1: Sequelize.where(Sequelize.col("course.id"), "=", Sequelize.col("course->statisticCohorts.course_id")),
                      col21: Sequelize.where(Sequelize.col("course->statisticCohorts.year"), "=",cohortYear)
                    }
                  }
                ]
              }
            ]
          });
        return historyAcademics;

      },

      getCourseAcademics: async (courseId) => {
        const statisticTerms = await db.statisticTerm.findAll({
          order:[
            [db.term,'year','DESC'],
            [db.term,'semester','ASC']
          ],
          where:{
            course_code:courseId
          },
          include:[
            {
              model:db.term,
            }
          ]
        });

        return statisticTerms;
      },

      /**
      * Summary. Get the las date when the data were loaded in the database.
      *
      * Description. Get the las date when the data were loaded in the database.
      *
      * @return {Object} The response object.
      */
      getLoadingDate: async () => {
        const loadingDate = await db.parameter.findAll({
          order:[
            ['loading_date','DESC']
          ]
        });

        return loadingDate.length === 0 ? null : loadingDate[0].loading_date;

      },
      /**
      *
      *Obtiene el listado de todos los estudiantes
      *
      */
      getAllStudents: async () => {
        const allStudents = await db.student.findAll({
          order: [
          ['name', 'DESC']
          ]
        });
        return allStudents;
      },
      /**
       * 
       * Query para extraer todas las materias a las que pertenece el estudiante con tal nombre
       */
      getCurriculum: async (studentId) => {
        const terms = await db.student.findAll({
          logging: console.log,
          // order: [
          //   [{model: db.program_course, as: "program_courses"}, "course_id", "ASC"]
          // ],
          // order: [
          //   ["student_curriculums.curriculum.program_terms.program_courses.course_id", 'asc']
          // ],
          where :{
            anonid:studentId
          },
          /*include:[
            {
              model:db.history_academic_course,
              //required: true
            }
            */
          include: [
            {
              model:db.student_curriculum,
              include:[
                {
                  model:db.curriculum,
                  include: [
                    {
                      model:db.program
                    },
                    {
                      model:db.program_term,
                      
                      include:[
                        {
                         
                          model:db.program_course,
                          
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ],
          order: [
            //con esta linea ordeno por id del curso que se encuentra en la tabla program course, pero a sequelize debo hacerle saber donde
            //se encuentra esta tabla, es decir, en que nivel de jerarquia, por lo cual debo poner el nivel de jerarquia
            [db.student_curriculum, db.curriculum, db.program_term, db.program_course, 'course_id', 'ASC'] 
          ] 
          //   {
          //   model:db.student_curriculum
          // }
          
        
          
         /* order:[
            ['position','ASC']
          ],
          where:{
            curriculum_id:1,
            [Op.or]: [{"$program_courses.state$":'ACTIVO'},{"$program_courses.state$":null}]
          },
          include:[
                    {
                      model:db.program_course,
                      include:[
                        {
                          model:db.course,
                          where:{
                              state:'active'
                            }
                        }
                      ]
                      
                    },
                    {
                      model:db.curriculum,
                      include: [
                        {
                          model:db.program
                        }
                      ]
                    }
          ]*/
        });
      return terms;
      },

      getCorsesAndProgramCourse: async (program_term_id) =>{
          const coursesPC= db.program_course.findAll({
           // logging: console.log,
           order: [
             ["program_term_id", "ASC"]
           ],
            where: {
              program_term_id:program_term_id
            },
            include: [{
              model:db.course
            }]
          });
          return coursesPC;
      },
      
      //Para extraer el historial academico de un estudiante a traves de su id
      getHistoryAcademicStudentByCurriculumId : async (studentId, curriculumId) => {
        const historyAcademics = await db.history_academic_course.findAll({
          //logging: console.log,
          order: [['term_id', 'ASC']],  
          where: {
              student_id: studentId,
              curriculum_id: curriculumId
            },
            include:[{
              model: db.term
            }]
        });
        return historyAcademics;
      },

      //Para extraer a todos los companeros de aula que aprobaron la asignatura dada
      getPartnersOfCourse : async (curriculum_id, course_id, term_id, group) => {
        const partners = await db.student.findAll({
          include: {
            model: db.history_academic_course,
            where: {
              curriculum_id: curriculum_id,
              course_id: course_id,
              term_id: term_id,
              group: group,
              state: 'APROBADO'
            }
          }
        });
        return partners;
      },

      //Para extraer a todos los companeros de aula que reprobaron la asignatura dada
      getPartnersOfCourseReprobados: async (curriculum_id, course_id, term_id, group, arrayAprobados) => {
        const Op = Sequelize.Op;//con este se guardan todos los operadores de sequalizer en OP (Op.notIn, Op.and, Op.or, etc, ver mas en http://docs.sequelizejs.com/manual/tutorial/querying.html#operators)
        const partnersLose= await db.student.findAll({
          
          //logging: console.log,
          include: {
            model: db.history_academic_course,
            where: {
              curriculum_id: curriculum_id,
              course_id: course_id,
              term_id: term_id,
              group: group,
              student_id: { [Op.notIn]: arrayAprobados }//indico que todos los datos que extraiga deben cumplir con la condicion de que los datos que extraiga, mediante el student_id no deben estar en los datos del array
            }
          }
        });
        return partnersLose;
      },

      //para extraer el historial de las sesiones de consegerias del estudiante
      getMeetingHistory: async (student_id) => {
        const history = await db.meeting.findAll({
          order: [
            ["date", "DESC"]
          ],
          where: {
            student_id: student_id
          },
          include: {
            model: db.counselor
          }
        });
        return history;
      },

      addMeeting: async (studentId, counselor_id, observations, date) => {
        console.log('esto llega',studentId, counselor_id, observations, date);
        return await db.meeting.create({
          student_id: studentId,
          counselor_id: counselor_id,
          observations: observations,
          date: date
        }).then(function(data){
          console.log(data);
          return data;
        }).catch(function(error){
          console.log(error.stack);
          return error;
        });
      }
    }

}
